/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of Oppai Trainer 2015.
	Oppai Trainer 2015 is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	Oppai Trainer 2015 is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with Oppai Trainer 2015. If not, see <http://www.gnu.org/licenses/>.
*/

#include "pointerwatch.h"

#include <stdio.h>
#include <hackdesu/command_queue.h>

#include "resource.h"
#include "win32_gui.h"
#include "maple_hacks.h"
#include "maple_structs.h"

#define PTRWATCH_CLASS "OppaiTrainer2015PtrWatch"
#define PTRWATCH_NAME "Pointer Watch"

#define IDC_EDIT_OUTPUT 101
#define IDC_BUTTON_REFRESH 102
#define IDC_CHECK_AUTOREFRESH 103

extern HINSTANCE oppai_instance;

static HWND ptrwatch_wnd = NULL; /* note: this must only be accessed from maple's thread */
static int ptrwatch_autorefresh = 0;

/* (called in maple's main thread) updates the pointer watch gui */
static void ptrwatch_performupdate(void *param) {
	HWND wnd = (HWND)param;
	char buf[5096] = { 0 };
	char buf2[2048] = { 0 };
	CUserLocal *user;
	CMobPool *mobs;
	CDropPool *drops;
	CWvsPhysicalSpace2D *map;
	CUIStatusBar *status;
	CWvsContext *server;

	user = CUserLocal__GetInstance();
	if (user) {
		CUserLocal_str(user, buf2, 2048);
		strcat_s(buf, 5096, buf2);
		strcat_s(buf, 5096, "\r\n\r\n");
	}

	mobs = CMobPool__GetInstance();
	if (mobs) {
		CMobPool_str(mobs, buf2, 2048);
		strcat_s(buf, 5096, buf2);
		strcat_s(buf, 5096, "\r\n\r\n");
	}

	drops = CDropPool__GetInstance();
	if (drops) {
		CDropPool_str(drops, buf2, 2048);
		strcat_s(buf, 5096, buf2);
		strcat_s(buf, 5096, "\r\n\r\n");
	}

	map = CWvsPhysicalSpace2D__GetInstance();
	if (map) {
		CWvsPhysicalSpace2D_str(map, buf2, 2048);
		strcat_s(buf, 5096, buf2);
		strcat_s(buf, 5096, "\r\n\r\n");
	}

	status = CUIStatusBar__GetInstance();
	if (status) {
		CUIStatusBar_str(status, buf2, 2048);
		strcat_s(buf, 5096, buf2);
		strcat_s(buf, 5096, "\r\n\r\n");
	}

	server = CWvsContext__GetInstance();
	if (server) {
		CWvsContext_str(server, buf2, 2048);
		strcat_s(buf, 5096, buf2);
	}

	if (!SetDlgItemTextA(wnd, IDC_EDIT_OUTPUT, buf)) {
		win32_show_error("SetDlgItemTextA failed for edit_output");
	}
}

static void ptrwatch_onupdate() {
	if (ptrwatch_autorefresh) {
		ptrwatch_performupdate((void *)ptrwatch_wnd);
	}
}

static void ptrwatch_onsize(HWND wnd) {
	win32_expand(wnd, IDC_EDIT_OUTPUT, 10, 40);
	win32_dock_bottom(wnd, IDC_BUTTON_REFRESH, 22, 11);
	win32_dock_bottom(wnd, IDC_CHECK_AUTOREFRESH, 20, 10);
}

static LRESULT ptrwatch_initwnd(HWND wnd) {
	HWND w;
	char buftitle[128] = { 0 };

	sprintf_s(buftitle, 128, "Pointer Watch | PID: %d", GetCurrentProcessId());
	SetWindowTextA(wnd, buftitle);

	w = win32_make_edit_s(
		wnd, IDC_EDIT_OUTPUT, "edit_output",
		"", 10, 10, 1, 1, 
		(WS_EDIT_DEFAULT ^ ES_AUTOHSCROLL) | ES_MULTILINE | WS_VSCROLL | WS_HSCROLL | ES_READONLY);
	if (!w) {
		return -1;
	}

	w = win32_make_button(wnd, IDC_BUTTON_REFRESH, "button_refresh", "Refresh", 9, 0, 52, 22);
	if (!w) {
		return -1;
	}

	/*
	w = win32_make_checkbox(wnd, IDC_CHECK_AUTOREFRESH, "check_autorefresh", "Auto Refresh (NOTE: this will lag)", 70, 0, 100, 20);
	if (!w) {
		return -1;
	}
	*/

	return 0;
}

static void ptrwatch_refreshclicked(HWND wnd) {
	cq_callback cb = { ptrwatch_performupdate };
	cq_call(cb, wnd, cq_no_callback);
}

/* (called in maple's main thread) sets the auto refresh flag */
static void ptrwatch_setautorefresh(void *param) {
	ptrwatch_autorefresh = (int)param;
}

static void ptrwatch_autorefreshclicked(HWND wnd) {
	cq_callback cb;
	HWND wndcheck;

	wndcheck = GetDlgItem(wnd, IDC_CHECK_AUTOREFRESH);
	if (!wndcheck) {
		win32_show_error("GetDlgItem failed for autorefresh");
		return;
	}
	
	cb.func_ptr = ptrwatch_setautorefresh;
	cq_call(cb, (void *)(Button_GetCheck(wndcheck) == BST_CHECKED), cq_no_callback);
}

static LRESULT CALLBACK ptrwatch_wndproc(HWND wnd, UINT msgid, WPARAM wparam, LPARAM lparam) {
	switch (msgid) {
	case WM_CREATE:
		return ptrwatch_initwnd(wnd);

	case WM_SIZE:
		ptrwatch_onsize(wnd);
		break;

	case WM_CLOSE:
		ShowWindow(wnd, SW_HIDE);
		break;

	case WM_COMMAND:
		switch (LOWORD(wparam)) {
		case IDC_BUTTON_REFRESH:
			ptrwatch_refreshclicked(wnd);
			return TRUE;

		case IDC_CHECK_AUTOREFRESH:
			ptrwatch_autorefreshclicked(wnd);
			return TRUE;
		}
		break;

	default:
		return DefWindowProcA(wnd, msgid, wparam, lparam);
	}

	return 0;
}

/* (called in maple's main thread) calls ShowWindow with the given param on the pointer watch wnd */
static void ptrwatch_showwnd(void *params) {
	if (ptrwatch_wnd) {
		ShowWindow(ptrwatch_wnd, (int)params);
		ptrwatch_refreshclicked(ptrwatch_wnd);
	}
}

static void ptrwatch_setwnd(void *param) {
	ptrwatch_wnd = (HWND)param;
}

static void ptrwatch_destroywnd(void *param) {
	if (ptrwatch_wnd) {
		DestroyWindow(ptrwatch_wnd);
	}
}

void ptrwatch_show() {
	cq_callback cb = { ptrwatch_showwnd };
	cq_call(cb, (void *)SW_SHOW, cq_no_callback);
}

void ptrwatch_create(HWND parent) {
	HWND wnd;
	cq_callback cb;

	wnd = win32_make_child_window_ex(
		parent, PTRWATCH_CLASS, PTRWATCH_NAME,
		oppai_instance, ptrwatch_wndproc,
		LoadIconA(oppai_instance, MAKEINTRESOURCEA(IDI_ICON1)),
		640, 480, SW_HIDE, 0,
		WS_OVERLAPPEDWINDOW
	);

	cb.func_ptr = ptrwatch_setwnd;
	cq_call(cb, (void *)wnd, cq_no_callback);
	maple_update_func(ptrwatch_onupdate);
	ptrwatch_refreshclicked(ptrwatch_wnd);
}

void ptrwatch_destroy() {
	cq_callback cb = { ptrwatch_destroywnd };
	cq_call(cb, NULL, cq_no_callback);
	UnregisterClassA(PTRWATCH_CLASS, oppai_instance);
}