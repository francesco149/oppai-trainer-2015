/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of Oppai Trainer 2015.
	Oppai Trainer 2015 is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	Oppai Trainer 2015 is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with Oppai Trainer 2015. If not, see <http://www.gnu.org/licenses/>.
*/

#include "itemfilter.h"

#include <stdio.h>
#include <assert.h>
#include <hackdesu/ini.h>
#include "globals.h"
#include "resource.h"
#include "win32_gui.h"
#include "maple_hacks.h"

#define ITEMFILTER_CLASS "OppaiTrainer2015ItemFilter"
#define ITEMFILTER_NAME "Item Filter"

#define ITEMFILTER_W 417
#define ITEMFILTER_H 276

#define IDC_LIST_ITEMS 101
#define IDC_CHECK_ENABLE 102
#define IDC_COMBO_MODE 103
#define IDC_LABEL_MESO 104
#define IDC_EDIT_MESO 105
#define IDC_BUTTON_ADD 106
#define IDC_EDIT_ITEMID 107
#define IDC_BUTTON_REMOVE 108
#define IDC_BUTTON_CLEAR 109

extern HINSTANCE oppai_instance;
static HWND itemfilter_wnd = NULL;

static void itemfilter_add(HWND wnd, char *buf);
static void itemfilter_handlecheckbox(HWND wnd, int cb);

/* <settings> */
static const char *sect = "itemfilter";

void itemfilter_savesettings() {
	RECT r;
	char buf[16] = { 0 };
	char buf2[32] = { 0 };
	long itemcount = 0;
	HWND lv = NULL;
	int i;

#if _DEBUG
	puts("Saving itemfilter settings...");
#endif

	if (!GetWindowRect(itemfilter_wnd, &r)) {
		win32_show_error("GetWindowRect failed for itemfilter_wnd");
	}
	else if (r.left < 32767 && r.left > -32000 && r.top < 32767 && r.top > -32000) { /* should prevent invalid positions from saving */
		ini_write_int(sect, "x", r.left, OPPAI_SETTINGS);
		ini_write_int(sect, "y", r.top, OPPAI_SETTINGS);
	}
	ini_write_check(sect, "enable", itemfilter_wnd, IDC_CHECK_ENABLE, OPPAI_SETTINGS);
	ini_write_edit(sect, "meso", itemfilter_wnd, IDC_EDIT_MESO, OPPAI_SETTINGS);
	ini_write_combo(sect, "mode", itemfilter_wnd, IDC_COMBO_MODE, OPPAI_SETTINGS);

	/* save item list */
	do {
		lv = GetDlgItem(itemfilter_wnd, IDC_LIST_ITEMS);
		if (!lv) {
			win32_show_error("GetDlgItem failed for items listview");
			break;
		}

		itemcount = ListView_GetItemCount(lv);
		ini_write_int(sect, "count", itemcount, OPPAI_SETTINGS);

		for (i = 0; i < itemcount; i++) {
			ListView_GetItemText(lv, i, 0, buf, 16);
			assert(strlen(buf) > 0);
			sprintf_s(buf2, 32, "item%d", i);
			ini_write_string(sect, buf2, buf, OPPAI_SETTINGS);
		}
	} while (0);
#if _DEBUG
	puts("Itemfilter saved!");
#endif
}

void itemfilter_loadsettings() {
	POINT pos;
	int count;
	int i;
	char buf[16] = { 0 };
	char buf2[32] = { 0 };

#if _DEBUG
	puts("Loading itemfilter settings...");
#endif
	pos.x = ini_get_int(sect, "x", CW_USEDEFAULT, OPPAI_SETTINGS);
	pos.y = ini_get_int(sect, "y", CW_USEDEFAULT, OPPAI_SETTINGS);

	if (pos.x != CW_USEDEFAULT && pos.y != CW_USEDEFAULT) {
		SetWindowPos(itemfilter_wnd, NULL, pos.x, pos.y, 0, 0,
			SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOZORDER);
	}

	ini_get_check(sect, "enable", itemfilter_wnd, IDC_CHECK_ENABLE, OPPAI_SETTINGS);
	ini_get_edit(sect, "meso", itemfilter_wnd, IDC_EDIT_MESO, OPPAI_SETTINGS);
	ini_get_combo(sect, "mode", itemfilter_wnd, IDC_COMBO_MODE, OPPAI_SETTINGS);

	count = ini_get_int(sect, "count", 0, OPPAI_SETTINGS);
	for (i = 0; i < count; i++) {
		sprintf_s(buf2, 32, "item%d", i);
		ini_get_string(sect, buf2, "", buf, 16, OPPAI_SETTINGS);
		if (!strlen(buf)) {
#if _DEBUG
			printf("ini_getstring failed for [%s] %s\n", sect, buf2);
#endif
			continue;
		}
		itemfilter_add(itemfilter_wnd, buf);
	}

	/* force all checkboxes to be processed so that enabled hacks in the settings will be enabled */
	itemfilter_handlecheckbox(itemfilter_wnd, IDC_CHECK_ENABLE);
#if _DEBUG
	puts("Loaded itemfilter!");
#endif
}
/* </settings> */

static void itemfilter_onsize(HWND wnd) {
	win32_expand(wnd, IDC_LIST_ITEMS, 10, 70);
	win32_dock_bottom(wnd, IDC_CHECK_ENABLE, 20, 40);
	win32_dock_bottom(wnd, IDC_COMBO_MODE, 20, 40);
	win32_dock_bottom(wnd, IDC_LABEL_MESO, 20, 37);
	win32_dock_bottom(wnd, IDC_EDIT_MESO, 20, 40);
	win32_dock_bottom(wnd, IDC_BUTTON_CLEAR, 22, 38);
	win32_dock_bottom(wnd, IDC_BUTTON_REMOVE, 22, 38);
	win32_dock_bottom(wnd, IDC_BUTTON_ADD, 22, 10);
	win32_dock_bottom(wnd, IDC_EDIT_ITEMID, 20, 11);
	win32_expand_horizontal(wnd, IDC_EDIT_ITEMID, 10, 20);
}

static LRESULT itemfilter_initwnd(HWND wnd) {
	HWND w;
	int colindex;
	char buftitle[128] = { 0 };

	sprintf_s(buftitle, 128, "Item Filter | PID: %d", GetCurrentProcessId());
	SetWindowTextA(wnd, buftitle);

	/* TODO: make this virtual */
	w = win32_make_listview(wnd, IDC_LIST_ITEMS, "list_items", 10, 10, 1, 1);
	if (!w) {
		return -1;
	}

	colindex = win32_listview_add_column(w, 0, "Item ID", -1);
	if (colindex == -1) {
		return -1;
	}

	ListView_SetExtendedListViewStyle(w, LVS_EX_FULLROWSELECT);

	w = win32_make_checkbox(wnd, IDC_CHECK_ENABLE, "check_enable", "Enable", 10, 0, 50, 20);
	if (!w) {
		return -1;
	}

	w = win32_make_combobox(wnd, IDC_COMBO_MODE, "combo_mode", 70, 0, 60, 20);
	if (!w) {
		return -1;
	}
	ComboBox_AddString(w, "Accept");
	ComboBox_AddString(w, "Reject");
	ComboBox_SetCurSel(w, 0);

	w = win32_make_static(wnd, IDC_LABEL_MESO, "label_meso", "Meso: ", 140, 0, 38, 22);
	if (!w) {
		return -1;
	}

	w = win32_make_edit_s(wnd, IDC_EDIT_MESO, "edit_meso", "1", 170, 0, 52, 22, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!w) {
		return -1;
	}

	w = win32_make_button(wnd, IDC_BUTTON_CLEAR, "button_clear", "Clear", 230, 0, 52, 22);
	if (!w) {
		return -1;
	}

	w = win32_make_button(wnd, IDC_BUTTON_REMOVE, "button_remove", "Remove Selected", 290, 0, 102, 22);
	if (!w) {
		return -1;
	}

	w = win32_make_button(wnd, IDC_BUTTON_ADD, "button_add", "Add", 9, 0, 52, 22);
	if (!w) {
		return -1;
	}

	w = win32_make_edit_s(wnd, IDC_EDIT_ITEMID, "edit_itemid", "4000001", 70, 0, 52, 22, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!w) {
		return -1;
	}

	itemfilter_onsize(wnd);

	return 0;
}

static void itemfilter_toggle(HWND wnd, int enabled) {
	char buf[16] = { 0 };
	int selectedindex;
	int meso;
	HWND wndcombo;

	if (enabled) {
		wndcombo = GetDlgItem(wnd, IDC_COMBO_MODE);
		if (!wndcombo) {
			win32_show_error("GetDlgItem failed for wndcombo");
			return;
		}
		selectedindex = ComboBox_GetCurSel(wndcombo);
		if (selectedindex == CB_ERR) {
			win32_show_error("ComboBox_GetCurSel failed for wndcombo");
			return;
		}

		if (!GetDlgItemTextA(wnd, IDC_EDIT_MESO, buf, 16)) {
			win32_show_error("GetDlgItemTextA failed for meso");
			return;
		}

		if (!strlen(buf)) {
			MessageBoxA(wnd, "Empty meso value", ITEMFILTER_NAME, MB_OK | MB_ICONERROR);
			return;
		}

		if (sscanf_s(buf, "%lu", &meso) != 1) {
			MessageBoxA(wnd, "Invalid meso value", ITEMFILTER_NAME, MB_OK | MB_ICONERROR);
			return;
		}

		maple_itemfilter_setmode(selectedindex);
		maple_itemfilter_setmeso(meso);
	}

	hack_toggle(&maple_itemfilter, enabled);
}

static void itemfilter_add(HWND wnd, char *buf) {
	char buf2[16] = { 0 };
	uint32_t itemid;
	LVITEMA item = { 0 }; /* listview item struct for item insertion */
	HWND lv = NULL; /* listview wnd */
	int i;
	int count;

	if (sscanf_s(buf, "%lu", &itemid) != 1) {
		MessageBoxA(wnd, "Invalid Item ID", ITEMFILTER_NAME, MB_OK | MB_ICONERROR);
		return;
	}

	if (!itemid) {
		MessageBoxA(wnd, "0 is not a valid item id", ITEMFILTER_NAME, MB_OK | MB_ICONERROR);
		return;
	}

	lv = GetDlgItem(wnd, IDC_LIST_ITEMS);
	if (!lv) {
		win32_show_error("GetDlgItem failed for lv");
		return;
	}

	count = ListView_GetItemCount(lv);

	for (i = 0; i < count; i++) {
		ListView_GetItemText(lv, i, 0, buf2, 16);

		if (strcmp(buf, buf2) == 0) {
			MessageBoxA(wnd, "Your item is already in the list.", ITEMFILTER_NAME, MB_OK | MB_ICONERROR);
			return;
		}
	}

	item.mask = LVIF_TEXT;
	item.iItem = 0x7FFFFFFF;
	item.cchTextMax = 16;
	item.pszText = buf;

	if (ListView_InsertItem(lv, &item) == -1) {
		win32_show_error("ListView_InsertItem failed");
	}

	maple_itemfilter_add(itemid);
}

/* triggered by "Add" */
static void itemfilter_addclicked(HWND wnd) {
	char buf[16] = { 0 };

	if (!GetDlgItemTextA(wnd, IDC_EDIT_ITEMID, buf, 16)) {
		win32_show_error("GetDlgItemTextA failed for itemid");
		return;
	}

	if (!strlen(buf)) {
		MessageBoxA(wnd, "Empty Item ID", ITEMFILTER_NAME, MB_OK | MB_ICONERROR);
		return;
	}

	itemfilter_add(wnd, buf);
}

static void itemfilter_removeclicked(HWND wnd) {
	HWND lv = NULL;
	int selected, count;
	uint32_t itemid;
	char buf[16] = { 0 };

	lv = GetDlgItem(wnd, IDC_LIST_ITEMS);
	if (!lv) {
		win32_show_error("GetDlgItem failed for lv");
		return;
	}

	selected = ListView_GetNextItem(lv, -1, LVNI_SELECTED);
	if (selected < 0) {
		MessageBoxA(wnd, "No item selected.", ITEMFILTER_NAME, MB_OK | MB_ICONERROR);
		return;
	}

	ListView_GetItemText(lv, selected, 0, buf, 16);
	assert(strlen(buf) > 0);

	count = sscanf_s(buf, "%lu", &itemid);
	assert(count == 1);
	assert(itemid != 0);

	if (!ListView_DeleteItem(lv, selected)) {
		win32_show_error("ListView_DeleteItem failed");
		return;
	}

	maple_itemfilter_remove(itemid);
}

static void itemfilter_clearclicked(HWND wnd) {
	HWND lv = NULL;
	lv = GetDlgItem(wnd, IDC_LIST_ITEMS);
	if (!lv) {
		win32_show_error("GetDlgItem failed for lv");
		return;
	}
	maple_itemfilter_clear();
	ListView_DeleteAllItems(lv);
}

/* triggered when any checkbox is clicked */
static void itemfilter_handlecheckbox(HWND wnd, int cb) {
	HWND wndcheck;
	int checked;

	wndcheck = GetDlgItem(wnd, cb);
	if (!wndcheck) {
		win32_show_error("GetDlgItem failed for wndcheck");
		return;
	}
	checked = Button_GetCheck(wndcheck) == BST_CHECKED;

	switch (cb) {
	case IDC_CHECK_ENABLE:
		itemfilter_toggle(wnd, checked);
		break;

#if _DEBUG
	default:
		printf("Unhandled checkbox message for control id %d\n", cb);
#endif
	}
}

static LRESULT CALLBACK itemfilter_wndproc(HWND wnd, UINT msgid, WPARAM wparam, LPARAM lparam) {
	MINMAXINFO* mmi;

	switch (msgid) {
	case WM_CREATE:
		return itemfilter_initwnd(wnd);

	case WM_CLOSE:
		ShowWindow(wnd, SW_HIDE);
		break;

	case WM_SIZE:
		itemfilter_onsize(wnd);
		break;

	case WM_COMMAND:
		switch (LOWORD(wparam)) {
		case IDC_CHECK_ENABLE:
			itemfilter_handlecheckbox(wnd, LOWORD(wparam));
			break;

		case IDC_BUTTON_ADD:
			itemfilter_addclicked(wnd);
			break;

		case IDC_BUTTON_REMOVE:
			itemfilter_removeclicked(wnd);
			break;

		case IDC_BUTTON_CLEAR:
			itemfilter_clearclicked(wnd);
			break;
		}
		break;

	case WM_GETMINMAXINFO:
		mmi = (MINMAXINFO *)lparam;
		mmi->ptMinTrackSize.x = ITEMFILTER_W;
		mmi->ptMinTrackSize.y = ITEMFILTER_H;
		return 0;

	default:
		return DefWindowProcA(wnd, msgid, wparam, lparam);
	}

	return 0;
}

void itemfilter_show() {
	if (itemfilter_wnd) {
		ShowWindow(itemfilter_wnd, SW_SHOW);
	}
}

void itemfilter_create(HWND parent) {
	itemfilter_wnd = win32_make_child_window_ex(
		parent, ITEMFILTER_CLASS, ITEMFILTER_NAME,
		oppai_instance, itemfilter_wndproc,
		LoadIconA(oppai_instance, MAKEINTRESOURCEA(IDI_ICON1)),
		ITEMFILTER_W, ITEMFILTER_H, SW_HIDE, 0,
		WS_OVERLAPPEDWINDOW
	);
}

void itemfilter_destroy() {
	if (itemfilter_wnd) {
		DestroyWindow(itemfilter_wnd);
	}
	UnregisterClassA(ITEMFILTER_CLASS, oppai_instance);
}
