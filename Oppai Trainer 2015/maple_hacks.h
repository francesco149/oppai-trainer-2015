/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of Oppai Trainer 2015.
	Oppai Trainer 2015 is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	Oppai Trainer 2015 is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with Oppai Trainer 2015. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAPLE_HACKS_H
#define MAPLE_HACKS_H

#include <stdint.h>
#include <windows.h>

typedef struct taghack hack;
typedef enum tagkeypresstype keypresstype;

/* find the current process' maple window */
HWND maple_window();

/* must be called on startup to initialize the random hwid hack */
void hwid_tryinit();

/* the given callback will be called on each update when update is hooked. */
void maple_update_func(__in void (*callback)());

/* toggles the hack on and off. returns 0 on failure. */
int hack_toggle(__inout hack *p, int enabled);

/* installs the update hook. must be enabled for any other hack to function. */
int maple_hookupdate(int enabled);

#if 0
/* bypasses black cipher if it isn't already bypassed. returns 1 on success. */
int maple_blackcipher_trybypass();
#endif

/* prevents maple from creating crash reports */
void maple_blockcrashreports();

/* enqueues a teleport to the given position */
int maple_teleport(long x, long y);

/* toggles kami on and off */
int maple_kami(int enabled, long rangex, long rangey, keypresstype kptype);

/* sets the number of items after which Kami Loot will loot */
void maple_itemhook_setitems(int32_t count);

/* forces kami loot to stop */
void maple_kami_stoploot();

/* toggles mouse fly on and off */
int maple_mousefly(int enabled);

/* toggles slow tubi */
int maple_slowtubi(int enabled);

/* sets the minimum meso amount for item filter */
void maple_itemfilter_setmeso(uint32_t meso);

/* sets the itemfilter mode (0 = accept, 1 = reject) */
void maple_itemfilter_setmode(int mode);

/* adds an item to the item filter list */
void maple_itemfilter_add(uint32_t itemid);

/* removes an item from the item filter list */
void maple_itemfilter_remove(uint32_t itemid);

/* clears the item filter list */
void maple_itemfilter_clear();

/* sets autologin info */
void maple_autologinsettings(const char *user, const char *pass, int32_t ch, int32_t w, const char *pic, int32_t chara);

extern hack
	maple_tubi, 
	maple_nokb,
	maple_fullgodmode,
	maple_timedgodmode, 
	maple_fullmobdisarm,
	maple_lemmings,
	maple_skiplogo,
	maple_randomizehwid, 
	maple_cpuhack,
	maple_magicgodmode,
	maple_instadrop,
	maple_jda,
	maple_viewswears,
	maple_reactordem,
	maple_nofa,
	maple_ua, 
	maple_itemfilter, 
	maple_suckleft, 
	maple_swimhack, 
	maple_airchecks, 
	maple_nocodex, 
	maple_noblueboxes, 
	maple_itemhook, 
	maple_autologin;

#endif
