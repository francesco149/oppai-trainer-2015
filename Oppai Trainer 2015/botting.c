/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of Oppai Trainer 2015.
	Oppai Trainer 2015 is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	Oppai Trainer 2015 is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with Oppai Trainer 2015. If not, see <http://www.gnu.org/licenses/>.
*/

#include "botting.h"

#include <stdio.h>
#include <hackdesu/ini.h>
#include "globals.h"
#include "resource.h"
#include "win32_gui.h"
#include "maple_macros.h"
#include "maple_hacks.h"

#define BOTTING_CLASS "OppaiTrainer2015Botting"
#define BOTTING_NAME "Botting"

#define GROUP_BOTTING_MARGINW 10
#define GROUP_BOTTING_MARGINH 10
#define GROUP_BOTTING_W 215
#define GROUP_BOTTING_H 306
#define CHECK_AUTOATTACK_X (GROUP_BOTTING_MARGINW + 14)
#define CHECK_AUTOATTACK_Y (GROUP_BOTTING_MARGINH + 14 + 4)

#define GROUP_AUTOCC_W GROUP_BOTTING_W
#define GROUP_AUTOCC_H 122
#define CHECK_AUTOCC_ENABLE_X (CHECK_AUTOATTACK_X + GROUP_BOTTING_W + GROUP_BOTTING_MARGINW)
#define CHECK_AUTOCC_ENABLE_Y CHECK_AUTOATTACK_Y

#define GROUP_AUTOLOGIN_W GROUP_BOTTING_W
#define GROUP_AUTOLOGIN_H (GROUP_BOTTING_H - GROUP_AUTOCC_H - GROUP_BOTTING_MARGINH)
#define CHECK_AUTOLOGIN_X CHECK_AUTOCC_ENABLE_X
#define CHECK_AUTOLOGIN_Y (CHECK_AUTOCC_ENABLE_Y + GROUP_AUTOCC_H + GROUP_BOTTING_MARGINH)

#define BOTTING_W (GROUP_BOTTING_W + GROUP_AUTOCC_W + GROUP_BOTTING_MARGINW * 3 + 6)
#define BOTTING_H (GROUP_BOTTING_H + GROUP_BOTTING_MARGINH * 2 + 28)

#define IDC_GROUP_BOTTING 301
#define IDC_CHECK_AUTOATTACK 302
#define IDC_COMBO_AUTOATTACK 303
#define IDC_CHECK_AUTOLOOT 304
#define IDC_CHECK_AUTOSKILL1 305
#define IDC_EDIT_AUTOSKILL1 306
#define IDC_CHECK_AUTOSKILL2 307
#define IDC_EDIT_AUTOSKILL2 308
#define IDC_CHECK_AUTOSKILL3 309
#define IDC_EDIT_AUTOSKILL3 310
#define IDC_CHECK_AUTOSKILL4 311
#define IDC_EDIT_AUTOSKILL4 312
#define IDC_CHECK_AUTOSKILL5 313
#define IDC_EDIT_AUTOSKILL5 314
#define IDC_CHECK_AUTOSKILL6 315
#define IDC_EDIT_AUTOSKILL6 316
#define IDC_CHECK_AUTOSKILL7 317
#define IDC_EDIT_AUTOSKILL7 318
#define IDC_CHECK_AUTOSKILL8 319
#define IDC_EDIT_AUTOSKILL8 320
#define IDC_CHECK_AUTOHP 321
#define IDC_EDIT_AUTOHP 322
#define IDC_CHECK_AUTOMP 323
#define IDC_EDIT_AUTOMP 324

#define IDC_GROUP_AUTOCC 401
#define IDC_CHECK_AUTOCC_ENABLE 402
#define IDC_CHECK_AUTOCC_PEOPLE 403
#define IDC_CHECK_AUTOCC_MOBS 404
#define IDC_CHECK_AUTOCC_TIMED 405
#define IDC_EDIT_AUTOCC_PEOPLE 406
#define IDC_EDIT_AUTOCC_MOBS 407
#define IDC_EDIT_AUTOCC_TIME 408

#define IDC_GROUP_AUTOLOGIN 501
#define IDC_CHECK_AUTOLOGIN 502
#define IDC_EDIT_USER 503
#define IDC_EDIT_PASS 504
#define IDC_COMBO_SERVER 505
#define IDC_COMBO_CHANNEL 506
#define IDC_COMBO_CHARACTER 507
#define IDC_STATIC_PIC 508
#define IDC_EDIT_PIC 509

extern HINSTANCE oppai_instance;
static HWND botting_wnd = NULL;

static void botting_handlecheckbox(HWND wnd, int cb);

/* <settings> */
static const char *sect = "botting";

static const ini_control checkboxes[] = {
	{ "autoattack", IDC_CHECK_AUTOATTACK },
	{ "autoloot", IDC_CHECK_AUTOLOOT },
	{ "autoskill1", IDC_CHECK_AUTOSKILL1 },
	{ "autoskill2", IDC_CHECK_AUTOSKILL2 },
	{ "autoskill3", IDC_CHECK_AUTOSKILL3 },
	{ "autoskill4", IDC_CHECK_AUTOSKILL4 },
	{ "autoskill5", IDC_CHECK_AUTOSKILL5 },
	{ "autoskill6", IDC_CHECK_AUTOSKILL6 },
	{ "autoskill7", IDC_CHECK_AUTOSKILL7 },
	{ "autoskill8", IDC_CHECK_AUTOSKILL8 },
	{ "autohp", IDC_CHECK_AUTOHP },
	{ "automp", IDC_CHECK_AUTOMP },
	{ "autocc", IDC_CHECK_AUTOCC_ENABLE }, 
	{ "autocc_people", IDC_CHECK_AUTOCC_PEOPLE },
	{ "autocc_mobs", IDC_CHECK_AUTOCC_MOBS },
	{ "autocc_timed", IDC_CHECK_AUTOCC_TIMED }, 
	{ "autologin", IDC_CHECK_AUTOLOGIN }, 
	{ NULL, 0 },
};

static const ini_control edits[] = {
	{ "autoskill1_delay", IDC_EDIT_AUTOSKILL1 },
	{ "autoskill2_delay", IDC_EDIT_AUTOSKILL2 },
	{ "autoskill3_delay", IDC_EDIT_AUTOSKILL3 },
	{ "autoskill4_delay", IDC_EDIT_AUTOSKILL4 },
	{ "autoskill5_delay", IDC_EDIT_AUTOSKILL5 },
	{ "autoskill6_delay", IDC_EDIT_AUTOSKILL6 },
	{ "autoskill7_delay", IDC_EDIT_AUTOSKILL7 },
	{ "autoskill8_delay", IDC_EDIT_AUTOSKILL8 },
	{ "autohp_value", IDC_EDIT_AUTOHP },
	{ "automp_value", IDC_EDIT_AUTOMP },
	{ "autocc_people_count", IDC_EDIT_AUTOCC_PEOPLE },
	{ "autocc_mobs_count", IDC_EDIT_AUTOCC_MOBS },
	{ "autocc_timed_time", IDC_EDIT_AUTOCC_TIME },
	{ "autologin_user", IDC_EDIT_USER }, 
	{ "autologin_pass", IDC_EDIT_PASS },
	{ "autologin_pic", IDC_EDIT_PIC },
	{ NULL, 0 },
};

void botting_savesettings() {
	RECT r;

#if _DEBUG
	puts("Saving botting settings...");
#endif

	if (!GetWindowRect(botting_wnd, &r)) {
		win32_show_error("GetWindowRect failed for botting_wnd");
	}
	else if (r.left < 32767 && r.left > -32000 && r.top < 32767 && r.top > -32000) { /* should prevent invalid positions from saving */
		ini_write_int(sect, "x", r.left, OPPAI_SETTINGS);
		ini_write_int(sect, "y", r.top, OPPAI_SETTINGS);
	}
	ini_write_checks(sect, botting_wnd, checkboxes, OPPAI_SETTINGS);
	ini_write_edits(sect, botting_wnd, edits, OPPAI_SETTINGS);
	ini_write_combo(sect, "autoattack_type", botting_wnd, IDC_COMBO_AUTOATTACK, OPPAI_SETTINGS);
	ini_write_combo(sect, "autologin_server", botting_wnd, IDC_COMBO_SERVER, OPPAI_SETTINGS);
	ini_write_combo(sect, "autologin_channel", botting_wnd, IDC_COMBO_CHANNEL, OPPAI_SETTINGS);
	ini_write_combo(sect, "autologin_character", botting_wnd, IDC_COMBO_CHARACTER, OPPAI_SETTINGS);
#if _DEBUG
	puts("Botting saved!");
#endif
}

void botting_loadsettings() {
	POINT pos;
	const ini_control *it;

#if _DEBUG
	puts("Loading botting settings...");
#endif
	pos.x = ini_get_int(sect, "x", CW_USEDEFAULT, OPPAI_SETTINGS);
	pos.y = ini_get_int(sect, "y", CW_USEDEFAULT, OPPAI_SETTINGS);

	if (pos.x != CW_USEDEFAULT && pos.y != CW_USEDEFAULT) {
		SetWindowPos(botting_wnd, NULL, pos.x, pos.y, 0, 0,
			SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOZORDER);
	}

	ini_get_checks(sect, botting_wnd, checkboxes, OPPAI_SETTINGS);
	ini_get_edits(sect, botting_wnd, edits, OPPAI_SETTINGS);
	ini_get_combo(sect, "autoattack_type", botting_wnd, IDC_COMBO_AUTOATTACK, OPPAI_SETTINGS);
	ini_get_combo(sect, "autologin_server", botting_wnd, IDC_COMBO_SERVER, OPPAI_SETTINGS);
	ini_get_combo(sect, "autologin_channel", botting_wnd, IDC_COMBO_CHANNEL, OPPAI_SETTINGS);
	ini_get_combo(sect, "autologin_character", botting_wnd, IDC_COMBO_CHARACTER, OPPAI_SETTINGS);

	/* force all checkboxes to be processed so that enabled hacks in the settings will be enabled */
	for (it = checkboxes; it->key; it++) {
		botting_handlecheckbox(botting_wnd, it->ctlid);
	}

#if _DEBUG
	puts("Loaded botting!");
#endif
}
/* </settings> */

static LRESULT botting_initwnd(HWND wnd) {
	HWND group, check, edit, combo, label;
	int w, w2;
	int d;
	char buf[128] = { 0 };
	int i;

	sprintf_s(buf, 128, "Botting | PID: %d", GetCurrentProcessId());
	SetWindowTextA(wnd, buf);

	/* botting groupbox */
	w = (GROUP_BOTTING_W / 12) * 7 - CHECK_AUTOATTACK_X;
	w2 = GROUP_BOTTING_W - w - CHECK_AUTOATTACK_X * 2 - 4;
	group = win32_make_groupbox(
		wnd, IDC_GROUP_BOTTING, "group_botting", "Botting",
		GROUP_BOTTING_MARGINW, GROUP_BOTTING_MARGINH,
		GROUP_BOTTING_W, GROUP_BOTTING_H);
	if (!group) {
		return -1;
	}

	d = 0;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOATTACK, "check_autoattack", "Attack (CTRL)",
		CHECK_AUTOATTACK_X, CHECK_AUTOATTACK_Y, w, 20);
	if (!check) {
		return -1;
	}

	combo = win32_make_combobox(
		wnd, IDC_COMBO_AUTOATTACK, "combo_autoattack",
		CHECK_AUTOATTACK_X * 2 + w, CHECK_AUTOATTACK_Y + d, w2, 20);
	if (!combo) {
		return -1;
	}
	ComboBox_AddString(combo, "Normal");
	ComboBox_AddString(combo, "Combo");
	ComboBox_AddString(combo, "Fast Combo");
	ComboBox_AddString(combo, "Hold");
	ComboBox_SetCurSel(combo, 0);

	d += 23;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOLOOT, "check_autoloot", "Loot (Z)",
		CHECK_AUTOATTACK_X, CHECK_AUTOATTACK_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 23;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOSKILL1, "check_autoskill1", "Skill1 (INS)",
		CHECK_AUTOATTACK_X, CHECK_AUTOATTACK_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_AUTOSKILL1, "edit_autoskill1", "5000",
		CHECK_AUTOATTACK_X * 2 + w, CHECK_AUTOATTACK_Y + d,
		w2, 20, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!edit) {
		return -1;
	}

	d += 23;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOSKILL2, "check_autoskill2", "Skill2 (DEL)",
		CHECK_AUTOATTACK_X, CHECK_AUTOATTACK_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_AUTOSKILL2, "edit_autoskill2", "10000",
		CHECK_AUTOATTACK_X * 2 + w, CHECK_AUTOATTACK_Y + d,
		w2, 20, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!edit) {
		return -1;
	}

	d += 23;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOSKILL3, "check_autoskill3", "Skill3 (HOME)",
		CHECK_AUTOATTACK_X, CHECK_AUTOATTACK_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_AUTOSKILL3, "edit_autoskill3", "20000",
		CHECK_AUTOATTACK_X * 2 + w, CHECK_AUTOATTACK_Y + d,
		w2, 20, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!edit) {
		return -1;
	}

	d += 23;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOSKILL4, "check_autoskill4", "Skill4 (END)",
		CHECK_AUTOATTACK_X, CHECK_AUTOATTACK_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_AUTOSKILL4, "edit_autoskill4", "20000",
		CHECK_AUTOATTACK_X * 2 + w, CHECK_AUTOATTACK_Y + d,
		w2, 20, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!edit) {
		return -1;
	}

	d += 23;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOSKILL5, "check_autoskill5", "Skill5 (0)",
		CHECK_AUTOATTACK_X, CHECK_AUTOATTACK_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_AUTOSKILL5, "edit_autoskill5", "20000",
		CHECK_AUTOATTACK_X * 2 + w, CHECK_AUTOATTACK_Y + d,
		w2, 20, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!edit) {
		return -1;
	}

	d += 23;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOSKILL6, "check_autoskill6", "Skill6 (1)",
		CHECK_AUTOATTACK_X, CHECK_AUTOATTACK_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_AUTOSKILL6, "edit_autoskill6", "20000",
		CHECK_AUTOATTACK_X * 2 + w, CHECK_AUTOATTACK_Y + d,
		w2, 20, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!edit) {
		return -1;
	}

	d += 23;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOSKILL7, "check_autoskill7", "Skill7 (2)",
		CHECK_AUTOATTACK_X, CHECK_AUTOATTACK_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_AUTOSKILL7, "edit_autoskill7", "20000",
		CHECK_AUTOATTACK_X * 2 + w, CHECK_AUTOATTACK_Y + d,
		w2, 20, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!edit) {
		return -1;
	}

	d += 23;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOSKILL8, "check_autoskill8", "Skill8 (3)",
		CHECK_AUTOATTACK_X, CHECK_AUTOATTACK_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_AUTOSKILL8, "edit_autoskill8", "20000",
		CHECK_AUTOATTACK_X * 2 + w, CHECK_AUTOATTACK_Y + d,
		w2, 20, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!edit) {
		return -1;
	}

	d += 23;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOHP, "check_autohp", "HP (PAGE UP)",
		CHECK_AUTOATTACK_X, CHECK_AUTOATTACK_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_AUTOHP, "edit_autohp", "200",
		CHECK_AUTOATTACK_X * 2 + w, CHECK_AUTOATTACK_Y + d,
		w2, 20, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!edit) {
		return -1;
	}

	d += 23;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOMP, "check_automp", "MP (PAGE DN)",
		CHECK_AUTOATTACK_X, CHECK_AUTOATTACK_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_AUTOMP, "edit_automp", "50",
		CHECK_AUTOATTACK_X * 2 + w, CHECK_AUTOATTACK_Y + d,
		w2, 20, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!edit) {
		return -1;
	}

	group = win32_make_groupbox(
		wnd, IDC_GROUP_AUTOCC, "group_autocc", "Auto CC",
		GROUP_BOTTING_W + GROUP_BOTTING_MARGINW * 2, GROUP_BOTTING_MARGINH,
		GROUP_AUTOCC_W, GROUP_AUTOCC_H);
	if (!group) {
		return -1;
	}

	d = 0;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOCC_ENABLE, "check_enableautocc", "Enable",
		CHECK_AUTOCC_ENABLE_X, CHECK_AUTOCC_ENABLE_Y, w, 20);
	if (!check) {
		return -1;
	}

	d += 23;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOCC_PEOPLE, "check_autoccpeople", "When People >",
		CHECK_AUTOCC_ENABLE_X, CHECK_AUTOCC_ENABLE_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_AUTOCC_PEOPLE, "edit_autoccpeople", "0",
		CHECK_AUTOCC_ENABLE_X + CHECK_AUTOATTACK_X + w, CHECK_AUTOATTACK_Y + d,
		w2, 20, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!edit) {
		return -1;
	}

	d += 23;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOCC_MOBS, "check_autoccmobs", "When Mobs <",
		CHECK_AUTOCC_ENABLE_X, CHECK_AUTOCC_ENABLE_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_AUTOCC_MOBS, "edit_autoccmobs", "1",
		CHECK_AUTOCC_ENABLE_X + CHECK_AUTOATTACK_X + w, CHECK_AUTOATTACK_Y + d,
		w2, 20, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!edit) {
		return -1;
	}

	d += 23;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOCC_TIMED, "check_autocctime", "Every (millisecs)",
		CHECK_AUTOCC_ENABLE_X, CHECK_AUTOCC_ENABLE_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_AUTOCC_TIME, "edit_autocctime", "300000",
		CHECK_AUTOCC_ENABLE_X + CHECK_AUTOATTACK_X + w, CHECK_AUTOATTACK_Y + d,
		w2, 20, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!edit) {
		return -1;
	}

	group = win32_make_groupbox(
		wnd, IDC_GROUP_AUTOLOGIN, "group_autologin", "Auto Login (by Mahorori)",
		GROUP_BOTTING_W + GROUP_BOTTING_MARGINW * 2, GROUP_AUTOCC_H + GROUP_BOTTING_MARGINH * 2,
		GROUP_AUTOLOGIN_W, GROUP_AUTOLOGIN_H);
	if (!group) {
		return -1;
	}

	w = GROUP_BOTTING_W - CHECK_AUTOATTACK_X - 4;
	d = 0;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_AUTOLOGIN, "check_autologin", "Enable",
		CHECK_AUTOLOGIN_X, CHECK_AUTOLOGIN_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 23;
	edit = win32_make_edit_s(
		wnd, IDC_EDIT_USER, "edit_autologinuser", "user",
		CHECK_AUTOLOGIN_X, CHECK_AUTOLOGIN_Y + d,
		w, 20, WS_EDIT_DEFAULT);
	if (!edit) {
		return -1;
	}

	d += 23;
	edit = win32_make_edit_s(
		wnd, IDC_EDIT_PASS, "edit_autologinpass", "password",
		CHECK_AUTOLOGIN_X, CHECK_AUTOLOGIN_Y + d,
		w, 20, WS_EDIT_DEFAULT | ES_PASSWORD);
	if (!edit) {
		return -1;
	}

	d += 23;
	combo = win32_make_combobox(
		wnd, IDC_COMBO_SERVER, "combo_server",
		CHECK_AUTOLOGIN_X, CHECK_AUTOLOGIN_Y + d, w, 20);
	if (!combo) {
		return -1;
	}
	ComboBox_AddString(combo, "Scania");
	ComboBox_AddString(combo, "Bera");
	ComboBox_AddString(combo, "Broa");
	ComboBox_AddString(combo, "Windia");
	ComboBox_AddString(combo, "Khaini");
	ComboBox_AddString(combo, "Bellocan");
	ComboBox_AddString(combo, "Mardia");
	ComboBox_AddString(combo, "Kradia");
	ComboBox_AddString(combo, "Yellonde");
	ComboBox_AddString(combo, "Demethos");
	ComboBox_AddString(combo, "Galicia");
	ComboBox_AddString(combo, "El Nido");
	ComboBox_AddString(combo, "Zenith");
	ComboBox_AddString(combo, "Arcania");
	ComboBox_AddString(combo, "Chaos");
	ComboBox_AddString(combo, "Nova");
	ComboBox_AddString(combo, "Renegades");
	ComboBox_SetCurSel(combo, 0);

	d += 24;
	combo = win32_make_combobox(
		wnd, IDC_COMBO_CHANNEL, "combo_channel",
		CHECK_AUTOLOGIN_X, CHECK_AUTOLOGIN_Y + d, w / 2 - 2, 20);
	if (!combo) {
		return -1;
	}
	for (i = 0; i < 20; i++) {
		sprintf_s(buf, 128, "Channel %d", i + 1);
		ComboBox_AddString(combo, buf);
	}
	ComboBox_SetCurSel(combo, 19);

	combo = win32_make_combobox(
		wnd, IDC_COMBO_CHARACTER, "combo_character",
		CHECK_AUTOLOGIN_X + w / 2 + 1, 
		CHECK_AUTOLOGIN_Y + d, w / 2, 20);
	if (!combo) {
		return -1;
	}
	SetWindowLongPtrA(combo, GWL_STYLE, WS_TABSTOP | WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST | CBS_HASSTRINGS | WS_VSCROLL);
	for (i = 0; i < 32; i++) {
		sprintf_s(buf, 128, "Character %d", i + 1);
		ComboBox_AddString(combo, buf);
	}
	ComboBox_SetCurSel(combo, 0);

	d += 24;
	label = win32_make_static(
		wnd, IDC_STATIC_PIC, "static_pic", "PIC: ",
		CHECK_AUTOLOGIN_X, CHECK_AUTOLOGIN_Y + d + 4, 
		25, 20);
	if (!label) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_PIC, "edit_autologinpic", "12345",
		CHECK_AUTOLOGIN_X + 25 + GROUP_BOTTING_MARGINW, CHECK_AUTOLOGIN_Y + d,
		w - 25 - GROUP_BOTTING_MARGINW, 20, WS_EDIT_DEFAULT | ES_PASSWORD);
	if (!edit) {
		return -1;
	}

	return 0;
}

int botting_getautoattacktype(keypresstype *kptype) {
	int selectedindex;
	HWND wndcombo;

	wndcombo = GetDlgItem(botting_wnd, IDC_COMBO_AUTOATTACK);
	if (!wndcombo) {
		win32_show_error("GetDlgItem failed for wndcombo");
		return 0;
	}
	selectedindex = ComboBox_GetCurSel(wndcombo);
	if (selectedindex == CB_ERR) {
		win32_show_error("ComboBox_GetCurSel failed for wndcombo");
		return 0;
	}

	switch (selectedindex) {
	case 0:
		*kptype = KEYPRESS_PRESSED;
		break;
	case 1:
		*kptype = KEYPRESS_HOLD;
		break;
	case 2:
		*kptype = KEYPRESS_FASTHOLD;
		break;
	case 3:
		*kptype = KEYPRESS_KEEPDOWN;
		break;
	default:
#if _DEBUG
		puts("invalid selected index for attack combobox");
#endif
		return 0;
	}

	return 1;
}

static void botting_toggleautoattack(HWND wnd, int enabled) {
	keypresstype kptype = KEYPRESS_PRESSED;
	if (enabled && !botting_getautoattacktype(&kptype)) {
		return;
	}
	maple_setmacro(VK_CONTROL, MAPLEKEY_CTRL, enabled, 0, kptype);
}

static void botting_toggleautoskill(HWND wnd, uint32_t key, int editcontrol, int enabled) {
	char buf[16] = { 0 };
	uint32_t delay = 0;

	if (enabled) {
		if (!GetDlgItemTextA(wnd, editcontrol, buf, 16)) {
			win32_show_error("GetDlgItemTextA failed for editcontrol");
			return;
		}

		if (sscanf_s(buf, "%lu", &delay) != 1) {
			MessageBoxA(wnd, "Invalid autoskill value.", BOTTING_NAME, MB_OK | MB_ICONERROR);
			return;
		}
	}

#if _DEBUG
	printf("maple_setmacro(%.08X, %d, %lu, KEYPRESS_PRESSED)\n", key, enabled, delay);
#endif

	maple_setmacro(0, key, enabled, delay, KEYPRESS_PRESSED);
}

static void botting_toggleautopot(HWND wnd, uint32_t key, int editcontrol, int enabled) {
	char buf[16] = { 0 };
	uint32_t value = 0;

	if (enabled) {
		if (!GetDlgItemTextA(wnd, editcontrol, buf, 16)) {
			win32_show_error("GetDlgItemTextA failed for editcontrol");
			return;
		}

		if (sscanf_s(buf, "%lu", &value) != 1) {
			MessageBoxA(wnd, "Invalid autopot value.", BOTTING_NAME, MB_OK | MB_ICONERROR);
			return;
		}
	}

	switch (editcontrol) {
	case IDC_EDIT_AUTOHP:
		maple_setautohp(key, enabled, value);
		break;

	case IDC_EDIT_AUTOMP:
		maple_setautomp(key, enabled, value);
		break;
	}
}

static void botting_toggleautocc(HWND wnd, int checked) {
	int flags = AUTOCC_DISABLED;
	HWND wndcheck;
	char buf[16] = { 0 };
	int value;

	if (!checked) {
		maple_setautoccflags(AUTOCC_DISABLED);
		return;
	}

	wndcheck = GetDlgItem(wnd, IDC_CHECK_AUTOCC_PEOPLE);
	if (!wndcheck) {
		win32_show_error("GetDlgItem failed for autocc_people");
		return;
	}
	if (Button_GetCheck(wndcheck) == BST_CHECKED) {
		flags |= AUTOCC_PEOPLE;

		if (!GetDlgItemTextA(wnd, IDC_EDIT_AUTOCC_PEOPLE, buf, 16)) {
			win32_show_error("GetDlgItemTextA failed for autocc_people");
			return;
		}

		if (sscanf_s(buf, "%d", &value) != 1) {
			MessageBoxA(wnd, "Invalid autocc people value.", BOTTING_NAME, MB_OK | MB_ICONERROR);
			return;
		}

		maple_setautoccpeople(value);
	}

	wndcheck = GetDlgItem(wnd, IDC_CHECK_AUTOCC_MOBS);
	if (!wndcheck) {
		win32_show_error("GetDlgItem failed for autocc_mobs");
		return;
	}
	if (Button_GetCheck(wndcheck) == BST_CHECKED) {
		flags |= AUTOCC_MOBCOUNT;

		if (!GetDlgItemTextA(wnd, IDC_EDIT_AUTOCC_MOBS, buf, 16)) {
			win32_show_error("GetDlgItemTextA failed for autocc_mobs");
			return;
		}

		if (sscanf_s(buf, "%d", &value) != 1) {
			MessageBoxA(wnd, "Invalid autocc mobs value.", BOTTING_NAME, MB_OK | MB_ICONERROR);
			return;
		}

		maple_setautoccmobs(value);
	}

	wndcheck = GetDlgItem(wnd, IDC_CHECK_AUTOCC_TIMED);
	if (!wndcheck) {
		win32_show_error("GetDlgItem failed for autocc_timed");
		return;
	}
	if (Button_GetCheck(wndcheck) == BST_CHECKED) {
		flags |= AUTOCC_TIMED;

		if (!GetDlgItemTextA(wnd, IDC_EDIT_AUTOCC_TIME, buf, 16)) {
			win32_show_error("GetDlgItemTextA failed for autocc_time");
			return;
		}

		if (sscanf_s(buf, "%d", &value) != 1) {
			MessageBoxA(wnd, "Invalid autocc time value.", BOTTING_NAME, MB_OK | MB_ICONERROR);
			return;
		}

		maple_setautocctime(value);
	}

	maple_setautoccflags(flags);
}

static void botting_toggleautologin(HWND wnd, int checked) {
	char user[254] = { 0 };
	char pass[254] = { 0 };
	int world = 0;
	int channel = 19;
	int character = 0;
	char pic[16] = { 0 };
	HWND wndcombo = NULL;

	if (checked) {
		if (!GetDlgItemTextA(wnd, IDC_EDIT_USER, user, 254)) {
			win32_show_error("GetDlgItemTextA failed for edit_user");
			return;
		}

		if (!GetDlgItemTextA(wnd, IDC_EDIT_PASS, pass, 254)) {
			win32_show_error("GetDlgItemTextA failed for edit_pass");
			return;
		}

		if (!GetDlgItemTextA(wnd, IDC_EDIT_PIC, pic, 16)) {
			win32_show_error("GetDlgItemTextA failed for edit_pic");
			return;
		}

		wndcombo = GetDlgItem(botting_wnd, IDC_COMBO_SERVER);
		if (!wndcombo) {
			win32_show_error("GetDlgItem failed for combo_server");
			return;
		}
		world = ComboBox_GetCurSel(wndcombo);
		if (world == CB_ERR) {
			win32_show_error("ComboBox_GetCurSel failed for combo_server");
			return;
		}

		wndcombo = GetDlgItem(botting_wnd, IDC_COMBO_CHANNEL);
		if (!wndcombo) {
			win32_show_error("GetDlgItem failed for combo_channel");
			return;
		}
		channel = ComboBox_GetCurSel(wndcombo);
		if (channel == CB_ERR) {
			win32_show_error("ComboBox_GetCurSel failed for combo_channel");
			return;
		}

		wndcombo = GetDlgItem(botting_wnd, IDC_COMBO_CHARACTER);
		if (!wndcombo) {
			win32_show_error("GetDlgItem failed for combo_character");
			return;
		}
		character = ComboBox_GetCurSel(wndcombo);
		if (character == CB_ERR) {
			win32_show_error("ComboBox_GetCurSel failed for combo_character");
			return;
		}

		maple_autologinsettings(user, pass, channel, world, pic, character);
	}

	hack_toggle(&maple_autologin, checked);
}

/* triggered when any checkbox is clicked */
static void botting_handlecheckbox(HWND wnd, int cb) {
	HWND wndcheck;
	int checked;

	wndcheck = GetDlgItem(wnd, cb);
	if (!wndcheck) {
		win32_show_error("GetDlgItem failed for wndcheck");
		return;
	}
	checked = Button_GetCheck(wndcheck) == BST_CHECKED;

	switch (cb) {
	case IDC_CHECK_AUTOATTACK:
		botting_toggleautoattack(wnd, checked);
		break;

	case IDC_CHECK_AUTOLOOT:
		maple_setmacro(0, MAPLEKEY_Z, checked, 0, KEYPRESS_PRESSED);
		break;

	case IDC_CHECK_AUTOSKILL1:
		botting_toggleautoskill(wnd, MAPLEKEY_INSERT, IDC_EDIT_AUTOSKILL1, checked);
		break;

	case IDC_CHECK_AUTOSKILL2:
		botting_toggleautoskill(wnd, MAPLEKEY_DEL, IDC_EDIT_AUTOSKILL2, checked);
		break;

	case IDC_CHECK_AUTOSKILL3:
		botting_toggleautoskill(wnd, MAPLEKEY_HOME, IDC_EDIT_AUTOSKILL3, checked);
		break;

	case IDC_CHECK_AUTOSKILL4:
		botting_toggleautoskill(wnd, MAPLEKEY_END, IDC_EDIT_AUTOSKILL4, checked);
		break;

	case IDC_CHECK_AUTOSKILL5:
		botting_toggleautoskill(wnd, MAPLEKEY_0, IDC_EDIT_AUTOSKILL5, checked);
		break;

	case IDC_CHECK_AUTOSKILL6:
		botting_toggleautoskill(wnd, MAPLEKEY_1, IDC_EDIT_AUTOSKILL6, checked);
		break;

	case IDC_CHECK_AUTOSKILL7:
		botting_toggleautoskill(wnd, MAPLEKEY_2, IDC_EDIT_AUTOSKILL7, checked);
		break;

	case IDC_CHECK_AUTOSKILL8:
		botting_toggleautoskill(wnd, MAPLEKEY_3, IDC_EDIT_AUTOSKILL8, checked);
		break;

	case IDC_CHECK_AUTOHP:
		botting_toggleautopot(wnd, MAPLEKEY_PAGEUP, IDC_EDIT_AUTOHP, checked);
		break;

	case IDC_CHECK_AUTOMP:
		botting_toggleautopot(wnd, MAPLEKEY_PAGEDOWN, IDC_EDIT_AUTOMP, checked);
		break;

	case IDC_CHECK_AUTOCC_ENABLE:
		botting_toggleautocc(wnd, checked);
		break;

	case IDC_CHECK_AUTOLOGIN:
		botting_toggleautologin(wnd, checked);
		break;

#if _DEBUG
	default:
		printf("Unhandled checkbox message for control id %d\n", cb);
#endif
	}
}

static LRESULT CALLBACK botting_wndproc(HWND wnd, UINT msgid, WPARAM wparam, LPARAM lparam) {
	switch (msgid) {
	case WM_CREATE:
		return botting_initwnd(wnd);

	case WM_CLOSE:
		ShowWindow(wnd, SW_HIDE);
		break;

	case WM_COMMAND:
		switch (LOWORD(wparam)) {
		case IDC_CHECK_AUTOATTACK:
		case IDC_CHECK_AUTOLOOT:
		case IDC_CHECK_AUTOSKILL1:
		case IDC_CHECK_AUTOSKILL2:
		case IDC_CHECK_AUTOSKILL3:
		case IDC_CHECK_AUTOSKILL4:
		case IDC_CHECK_AUTOSKILL5:
		case IDC_CHECK_AUTOSKILL6:
		case IDC_CHECK_AUTOSKILL7:
		case IDC_CHECK_AUTOSKILL8:
		case IDC_CHECK_AUTOHP:
		case IDC_CHECK_AUTOMP:
		case IDC_CHECK_AUTOCC_ENABLE:
		case IDC_CHECK_AUTOLOGIN:
			botting_handlecheckbox(wnd, LOWORD(wparam));
			return TRUE;
		}
		break;

	default:
		return DefWindowProcA(wnd, msgid, wparam, lparam);
	}

	return 0;
}

void botting_show() {
	if (botting_wnd) {
		ShowWindow(botting_wnd, SW_SHOW);
	}
}

void botting_create(HWND parent) {
	botting_wnd = win32_make_child_window_ex(
		parent, BOTTING_CLASS, BOTTING_NAME,
		oppai_instance, botting_wndproc,
		LoadIconA(oppai_instance, MAKEINTRESOURCEA(IDI_ICON1)),
		BOTTING_W, BOTTING_H, SW_HIDE, 0,
		WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME ^ WS_MAXIMIZEBOX
	);
}

void botting_destroy() {
	if (botting_wnd) {
		DestroyWindow(botting_wnd);
	}
	UnregisterClassA(BOTTING_CLASS, oppai_instance);
}
