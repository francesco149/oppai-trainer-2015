#include "maple_macros.h"

#include <stdlib.h>
#include <stdio.h>
#include <sys/timeb.h>
#include <hackdesu/hack.h>
#include <hackdesu/command_queue.h>

#include "maple_structs.h"

static int getmillicount() {
	struct timeb tb;
	int res;
	ftime(&tb);
	res = tb.millitm + (tb.time & 0xfffff) * 1000;
	return res;
}

static int getmillispan(int start) {
	int res = getmillicount() - start;
	if (res < 0)
		res += 0x100000 * 1000;
	return res;
}

/* 
 * void __thiscall CField::SendTransferChannelRequest(long)
 * 1st result 6A ? 68 ? ? ? ? ? ? 00 00 00 00 ? 83 EC 14 ? ? ? ? ? ? ? ? ? ? ? 8D ? ? ? ? ? 00 00 00 00 ? ? 8B 0D ? ? ? ? 85 C9 74 ? E8 ? ? ? ? 8B ? ? ? ? ? 6A 00 68 
 * or 2nd call at 74 ? ? E8 ? ? ? ? 8B C8 E8 ? ? ? ? ? ? ? ? 00 00 00 75 ? ? ? ? 00 ? ? ? ? 00 00 ? ? ? ? 00 00 01 00 00 00 74 ? 8B ? E8 ? ? ? ? ? ? C2 04 00
 */
typedef void(__fastcall *pfnCField__SendTransferChannelRequest)(void *pthis, void *edx, long ch);
static pfnCField__SendTransferChannelRequest CField__SendTransferChannelRequest = addrT(pfnCField__SendTransferChannelRequest, 007934D0); /* v161.1 */

#define AUTOCC_DISABLED 0
#define AUTOCC_PEOPLE	(1 << 0)
#define AUTOCC_MOBCOUNT (1 << 1)
#define AUTOCC_TIMED	(1 << 2)

typedef enum tagautoccstate {
	AUTOCC_IDLE, 
	AUTOCC_BREATHING, 
	AUTOCC_SENDING, 
} autoccstate;

static int autocc_last = 0;
static int autocc_people = 0;
static int autocc_mobs = 1;
static int autocc_time = 300000;
static int32_t autocc_mode = AUTOCC_DISABLED;
static int autocc_targetch = -1;
static autoccstate autocc_status = AUTOCC_IDLE;
static int breathend = 0;
static int autocc_packetsent = 0;

void setccflags(void *param) {
	int32_t flags = (int32_t)param;
	if (flags != AUTOCC_DISABLED) {
		autocc_last = getmillicount();
	}
	autocc_mode = flags;
	autocc_targetch = -1;
	autocc_status = AUTOCC_IDLE;
	breathend = 0;
	autocc_packetsent = 0;
}

/*
	sets the autocc mode, which is a bitfield that uses the following flags:
	AUTOCC_DISABLED: default value for disabled autocc
	AUTOCC_PEOPLE: cc when people > something
	AUTOCC_MOBCOUNT: cc when mobs < something
	AUTOCC_TIMED: cc every x milliseconds
*/
void maple_setautoccflags(int32_t flags) {
	cq_callback cb = { setccflags };
	cq_call(cb, (void *)flags, cq_no_callback);
}

void maple_setautoccpeople(int v) { autocc_people = v; }
void maple_setautoccmobs(int v) { autocc_mobs = v; }
void maple_setautocctime(int v) { autocc_time = v; }

int maple_isautoccing(int enqueue) { 
	if (enqueue) {
		autoccstate ccing = 0;
		cq_read4(&ccing, &autocc_status);
		return ccing != AUTOCC_IDLE;
	}

	return autocc_status != AUTOCC_IDLE;
}

static void maple_handleautocc() {
	int shouldcc = 0;
	CUserPool *puserpool = NULL;
	CMobPool *pmobpool = NULL;
	CField *pfield = NULL;
	CWvsContext *pcontext = NULL;
	CUserLocal *puser = NULL;

	if (autocc_mode == AUTOCC_DISABLED) {
		return;
	}
	
	switch (autocc_status) {
	/* ------------------------------------------------------------------------------------------- */
	case AUTOCC_IDLE:
		if (!CUserLocal__GetInstance()) {
			autocc_last = getmillicount();
			return;
		}

		do {
			if (autocc_mode & AUTOCC_PEOPLE) {
				puserpool = CUserPool__GetInstance();
				if (puserpool) {
					shouldcc = puserpool->count > autocc_people;
					if (shouldcc) {
#if _DEBUG
						printf("ccing because people = %d > %d\n", puserpool->count, autocc_people);
#endif
						break;
					}
				}
			}

			if (autocc_mode & AUTOCC_MOBCOUNT) {
				pmobpool = CMobPool__GetInstance();
				if (pmobpool) {
					shouldcc = pmobpool->size < autocc_mobs;
					if (shouldcc) {
#if _DEBUG
						printf("ccing because mobs = %d > %d\n", pmobpool->size, autocc_mobs);
#endif
						break;
					}
				}
			}

			if (autocc_mode & AUTOCC_TIMED) {
				shouldcc = getmillispan(autocc_last) >= autocc_time;
				if (shouldcc) {
#if _DEBUG
					printf("ccing because %dms have passed\n", autocc_time);
#endif
					break;
				}
			}
		} while (0);

		if (shouldcc) {
			autocc_status = AUTOCC_BREATHING;
		}
		break;

	/* ------------------------------------------------------------------------------------------- */
	case AUTOCC_BREATHING:
		puser = CUserLocal__GetInstance();
		if (!puser) {
#if _DEBUG
			puts("invalid charbase");
#endif
			return;
		}

		if (puser->breath) {
#if _DEBUG
			puts("autocc is waiting for breath...");
#endif
			breathend = 0;
			return;
		}

		if (!breathend) {
			breathend = getmillicount();
#if _DEBUG
			puts("done breathing...");
#endif
		}

		if (getmillispan(breathend) < 1000) {
#if _DEBUG
			puts("autocc is waiting after breath...");
#endif
			return;
		}

		pcontext = CWvsContext__GetInstance();
		if (!pcontext) {
#if _DEBUG
			puts("invalid context ptr");
#endif
			return;
		}
		autocc_targetch = (pcontext->channel + 1) % 20;
#if _DEBUG
		printf("target channel: %d\n", autocc_targetch);
#endif
		autocc_status = AUTOCC_SENDING;
		break;

	/* ------------------------------------------------------------------------------------------- */
	case AUTOCC_SENDING:
		pcontext = CWvsContext__GetInstance();
		if (!pcontext) {
#if _DEBUG
			puts("invalid context ptr");
#endif
			return;
		}
		if (pcontext->channel == autocc_targetch) {
#if _DEBUG
			puts("done ccing");
#endif
			autocc_status = AUTOCC_IDLE;
			breathend = 0;
			autocc_packetsent = 0;
			autocc_targetch = -1;
			autocc_last = getmillicount();
		}

		if (getmillispan(autocc_packetsent) >= 1000) {
			pfield = get_field();
			if (!pfield) {
#if _DEBUG
				puts("invalid field ptr");
#endif
				return;
			}
#if _DEBUG
			puts("sending autocc packet");
#endif
			CField__SendTransferChannelRequest(pfield, NULL, autocc_targetch);
			autocc_packetsent = getmillicount();
		}
		break;
	}
}

/*
 * virtual void __thiscall CWndMan::OnKey(unsigned int unknown, unsigned int key)
 * ? ? ? ? ? 85 ? 74 ? 8D ? ? 8B ? 8B ? FF ? C2 08 00
 */
typedef void(__fastcall *pfnCWndMan__OnKey)(__inout void *pthis, void *edx, uint32_t unknown, uint32_t key);
static pfnCWndMan__OnKey CWndMan__OnKey = addrT(pfnCWndMan__OnKey, 0163E160); /* v161.1 */
#if _DEBUG
static void __fastcall CWndMan__OnKey_hook(__inout void *pthis, void *edx, uint32_t unknown, uint32_t key) {
	printf("CWndMan__OnKey(0x%.08X, 0x%.08X)\n", unknown, key);
	CWndMan__OnKey(pthis, edx, unknown, key);
}
static detour debugkeyhook = { (void **)&CWndMan__OnKey, (void *)CWndMan__OnKey_hook };
static generic_hack hdebugkeyhook[] = {
	{ HACK_DETOUR, &debugkeyhook }
};
hack maple_debugkeyhook = { 1, hdebugkeyhook };
#endif

static void scheduled_presskey(__in void *param) {
	CWndMan *wndman;

	wndman = CWndMan__GetInstance();
	if (!wndman) {
#if _DEBUG
		puts("null wndman");
#endif
		return;
	}
	CWndMan__OnKey(wndman, NULL, 0, (unsigned int)param);
}

int maple_presskey(unsigned int key) {
	cq_callback cq = { scheduled_presskey };
	return cq_call(cq, (void *)key, cq_no_callback);
}

typedef struct tagmacro {
	uint32_t key;
	keypresstype type;
	int lastpressed;
	uint32_t delay;
	int enabled;
	unsigned int virtual_key;
} macro;

#define MAX_MACROS 0xFF
static macro macros[MAX_MACROS] = { 0 };
static macro autohp = { 0 };
static macro automp = { 0 };
static macro *exclusivemacro = NULL;
static int exclusivemacro_count = 0;

static void performsetmacro(void *param) {
	macro *m;
	uint8_t scancode;
	m = (macro *)param;
	m->key = m->key & 0x0FFF0000;
	scancode = (uint8_t)(m->key >> 16);
	macros[scancode] = *m;
	/* TODO: overhaul this code to do it the proper way as Waty & Shadow explained */

	if (!m->enabled && exclusivemacro && exclusivemacro->key == m->key) {
#if _DEBUG
		printf("macro for key %.08X was disabled while being pressed so it will be stopped", m->key);
#endif
		exclusivemacro = NULL;
		exclusivemacro_count = 0;
	}
}

static void deletesetmacro(void *param) {
	macro *m;
	m = (macro *)param;
	free(m);
}

/* enables or disable the given key's macro (type and delay are ignored when enabled is 0) */
int maple_setmacro(unsigned int virtual_key, unsigned int key, int enabled, uint32_t delay, keypresstype type) {
	macro *m;
	cq_callback perf_cb, del_cb;

	m = (macro *)malloc(sizeof(macro));
	memset(m, 0, sizeof(m));
	m->key = key;
	m->enabled = enabled;
	m->delay = delay;
	m->type = type;
	m->virtual_key = virtual_key;

	/* TODO: overhaul this code to do it the proper way as Waty & Shadow explained */
	perf_cb.func_ptr = performsetmacro;
	del_cb.func_ptr = deletesetmacro;
	return cq_call(perf_cb, m, del_cb);
}

/* returns 1 if the macro for the given key is enabled */
int maple_macroenabled(unsigned int key) {
	/* TODO: make this thread safe */
	uint8_t scancode;
	key = key & 0x0FFF0000;
	scancode = (uint8_t)(key >> 16);
	return macros[scancode].enabled;
}

#define AUTOPOT_HP 0
#define AUTOPOT_MP 1

static void performsetautopot(void *param) {
	macro *m;
	m = (macro *)param;
	m->key = m->key & 0x0FFF0000;
	switch (m->lastpressed) {
	case AUTOPOT_HP:
		autohp = *m;
		break;
	case AUTOPOT_MP:
		automp = *m;
		break;
#if _DEBUG
	default:
		puts("unknown autopot type");
#endif
	}
}

static void deletesetautopot(void *param) {
	macro *m;
	m = (macro *)param;
	free(m);
}

static int setautopot(int type, unsigned int key, int enabled, uint32_t value) {
	macro *m;
	cq_callback perf_cb, del_cb;

	m = (macro *)malloc(sizeof(macro));
	memset(m, 0, sizeof(m));
	m->key = key;
	m->enabled = enabled;
	m->delay = value;
	m->type = KEYPRESS_PRESSED;
	m->lastpressed = type;

	perf_cb.func_ptr = performsetautopot;
	del_cb.func_ptr = deletesetautopot;
	return cq_call(perf_cb, m, del_cb);
}

/* enables or disables autohp (value is ignored when enabled is 0) */
int maple_setautohp(unsigned int key, int enabled, uint32_t value) {
	return setautopot(AUTOPOT_HP, key, enabled, value);
}

/* enables or disables automp (value is ignored when enabled is 0) */
int maple_setautomp(unsigned int key, int enabled, uint32_t value) {
	return setautopot(AUTOPOT_MP, key, enabled, value);
}

static int lastautohp = 0;
static int lastautomp = 0;

/* executes one frame for all of the macros */
void maple_processmacros() {
	int i;
	CWndMan *wndman;
	CUIStatusBar *status;
	int now;
	int millispan;

	maple_handleautocc();
	if (autocc_status != AUTOCC_IDLE) {
		return;
	}

	now = getmillicount();

	if (autohp.enabled || automp.enabled) {
		status = CUIStatusBar__GetInstance();
		if (!status) {
#if _DEBUG
			puts("null status");
#endif
		}
		else {

#if _DEBUG
			printf("hp: %d, mp: %d\n", status->hp, status->mp);
#endif
			wndman = CWndMan__GetInstance();
			if (!wndman) {
#if _DEBUG
				puts("null wndman");
#endif
				return;
			}

			if (autohp.enabled && status->hp <= (int32_t)autohp.delay && now - lastautohp > 500) {
				CWndMan__OnKey(wndman, NULL, 0, autohp.key);
				lastautohp = now;
			}

			if (automp.enabled && status->mp <= (int32_t)automp.delay && now - lastautomp > 500) {
				CWndMan__OnKey(wndman, NULL, 0, automp.key);
				lastautomp = now;
			}
		}
	}

	for (i = 0; i < MAX_MACROS; i++) {
		if (!macros[i].enabled) {
			continue;
		}

		millispan = getmillispan(macros[i].lastpressed);
		if (millispan < (int)macros[i].delay) {
			continue;
		}

		/* macros with delays higher than 500ms will be exclusive and will stop all other macros for 500ms */
		if (exclusivemacro) {
			if (macros[i].key != exclusivemacro->key) {
				continue;
			}
			else {
				exclusivemacro_count++;

				if (exclusivemacro_count >= 30) {
#if _DEBUG
					printf("exclusive macro for key %.08X is done\n", macros[i].key);
#endif
					macros[i].lastpressed = now;
					exclusivemacro_count = 0;
					exclusivemacro = NULL;
				}
			}
		}
		else if (macros[i].delay > 500) {
#if _DEBUG
			printf("starting exclusive macro for key %.08X\n", macros[i].key);
#endif
			exclusivemacro = &macros[i];

			/* ghetto solution to autoskills not working with the hold autoattack type */
			/* TODO: find a way to do it properly */
			if (macros[0x1D].enabled && macros[0x1D].type == KEYPRESS_KEEPDOWN) {
				wndman = CWndMan__GetInstance();
				if (!wndman) {
#if _DEBUG
					puts("null wndman");
#endif
					return;
				}
				CWndMan__OnKey(wndman, NULL, VK_CONTROL, MAPLEKEY_CTRL);
				CWndMan__OnKey(wndman, NULL, VK_CONTROL, MAPLEKEY_UP(MAPLEKEY_CTRL));
			}
		}

		/* press the key */
		wndman = CWndMan__GetInstance();
		if (!wndman) {
#if _DEBUG
			puts("null wndman");
#endif
			return;
		}

#if _DEBUG
		printf("sending keypress for macro %.08X\n", macros[i].key);
#endif

		/* TODO: overhaul this code to do it the proper way as Waty & Shadow explained */
		switch (macros[i].type) {
		case KEYPRESS_PRESSED:
			CWndMan__OnKey(wndman, NULL, macros[i].virtual_key, macros[i].key);
			break;
		case KEYPRESS_HOLD:
			CWndMan__OnKey(wndman, NULL, macros[i].virtual_key, MAPLEKEY_DOWN(macros[i].key));
			CWndMan__OnKey(wndman, NULL, macros[i].virtual_key, macros[i].key);
			CWndMan__OnKey(wndman, NULL, macros[i].virtual_key, MAPLEKEY_UP(macros[i].key));
			break;
		case KEYPRESS_FASTHOLD:
			CWndMan__OnKey(wndman, NULL, macros[i].virtual_key, MAPLEKEY_DOWN(macros[i].key));
			CWndMan__OnKey(wndman, NULL, macros[i].virtual_key, MAPLEKEY_UP(macros[i].key));
			break;
		case KEYPRESS_KEEPDOWN:
			CWndMan__OnKey(wndman, NULL, macros[i].virtual_key, MAPLEKEY_DOWN(macros[i].key));
			break;
#if _DEBUG
		default:
			puts("unknown keypress type");
#endif
		}
	}
}