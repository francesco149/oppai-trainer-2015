/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of Oppai Trainer 2015.
	Oppai Trainer 2015 is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	Oppai Trainer 2015 is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with Oppai Trainer 2015. If not, see <http://www.gnu.org/licenses/>.
*/

#include <Windows.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

/* Shellapi */
#include <Shellapi.h>
#pragma comment(lib, "Shell32.lib")

/* win32_utils*/
#include "win32_console.h"
#include "win32_gui.h"
#if _DEBUG
#pragma comment(lib, "win32_utils_d.lib")
#else
#pragma comment(lib, "win32_utils.lib")
#endif

#include "globals.h"

#ifdef USE_AUTH
#include "GkAuthC.h"
#endif

#include <hackdesu/command_queue.h>
#include <hackdesu/ini.h>
#include "resource.h"
#include "maple_hacks.h"
#include "maple_macros.h"
#include "maple_structs.h"

/* other dialogs */
#include "pointerwatch.h"
#include "hacks.h"
#include "botting.h"
#include "itemfilter.h"

/* various info about the app */
#define OPPAI_NAME "Oppai Trainer 2015"
#define OPPAI_VERSION "v0.5.2 alpha"
#define OPPAI_CLASS "OppaiTrainer2015"

#ifdef USE_AUTH
/* login window stuff */
#define LOGIN_NAME "GameKiller Log-in"
#define LOGIN_CLASS "OppaiTrainer2015Login"
#define IDC_EDIT_USERNAME 101
#define IDC_EDIT_PASSWORD 102
#define IDC_CHECK_REMEMBER 103
#define IDC_BUTTON_LOGIN 104
#else
/* splash screen stuff */
#define SPLASH_CLASS "OppaiTrainer2015Splash"
#define IDT_TIMER1 800
#define IDC_BITMAP_SPLASH 999
#endif

/* component ids */
#define IDC_BITMAP_LOGO 999

#define IDM_FILE_SAVESETTINGS 901
#define IDM_FILE_EXIT 902
#define IDM_TOOLS_POINTERS 903
#define IDM_HELP_ABOUT 904

#define IDC_STATUS 101

#define IDC_GROUP_FEATURES 201
#define IDC_BUTTON_HACKS 202
#define IDC_BUTTON_BOTTING 203
#define IDC_BUTTON_ITEMFILTER 204
#define IDC_BUTTON_HIDE 205
#define IDC_BUTTON_DONATE 206

#define IDC_GROUP_INFO 301
#define IDC_EDIT_INFO 302

/* component sizes and margins */
#define OPPAI_W 405
#define OPPAI_H 532

#define LOGO_W 405
#define LOGO_H 96

HINSTANCE oppai_instance = NULL; /* handle to the trainer's dll in memory */
static HBITMAP oppai_logo = NULL; /* handle to the logo's bitmap */
#ifndef USE_AUTH
static HBITMAP oppai_splash = NULL; /* handle to the trainers, splashscreen */
#endif
static WNDPROC oppai_origlogoproc = NULL; /* pointer to the original static wndproc for the logo */
static int maple_hidden = 0; /* flag that indicates whether maple is hidden or not */

/*#define DEBUGKEYS*/

/*
* displays the given error message as a messagebox with the given MB icon
* followed by the formatted GetLastError message
*/
static void msgbox_gle(const char *msg, UINT icon) {
	DWORD gle;
	char *gle_msg;
	char buf[512] = { 0 };

	gle = GetLastError();
	if (!gle) {
		return;
	}

	FormatMessageA(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, gle,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPSTR)&gle_msg,
		0, NULL
	);

	sprintf_s(buf, 512, "%s, GLE=%.08X (%lu): %s\n", msg, gle, gle, gle_msg);
	MessageBoxA(NULL, buf, OPPAI_NAME, MB_OK | icon);
	LocalFree(gle_msg);
}

/* wrapper for a msgbox_gle call with a MB_ICONWARNING icon */
#define msgbox_gle_warning(msg) msgbox_gle(msg, MB_ICONWARNING)

/* wrapper for a msgbox_gle call with a MB_ICONERROR icon */
#define msgbox_gle_error(msg) msgbox_gle(msg, MB_ICONERROR)

#if _DEBUG
/* reallocates the console (required when starting the game) */
static void oppai_reallocconsole(void *param) {
	UNUSED(param);
	FreeConsole();
	if (!win32_console_create()) {
		msgbox_gle_warning("Failed to re-allocate console, "
			"you won't be able to see in-game debug logs.");
	}
}
#endif

static void oppai_opengamekiller() {
	ShellExecuteA(NULL, "open",
		"http://www.gamekiller.net/global-maplestory-hacks-and-bots/",
		NULL, NULL, SW_SHOWNORMAL);
}

/* <settings> */
static const char *sect = "mainwindow";

static void oppai_savesettings(HWND wnd) {
	RECT r;
	char buf[16] = { 0 };
	
	if (!GetWindowRect(wnd, &r)) {
		win32_show_error("GetWindowRect failed for the main window");
	}
	else if (r.left < 32767 && r.left > -32000 && r.top < 32767 && r.top > -32000) { /* should prevent invalid positions from saving */
#if _DEBUG
		puts("Saving mainwindow settings...");
#endif
		ini_write_int(sect, "x", r.left, OPPAI_SETTINGS);
		ini_write_int(sect, "y", r.top, OPPAI_SETTINGS);
#if _DEBUG
		puts("Saved mainwindow!");
#endif
	}

	hacks_savesettings();
	botting_savesettings();
	itemfilter_savesettings();
}

static void oppai_loadsettings(HWND wnd) {
	POINT pos;

#if _DEBUG
	puts("Loading mainwindow settings...");
#endif
	pos.x = ini_get_int(sect, "x", CW_USEDEFAULT, OPPAI_SETTINGS);
	pos.y = ini_get_int(sect, "y", CW_USEDEFAULT, OPPAI_SETTINGS);

	if (pos.x != CW_USEDEFAULT && pos.y != CW_USEDEFAULT) {
		SetWindowPos(wnd, NULL, pos.x, pos.y, 0, 0, 
			SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOZORDER);
	}
#if _DEBUG
	puts("Loaded mainwindow!");
#endif

	hacks_loadsettings();
	botting_loadsettings();
	itemfilter_loadsettings();
}
/* </settings> */

static LRESULT CALLBACK oppai_logoproc(HWND wnd, UINT msgid, WPARAM wparam, LPARAM lparam) {
	HCURSOR logocursor;

	switch (msgid) {
	case WM_SETCURSOR:
		logocursor = LoadCursorA(NULL, MAKEINTRESOURCEA(IDC_HAND));
		if (!logocursor) {
			logocursor = LoadCursorA(NULL, MAKEINTRESOURCEA(IDC_ARROW));
		}
		SetCursor(logocursor);
		return TRUE;
	}

	return CallWindowProcA(oppai_origlogoproc, wnd, msgid, wparam, lparam);
}

/* initializes the main window's controls and performs other initialization-time operations */
static LRESULT oppai_initwnd(HWND wnd) {
	HWND edit, button, bitmap, group, status;
	HMENU menu, submenu;
	RECT wndrect = { 0 };
	DWORD st;
	int d = 0;
	char buftitle[128] = { 0 };

	sprintf_s(buftitle, 128, "Oppai Trainer 2015 | PID: %d", GetCurrentProcessId());
	SetWindowTextA(wnd, buftitle);

	GetClientRect(wnd, &wndrect);
	
	/* menu */
	menu = CreateMenu();
	submenu = CreatePopupMenu();
	AppendMenuA(menu, MF_STRING | MF_POPUP, (UINT_PTR)submenu, "&File");
	AppendMenuA(submenu, MF_STRING, IDM_FILE_SAVESETTINGS, "&Save Settings");
	AppendMenuA(submenu, MF_STRING, IDM_FILE_EXIT, "&Exit");
	submenu = CreatePopupMenu();
	AppendMenuA(menu, MF_STRING | MF_POPUP, (UINT_PTR)submenu, "&Tools");
	AppendMenuA(submenu, MF_STRING, IDM_TOOLS_POINTERS, "&Pointer Watch");
	submenu = CreatePopupMenu();
	AppendMenuA(menu, MF_STRING | MF_POPUP, (UINT_PTR)submenu, "&Help");
	AppendMenuA(submenu, MF_STRING, IDM_HELP_ABOUT, "&About");
	SetMenu(wnd, menu);

	/* status bar */
	status = CreateWindowEx(
		0, STATUSCLASSNAMEA, NULL,
		WS_CHILD | WS_VISIBLE, 0, 0, 0, 0,
		wnd, (HMENU)IDC_STATUS, oppai_instance, NULL
	);
	SetWindowTextA(status, "\t\tby Franc[e]sco");

	/* logo */
	oppai_logo = LoadBitmapA(oppai_instance, MAKEINTRESOURCEA(IDB_IMAGE1));
	if (!oppai_logo) {
		win32_show_error("failed to load logo");
		return -1;
	}
	bitmap = win32_make_bitmap(wnd, IDC_BITMAP_LOGO, "bitmap_logo", 0, 0, LOGO_W, LOGO_H);
	if (!bitmap) {
		return -1;
	}
	
	SendMessageA(bitmap, STM_SETIMAGE, (WPARAM)IMAGE_BITMAP, (LPARAM)oppai_logo);

	st = GetWindowLongA(bitmap, GWL_STYLE);
	if (!st) {
		win32_show_error("GetWindowLongA failed for style");
		return -1;
	}
	if (!SetWindowLongA(bitmap, GWL_STYLE, st | SS_NOTIFY)) {
		win32_show_error("SetWindowLongA failed for style");
		return -1;
	}

	oppai_origlogoproc = (WNDPROC)SetWindowLongPtrA(bitmap, GWLP_WNDPROC, (LONG_PTR)oppai_logoproc);
	if (!oppai_origlogoproc) {
		win32_show_error("SetWindowLongA failed for wndproc");
		return -1;
	}
	
	/* features */
	group = win32_make_groupbox(
		wnd, IDC_GROUP_FEATURES, "group_features", "Features", 
		10, 106, 98, wndrect.bottom - 110 - 50
	);

	d = 0;
	button = win32_make_button(
		wnd, IDC_BUTTON_HACKS, "button_hacks", "Hacks",
		23, 126 + d, 72, 22);
	if (!button) {
		return -1;
	}

	d += 30;
	button = win32_make_button(
		wnd, IDC_BUTTON_BOTTING, "button_botting", "Botting",
		23, 126 + d, 72, 22);
	if (!button) {
		return -1;
	}

	d += 30;
	button = win32_make_button(
		wnd, IDC_BUTTON_ITEMFILTER, "button_itemfilter", "Item Filter",
		23, 126 + d, 72, 22);
	if (!button) {
		return -1;
	}

	d += 30;
	button = win32_make_button(
		wnd, IDC_BUTTON_HIDE, "button_hidemaple", "Hide Maple",
		23, 126 + d, 72, 22);
	if (!button) {
		return -1;
	}

	d += 30;
	button = win32_make_button(
		wnd, IDC_BUTTON_DONATE, "button_donate", "Donate",
		23, 126 + d, 72, 22);
	if (!button) {
		return -1;
	}

	/* useful info */
	group = win32_make_groupbox(
		wnd, IDC_GROUP_FEATURES, "group_info", "Useful Info",
		120, 106, 
		wndrect.right - 120 - 10, 
		wndrect.bottom - 110 - 50
	);

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_INFO, "edit_info",
"- The auto-skill values are in milliseconds.\r\n\
- The auto hp/mp values are not percentages.\r\n\
- If Kami D/Cs, reboot or switch map/job.\r\n\
- You don't need to tick auto-attack/loot for\r\n\
   kami. Just select auto-attack type.\r\n\
- You can toggle mouse fly with F10.\r\n\
- If no windows pop up when you click on the\r\n\
   features, you settings file might be corrupted\r\n\
   so please try deleting OppaiTrainer2015.ini\r\n\
   from your maplestory folder\r\n\
   \r\n\
Changelog:\r\n\
0.5.2a\r\n\
[*] Updated to 161.1\r\n\
[*] Fixed AutoCC not working properly with\r\n\
    multiple conditions checked.\r\n\
[*] Fixed autologin crashes at world select\r\n\
    (thanks Mahorori!)\r\n\
\r\n\
0.5.1a\r\n\
[*] Fixed wrong world id's for auto login\r\n\
[*] Reworked AutoCC logic entirely for stability\r\n\
\r\n\
0.5a\r\n\
[+] Added AutoCC\r\n\
[*] Kami now teleports to top left when ccing\r\n\
[*] The mouse fly checkbox now toggles the mouse\r\n\
    fly hotkey instead of activating mousefly\r\n\
[*] Kami can be toggled with F11 and kami loot\r\n\
    can be toggled with F12\r\n\
[+] Added Auto Login by Mahorori\r\n\
[+] Added save settings button in the File menu\r\n\
[+] The trainer now disable codex message boxes\r\n\
\r\n\
0.4.7a\r\n\
[*] Added PID to all windows\r\n\
[*] Updated to v160.3 (untested)\r\n\
[*] Removed blocking of crash reports until\r\n\
	I verify that it doesn't cause issues.\r\n\
\r\n\
0.4.6a\r\n\
[*] Updated vector controller struct which\r\n\
     changed in v160.2, causing distance\r\n\
     calculation in kami to miscalculate\r\n\
     and kami loot to return to incorrect\r\n\
     coords.\r\n\
[*] Removed GameKiller login.\r\n\
[*] Fixed random hwid crash.\r\n\
\r\n\
0.4.5a\r\n\
[*] Fixed kami crash with the \"new\" method\r\n\
\r\n\
0.4.4a\r\n\
[*] Updated to v160.2\r\n\
[*] Edited the kami code to use the vector\r\n\
     controller offset directly so that\r\n\
     there are less addresses to update\r\n\
\r\n\
0.4.3a\r\n\
[*] Updated to v160\r\n\
[*] Added a check to window coordinates to\r\n\
     prevent invalid window positions from\r\n\
     being saved\r\n\
\r\n\
0.4.2a\r\n\
[*] Added experimental hash detection bypass\r\n\
\r\n\
0.4.1a hotfix\r\n\
[*] Fixed AirCheck crashing on magician jobs\r\n\
[*] Fixed autoatt not being correctly disabled\r\n\
     when disabling kami during loot\r\n\
[*] Fixed autoskill not working with the \"Hold\"\r\n\
     autoattack\r\n\
\r\n\
0.4a public release\r\n\
[+] The trainer now saves settings as\r\n\
      well as window placements\r\n\
[+] Added Aircheck Bypass by Taku/Peru\r\n\
[*] Fixed autoattack not being re-enabled\r\n\
      after kami is done looting\r\n\
[+] Added Mouse Fly (toggle with F10)\r\n\
[*] Fixed combos not triggering with\r\n\
     autoattack on specific classes.\r\n\
[*] Added a new autoattack type \"Hold\"\r\n\
[*] Capped delay between each autopot\r\n\
     keypress to 500ms so that it doesn't\r\n\
      spam potions when tubi is enabled.\r\n\
[*] Temporarily removed the built-in black\r\n\
     cipher bypass as it seems to cause issues\r\n\
     for some people.\r\n\
\r\n\
0.3.1a hotfix\r\n\
[*] Fixed loot not always teleporting back to\r\n\
     the initial position correctly.\r\n\
[+] Added built - in blackcipher bypass that is\r\n\
automatically enabled if whatever bypass\r\n\
you're using doesn't bypass it already.\r\n\
\r\n\
0.3a public release\r\n\
[*] UI redesign (TODO: add pointers in the\r\n\
     main window's empty space?)\r\n\
[+] Added item filter (TODO: search by name)\r\n\
[*] The login window now accepts tabstops\r\n\
     (TODO: make enter key click login)\r\n\
[*] Made pointer watch's textbox read-only.\r\n\
[*] Coordinates textboxes now accept\r\n\
     negative values.\r\n\
[*] Fixed \"Teleport to coords\"\r\n\
[+] Added 6 more autoskills\r\n\
[*] Kami loot can now work by itself (without\r\n\
     having to be kami'ing) and will teleport\r\n\
     back to the starting position when done\r\n\
     looting.\r\n\
[*] Fixed autoloot not stopping when\r\n\
    unchecking loot\r\n\
[*] Added loot when items > ... setting\r\n\
[+] Added swim hack (use it to kami on\r\n\
    classes that can't attack in mid-air)\r\n\
[+] Added suck left vac\r\n"
"[+] The trainer now prevents maple from\r\n\
     creating crash reports that might\r\n\
     potentially leak information about\r\n\
     the hacks you're using\r\n"
"[*] Changed kami distance calculation to\r\n\
    use more accurate double coords\r\n\
\r\n\
0.2a public release\r\n\
[+] Added tubi and stable tubi\r\n\
[*] Fixed auto skill\r\n\
[*] Kami now loots every 20 items (will be \r\n\
     customizable in the near future)\r\n\
[+] Added 58s godmode(it's like full godmode \r\n\
     but you get hit 1 time every 58 seconds)\r\n\
[+] Added pointer watch window(will be a real\r\n\
     time listview in the near future)\r\n\
[*] Minor code cleanup and bugfixes\r\n\
\r\n\
0.1a public release\r\n\
[+] Initial release",
		120 + 15, 106 + 14 + 7,
		wndrect.right - 120 - 10 * 2 - 14 - 6, 
		wndrect.bottom - 106 - 10 * 2 - 50 - 14 - 7, 
		WS_EDIT_DEFAULT | ES_MULTILINE | ES_READONLY | WS_VSCROLL);
	if (!edit) {
		return -1;
	}

	/*
	removed because it seems to cause more problems that it's worth
	maple_blackcipher_trybypass();
	*/

	/*maple_blockcrashreports();*/
	hack_toggle(&maple_nocodex, 1);

	ptrwatch_create(wnd);
	hacks_create(wnd);
	botting_create(wnd);
	itemfilter_create(wnd);

	oppai_loadsettings(wnd);

	return 0;
}

/* triggered when the window in being closed through the x button */
static void oppai_onclose(HWND wnd) {
	int res = MessageBoxA(
		wnd, "Do you also want to terminate MapleStory?", 
		OPPAI_NAME, MB_YESNOCANCEL | MB_ICONQUESTION);
	if (res == IDCANCEL) {
		return;
	}

	oppai_savesettings(wnd);
	DestroyWindow(wnd);
	oppai_opengamekiller();

	if (res == IDYES) {
		TerminateProcess(GetCurrentProcess(), EXIT_SUCCESS);
	}
}

/* cleans up all modified memory before uninjecting */
static void oppai_restorememory() {
#if _DEBUG
	puts("Un-hooking update function...");
#endif
	maple_hookupdate(0);

#if _DEBUG
	puts("Disabling all hacks...");
#endif
	hack_toggle(&maple_tubi, 0);
	hack_toggle(&maple_nokb, 0);
	hack_toggle(&maple_timedgodmode, 0);
	hack_toggle(&maple_fullgodmode, 0);
	hack_toggle(&maple_fullmobdisarm, 0);
	hack_toggle(&maple_lemmings, 0);
	hack_toggle(&maple_skiplogo, 0);
	hack_toggle(&maple_randomizehwid, 0);
	hack_toggle(&maple_cpuhack, 0);
	hack_toggle(&maple_magicgodmode, 0);
	hack_toggle(&maple_instadrop, 0);
	hack_toggle(&maple_jda, 0);
	hack_toggle(&maple_viewswears, 0);
	hack_toggle(&maple_reactordem, 0);
	hack_toggle(&maple_nofa, 0);
	hack_toggle(&maple_ua, 0);
	hack_toggle(&maple_itemhook, 0);
	hack_toggle(&maple_itemfilter, 0);
	hack_toggle(&maple_suckleft, 0);
	hack_toggle(&maple_swimhack, 0);
	hack_toggle(&maple_airchecks, 0);
	hack_toggle(&maple_nocodex, 0);
	maple_kami(0, 0, 0, KEYPRESS_PRESSED);
	maple_slowtubi(0);
	hack_toggle(&maple_autologin, 0);

#if _DEBUG && defined DEBUGKEYS
	hack_toggle(&maple_debugkeyhook, 0);
#endif

	cq_process_all();
#if _DEBUG
	puts("Ready to un-inject safely.");
#endif
}

/* triggered when any checkbox is clicked */
static void oppai_handlecheckbox(HWND wnd, int cb) {
	HWND wndcheck;
	int checked;

	wndcheck = GetDlgItem(wnd, cb);
	if (!wndcheck) {
		win32_show_error("GetDlgItem failed for wndcheck");
		return;
	}
	checked = Button_GetCheck(wndcheck) == BST_CHECKED;

	switch (cb) {
		/* TODO: slow pointer update checkbox */

#if _DEBUG
	default:
		printf("Unhandled checkbox message for control id %d\n", cb);
#endif
	}
}

/* triggered by Help -> About */
static void oppai_aboutclicked(HWND wnd) {
	char info[512] = { 0 };
	sprintf_s(info, 512, "%s %s\r\n\r\n"
		"by Franc[e]sco (lolisamurai@tfwno.gf)\r\n"
		"Special thanks to AIRRIDE, Shadow, Conquest, Waty, Taku, kevintjuh93, DarkBoy "
		"and everyone who helps me out with reversing all the time", 
		OPPAI_NAME, OPPAI_VERSION);
	MessageBoxA(wnd, info, "About", MB_OK | MB_ICONINFORMATION);
}

/* finds and hides the maplestory window */
static void oppai_hidemaple(HWND dlg) {
	HWND hwnd;
	hwnd = maple_window();
	if (hwnd) {
		ShowWindow(hwnd, maple_hidden ? SW_SHOW : SW_HIDE);
		maple_hidden ^= 1;
		SetDlgItemTextA(dlg, IDC_BUTTON_HIDE, maple_hidden ? "Show Maple" : "Hide Maple");
	}
}

/* handles window messages */
static LRESULT CALLBACK oppai_wndproc(HWND wnd, UINT msgid, WPARAM wparam, LPARAM lparam) {
	switch (msgid) {
	case WM_CREATE:
		return oppai_initwnd(wnd);

	case WM_CLOSE:
		oppai_onclose(wnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_COMMAND:
		switch (LOWORD(wparam)) {
		case IDC_BITMAP_LOGO:
			oppai_opengamekiller();
			return TRUE;

		case IDC_BUTTON_HACKS:
			hacks_show(wnd);
			return TRUE;

		case IDC_BUTTON_BOTTING:
			botting_show(wnd);
			return TRUE;

		case IDC_BUTTON_ITEMFILTER:
			itemfilter_show(wnd);
			return TRUE;

		case IDC_BUTTON_HIDE:
			oppai_hidemaple(wnd);
			return TRUE;

		case IDM_FILE_EXIT:
			PostQuitMessage(0);
			return TRUE;

		case IDM_FILE_SAVESETTINGS:
			oppai_savesettings(wnd);
			return TRUE;

		case IDM_TOOLS_POINTERS:
			ptrwatch_show(wnd);
			return TRUE;

		case IDM_HELP_ABOUT:
			oppai_aboutclicked(wnd);
			return TRUE;

		case IDC_BUTTON_DONATE:
			ShellExecuteA(NULL, "open",
				"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=93EJS88KVQBB8",
				NULL, NULL, SW_SHOWNORMAL);
			return TRUE;

		/*case TODO:
			oppai_handlecheckbox(wnd, LOWORD(wparam));
			return TRUE;
		*/
		}
		break;

	default:
		return DefWindowProcA(wnd, msgid, wparam, lparam);
	}

	return 0;
}

#ifdef USE_AUTH
/* NOTE: these are cleared whenever they're not needed so it should be safe */
static char *gk_user[32] = { 0 };
static char *gk_password[32] = { 0 };
static int login_clicked = 0;

static LRESULT login_initwnd(HWND wnd) {
	HWND edit, button;

	edit = win32_make_edit(
		wnd, IDC_EDIT_USERNAME, "edit_user",
		"username", 10, 10, 275, 20
	);
	if (!edit) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_PASSWORD, "edit_password",
		"password", 10, 35, 275, 20, WS_EDIT_DEFAULT | ES_PASSWORD
	);
	if (!edit) {
		return -1;
	}

	button = win32_make_button(
		wnd, IDC_BUTTON_LOGIN, "button_login",
		"Login", 9, 65, 277, 22
	);
	if (!button) {
		return -1;
	}

	return 0;
}

static void login_handlelogin(HWND wnd) {
	if (!GetDlgItemTextA(wnd, IDC_EDIT_USERNAME, (LPSTR)gk_user, 32)) {
#if _DEBUG
		win32_show_error("GetDlgItemTextA failed for user");
#endif
		return;
	}

	if (!GetDlgItemTextA(wnd, IDC_EDIT_PASSWORD, (LPSTR)gk_password, 32)) {
#if _DEBUG
		win32_show_error("GetDlgItemTextA failed for user");
#endif
		return;
	}

	login_clicked = 1;
	DestroyWindow(wnd);
}

static LRESULT CALLBACK login_wndproc(HWND wnd, UINT msgid, WPARAM wparam, LPARAM lparam) {
	switch (msgid) {
	case WM_CREATE:
		return login_initwnd(wnd);

	case WM_CLOSE:
		oppai_onclose(wnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_COMMAND:
		switch (LOWORD(wparam)) {
		case IDC_BUTTON_LOGIN:
			login_handlelogin(wnd);
			break;
		}
		break;

	default:
		return DefWindowProcA(wnd, msgid, wparam, lparam);
	}

	return 0;
}
#else
static LRESULT splash_initwnd(HWND wnd) {
	HWND splash;

	SetWindowLongPtrA(wnd, GWL_STYLE, CS_HREDRAW | CS_VREDRAW | CS_OWNDC);

	/* timer that checks for maple */
	SetTimer(wnd, IDT_TIMER1, 100, NULL);

	/* logo */
	oppai_splash = LoadBitmapA(oppai_instance, MAKEINTRESOURCEA(IDB_IMAGE2));
	if (!oppai_splash) {
		win32_show_error("failed to load splash");
		return -1;
	}
	splash = win32_make_bitmap(wnd, IDC_BITMAP_SPLASH, "bitmap_splash", 0, 0, 474, 300);
	if (!splash) {
		return -1;
	}
	SendMessageA(splash, STM_SETIMAGE, (WPARAM)IMAGE_BITMAP, (LPARAM)oppai_splash);

	SetWindowPos(wnd, NULL, 
		GetSystemMetrics(SM_CXSCREEN) / 2 - 474 / 2, 
		GetSystemMetrics(SM_CYSCREEN) / 2 - 300 / 2, 0, 0,
		SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOZORDER
	);

	return 0;
}

static LRESULT CALLBACK splash_wndproc(HWND wnd, UINT msgid, WPARAM wparam, LPARAM lparam) {
	switch (msgid) {
	case WM_CREATE:
		return splash_initwnd(wnd);

	case WM_CLOSE:
		oppai_onclose(wnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_TIMER:
		switch (wparam) {
		case IDT_TIMER1:
			if (CWvsPhysicalSpace2D__GetInstance()) {
				DestroyWindow(wnd);
			}
			break;
		}
		break;

	default:
		return DefWindowProcA(wnd, msgid, wparam, lparam);
	}

	return 0;
}
#endif

/* registers and creates the main window */
static int oppai_create(HMODULE module, int cmdshow) {
	HWND wnd;
#ifdef USE_AUTH
	GK_AUTH auth = { 0 };
	DWORD result;
#endif
#if _DEBUG
#ifdef USE_AUTH
	char json[10000] = { 0 };
#endif
	cq_callback realloc_console = { oppai_reallocconsole };
#endif
	HINSTANCE inst = (HINSTANCE)module;
	oppai_instance = inst;
	char errbuf[256] = { 0 };

#if _DEBUG
	if (!win32_console_create()) {
		msgbox_gle_warning("Failed to allocate console, you won't be able to see debug logs.");
	}
#endif

	cq_init();
	srand((unsigned int)time(NULL));

	InitCommonControls();

#ifdef USE_AUTH
	/* GK auth */
	if (!CreateGkAuth(&auth)) {
#if _DEBUG
		puts("CreateGkAuth failed.");
#endif
		return EXIT_FAILURE;
	}

	while (1) {
		/* login */
		wnd = win32_make_window_s(
			LOGIN_CLASS, LOGIN_NAME,
			inst, login_wndproc,
			LoadIconA(inst, MAKEINTRESOURCEA(IDI_ICON1)),
			300, 125, cmdshow,
			WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME ^ WS_MAXIMIZEBOX
		);
		if (!wnd) {
			return EXIT_FAILURE;
		}

		win32_message_loop(wnd);
		UnregisterClassA(LOGIN_CLASS, inst);
		if (!login_clicked) {
			return EXIT_SUCCESS;
		}

		if (!strlen((const char *)gk_user)) {
			MessageBoxA(NULL, "Empty username.", LOGIN_NAME, MB_OK | MB_ICONERROR);
			continue;
		}

		if (!strlen((const char *)gk_password)) {
			MessageBoxA(NULL, "Empty password.", LOGIN_NAME, MB_OK | MB_ICONERROR);
			continue;
		}

		result = GkAuthAuthorize(&auth, OPPAI_CLASS, (char *)gk_user, (char *)gk_password);
		memset((void *)gk_user, 0, 32);
		memset((void *)gk_password, 0, 32);
		if (result == AUTH_SUCCEEDED ||
			result == AUTH_FAILED_INVALID_USERNAME ||
			result == AUTH_FAILED_INVALID_PASSWORD)
		{
#if _DEBUG
			GetGkAuthRecvJSON(&auth, json, 10000);

			puts("=====================");
			puts("Received Message");
			puts("=====================");
			printf("%s\n\n", json);
			printf("Public Access:\t%s\n", GkAuthHavePublicAccess(&auth) ? "Yes" : "No");
#endif

			switch (result) {
			case AUTH_FAILED_INVALID_USERNAME:
				MessageBoxA(NULL, "Invalid username.", LOGIN_NAME, MB_OK | MB_ICONERROR);
				break;
			case AUTH_FAILED_INVALID_PASSWORD:
				MessageBoxA(NULL, "Invalid password.", LOGIN_NAME, MB_OK | MB_ICONERROR);
				break;
			}

			if (GkAuthHavePublicAccess(&auth)) {
				break;
			}
		}
		else {
			sprintf_s(errbuf, 256, "Auth failure %d.", result);
			MessageBoxA(NULL, errbuf, LOGIN_NAME, MB_OK | MB_ICONERROR);
#if _DEBUG
			printf("Failure! Error code: %d\n", result);
#endif
		}
	}

	if (!DestroyGkAuth(&auth)) {
#if _DEBUG
		puts("DestroyGkAuth failed.");
#endif
		return EXIT_FAILURE;
	}
#else
	/* GUI init */
	wnd = win32_make_window_ex(
		SPLASH_CLASS, OPPAI_NAME ": Waiting for MapleStory...",
		inst, splash_wndproc,
		LoadIconA(inst, MAKEINTRESOURCEA(IDI_ICON1)),
		474, 300, cmdshow,
		WS_EX_APPWINDOW,
		WS_OVERLAPPED | WS_POPUP
	);
	if (!wnd) {
		return EXIT_FAILURE;
	}

	win32_message_loop(NULL);
	win32_clear_message_queue(NULL);
	UnregisterClassA(SPLASH_CLASS, inst);
#endif

	/* various init code */
#if _DEBUG
	puts("Hooking CWndMan::s_Update");
#endif
	maple_hookupdate(1);
#if _DEBUG
	puts("Ready!");
#endif

	hwid_tryinit();
#if _DEBUG
	cq_call(realloc_console, NULL, cq_no_callback); /* will be called on the first update */
#endif

#if _DEBUG && defined DEBUGKEYS
	hack_toggle(&maple_debugkeyhook, 1);
#endif

	/* GUI init */
	wnd = win32_make_window_s(
		OPPAI_CLASS, OPPAI_NAME,
		inst, oppai_wndproc,
		LoadIconA(inst, MAKEINTRESOURCEA(IDI_ICON1)),
		OPPAI_W, OPPAI_H, cmdshow, 
		WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME ^ WS_MAXIMIZEBOX
	);
	if (!wnd) {
		return EXIT_FAILURE;
	}

	return win32_message_loop(NULL);
}

/* main GUI thread started on injection */
static DWORD __stdcall rundll(__in LPVOID param) {
	HMODULE module = (HMODULE)param;
	HINSTANCE inst = (HINSTANCE)module;
	int res;

	res = oppai_create(inst, SW_SHOW);

	if (res != EXIT_SUCCESS) {
		win32_clear_message_queue(NULL); /* clear message queue, otherwise the messagebox won't work */
		msgbox_gle_error("Oops! Looks like something broke. "
			"Please report this to the developer or check debug logs.");
	}

#if _DEBUG
	/* pause to see the log */
	system("pause");
#endif

	/* free win32 objects */
	if (oppai_logo) {
		DeleteObject(oppai_logo);
	}
	if (oppai_splash) {
		DeleteObject(oppai_splash);
	}

	ptrwatch_destroy();
	hacks_destroy();
	botting_destroy();
	itemfilter_destroy();
	oppai_restorememory();

	/* DLLs must explicitly unregister classes, otherwise they will remain registered */
	UnregisterClassA(OPPAI_CLASS, inst);

#ifdef USE_AUTH
	UnregisterClassA(LOGIN_CLASS, inst);
#endif

	/* deallocate command queue */
	cq_destroy();

#if _DEBUG
	/* close console */
	FreeConsole();
#endif

	/* un-inject DLL */
	FreeLibraryAndExitThread(module, EXIT_SUCCESS);

	return res;
}

static const char junkdata[] = "HUEHUEHUEHEUHEUHEUHEUEHUEHUEHUEHUEHEUHEUHEUHEUHEU";

int useless_function(int a) {
	a += 11;
	if (a > 345) {
		return 567;
	}

	a *= 678;
	a += a / 3;
	return 4;
}

BOOL APIENTRY DllMain(HMODULE module, DWORD reason, LPVOID reserved) {
	UNUSED(reserved);

	HANDLE thread = NULL;
	const char *junk = junkdata;

	useless_function(666);
	/* make sure that junkdata doesn't get optimized out */
	for (; *junk != '\0'; junk++) {}

	if (reason != DLL_PROCESS_ATTACH)
		return TRUE;

	DisableThreadLibraryCalls(module);
	thread = CreateThread(NULL, 0, rundll, (LPVOID)module, 0, NULL);
	if (!thread) {
		msgbox_gle_error("Failed to start main dll thread.");
		FreeLibraryAndExitThread(module, EXIT_SUCCESS);
	}

	return TRUE;
}