#ifndef __GKAUTHC_H__
#define __GKAUTHC_H__
#pragma once

#include <windows.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _BUILD_LIB
/* Flags for checking result code returned */
typedef enum tagGK_AUTH_RESULT {
	AUTH_SUCCEEDED,
	AUTH_FAILED_CREATETHREAD,
	AUTH_FAILED_NO_EXIT_CODE,
	AUTH_FAILED_WSASTARTUP,
	AUTH_FAILED_GETADDRINFO,
	AUTH_FAILED_INVALID_SOCKET,
	AUTH_FAILED_CONNECTION,
	AUTH_FAILED_MSG_FORMAT,
	AUTH_FAILED_SEND_REQUEST,
	AUTH_FAILED_RECV_DATA,
	AUTH_FAILED_INVALID_USERNAME,
	AUTH_FAILED_INVALID_PASSWORD
} GK_AUTH_RESULT;

#ifdef _DEBUG
#pragma comment(lib, "GkAuthC_d.lib")
#else
#pragma comment(lib, "GkAuthC.lib")
#endif
typedef void *GK_AUTH;
#else
typedef struct tagGK_AUTH GK_AUTH;
#endif

#define GKC_API WINAPI

/* 
 * Creates an instance of the auth class. This instance must be later destroyed using DestroyGkAuth. 
 * Returns FALSE if the system is out of memory.
 */
BOOL GKC_API CreateGkAuth(GK_AUTH *a);

/* Destroys an instance of the auth class. Retuns false if a is not a valid instance. */
BOOL GKC_API DestroyGkAuth(GK_AUTH *a);

/* Get authorization details of a user */
DWORD GKC_API GkAuthAuthorize(GK_AUTH *a, const char *appName, const char *username, const char *password);

/* 
 * Retrieves the username associated with the auth instance and stores it in buf. 
 * If successful, this function returns zero.
 * If the buffer is too small, the function fails and returns the required buffer size.
 */
SIZE_T GKC_API GetGkAuthUsername(GK_AUTH *a, char *buf, SIZE_T bufsize);

/*
 * Retrieves the password associated with the auth instance and stores it in buf.
 * If successful, this function returns zero.
 * If the buffer is too small, the function fails and returns the required buffer size.
 */
SIZE_T GKC_API GetGkAuthPassword(GK_AUTH *a, char *buf, SIZE_T bufsize);

/*
 * Retrieves the remote host associated with the auth instance and stores it in buf.
 * If successful, this function returns zero.
 * If the buffer is too small, the function fails and returns the required buffer size.
 */
SIZE_T GKC_API GetGkAuthRemoteHost(GK_AUTH *a, char *buf, SIZE_T bufsize);

/*
 * Retrieves the app name associated with the auth instance and stores it in buf.
 * If successful, this function returns zero.
 * If the buffer is too small, the function fails and returns the required buffer size.
 */
SIZE_T GKC_API GetGkAuthAppName(GK_AUTH *a, char *buf, SIZE_T bufsize);

/* Retrieves the unique number associated with the auth instance. */
DWORD GKC_API GetGkAuthUniqueNum(GK_AUTH *a);

/*
 * Retrieves the raw received JSON associated with the auth instance and stores it in buf.
 * If successful, this function returns zero.
 * If the buffer is too small, the function fails and returns the required buffer size.
 */
SIZE_T GKC_API GetGkAuthRecvJSON(GK_AUTH *a, char *buf, SIZE_T bufsize);

/* Check's user privilege information */
BOOL GKC_API GkAuthHavePublicAccess(GK_AUTH *a);

#ifdef __cplusplus
} // extern "C"
#endif

#endif
