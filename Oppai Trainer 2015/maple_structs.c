/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of Oppai Trainer 2015.
	Oppai Trainer 2015 is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	Oppai Trainer 2015 is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with Oppai Trainer 2015. If not, see <http://www.gnu.org/licenses/>.
*/

#include "maple_structs.h"
#include <stdio.h>

static void IWzVector2D_str(__in IWzVector2D *p, __out char *buf, size_t bufsize) {
	sprintf_s(buf, bufsize,
		"\tIWzVector2D {\r\n\t\t"
		"x, y = %f, %f\r\n\t\t"
		"animation = %d\r\n\t\t"
		"knockback_toggle = %d\r\n\t\t"
		"knockback_vx = %f\r\n\t\t"
		"knockback_vy = %f\r\n\t"
		"}",
		p->x, p->y,
		p->animation,
		p->knockback_toggle,
		p->knockback_vx,
		p->knockback_vy
	);
}

static void mobinfo_str(__in struct tagmobinfo *p, __out char *buf, size_t bufsize) {
	sprintf_s(buf, bufsize,
		"\tmobinfo {"			"\r\n\t\t"
			"movement = %lu"	"\r\n\t\t"
			"unk = %lu"			"\r\n\t\t"
			"aggro = %lu"		"\r\n\t"
		"}",
		p->movement,
		p->unk,
		p->aggro
	);
}

static void animation_str(__in struct taganimation *p, __out char *buf, size_t bufsize) {
	sprintf_s(buf, bufsize,
		"\tanimation {"				"\r\n\t\t"
			"id = %lu"				"\r\n\t\t"
			"unk1 = %lu"			"\r\n\t\t"
			"frame = %lu"			"\r\n\t\t"
			"delay = %lu"			"\r\n\t\t"
			"totalduration = %lu"	"\r\n\t\t"
			"unk2 = %lu"			"\r\n\t"
		"}",
		p->id,
		p->unk1,
		p->frame,
		p->delay,
		p->totalduration,
		p->unk2
	);
}

static void attack_str(__in struct tagattack *p, __out char *buf, size_t bufsize) {
	sprintf_s(buf, bufsize,
		"\tattack {"			"\r\n\t\t"
			"lastx, lasty = %d, %d"	"\r\n\t\t"
			"count = %lu"			"\r\n\t"
		"}",
		p->lastx, p->lasty,
		p->count
	);
}

void CUserLocal_str(__in CUserLocal *p, __out char *buf, size_t bufsize) {
	char mobinfo[512] = { 0 };
	char anim[512] = { 0 };
	char pvc[512] = { 0 };
	char atk[512] = { 0 };

	mobinfo_str(&p->mobinfo, mobinfo, 512);
	animation_str(&p->animation, anim, 512);
	IWzVector2D_str(p->m_pvc, pvc, 512);
	attack_str(&p->attack, atk, 512);

	sprintf_s(buf, bufsize,
		"CUserLocal {\r\n"
			/*"vtable = %.08X"		"\r\n\t"*/
			"%s"					"\r\n\t"
			"breath = %lu"			"\r\n"
			"%s"					"\r\n"
			"%s"					"\r\n\t"
			"teleport_toggle: %lu"	"\r\n"
			"%s"					"\r\n\t"
			"x, y = %d, %d"			"\r\n"
		"}",
		/*p->vtable,*/
		mobinfo,
		p->breath,
		anim, 
		pvc,
		p->teleport_toggle,
		atk,
		p->x, p->y
	);
}

MobNode *CMobPool_first(__in CMobPool *p) {
	if (!p->pfirst_plus_0x10) {
		return NULL;
	}

	return (MobNode *)(p->pfirst_plus_0x10 - 0x10);
}

void CMobPool_str(__in CMobPool *p, __out char *buf, size_t bufsize) {
	sprintf_s(buf, bufsize,
		"CMobPool {\r\n\t"
			"size: %d"				"\r\n"
		"}",
		p->size
	);
}

void CDropPool_str(__in CDropPool *p, __out char *buf, size_t bufsize) {
	sprintf_s(buf, bufsize, 
		"CDropPool {\r\n\t"
			"size: %d"				"\r\n"
		"}",
		p->size
	);
}

void CWvsPhysicalSpace2D_str(__in CWvsPhysicalSpace2D *p, __out char *buf, size_t bufsize) {
	sprintf_s(buf, bufsize,
		"CWvsPhysicalSpace2D {\r\n\t"
			"left: %d"				"\r\n\t"
			"top: %d"				"\r\n\t"
			"right: %d"				"\r\n\t"
			"bottom: %d"			"\r\n"
		"}",
		p->bounds.left, 
		p->bounds.top, 
		p->bounds.right, 
		p->bounds.bottom
	);
}

void CUIStatusBar_str(__in CUIStatusBar *p, __out char *buf, size_t bufsize) {
	sprintf_s(buf, bufsize,
		"CUIStatusBar {\r\n\t"
			"hp: %d"				"\r\n\t"
			"mp: %d"				"\r\n"
		"}",
		p->hp,
		p->mp
	);
}

void CWvsContext_str(__in CWvsContext *p, __out char *buf, size_t bufsize) {
	sprintf_s(buf, bufsize,
		"CWvsContext {\r\n\t"
			"world: %d"				"\r\n\t"
			"unk: %d"				"\r\n\t"
			"channel: %d"			"\r\n"
		"}",
		p->worldid, 
		p->unk, 
		p->channel
	);
}