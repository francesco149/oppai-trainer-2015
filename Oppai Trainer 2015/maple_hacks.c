/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of Oppai Trainer 2015.
	Oppai Trainer 2015 is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	Oppai Trainer 2015 is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with Oppai Trainer 2015. If not, see <http://www.gnu.org/licenses/>.
*/

#include "maple_hacks.h"

#include <hackdesu/command_queue.h>
#include <hackdesu/string.h>
#include <hackdesu/memory.h>
#include <hackdesu/hack.h>

#include "maple_structs.h"
#include "maple_macros.h"
#include "win32_gui.h"
#include "hacks.h"

#include <Windows.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#ifdef HWIDWINAPI
#include <iphlpapi.h>
#endif

/* find the current process' maple window */
HWND maple_window() {
	char buf[256] = { 0 };
	DWORD procid = 0;
	HWND hwnd = NULL;

	for (hwnd = GetTopWindow(NULL); hwnd != NULL; hwnd = GetNextWindow(hwnd, GW_HWNDNEXT)) {
		GetWindowThreadProcessId(hwnd, &procid);

		if (procid != GetCurrentProcessId())
			continue;

		if (!GetClassNameA(hwnd, buf, 256))
			continue;

		if (strcmp(buf, "MapleStoryClass") != 0)
			continue;

		break;
	}

	return hwnd;
}

/* maplestory's timestamp function */
typedef int(__cdecl *pfnget_update_time)();
extern const pfnget_update_time get_update_time;

static void(*onupdate[16])() = { 0 };
static int onupdate_count = 0;

static void setupdatefunc(void *param) {
	onupdate[onupdate_count] = (void(*)())param;
	onupdate_count++;
}

/* 
 * the given callback will be called on each update after all commands have been processed by the queue.
 */
void maple_update_func(__in void(*callback)()) {
	cq_callback cb = { setupdatefunc };
	cq_call(cb, (void *)callback, cq_no_callback);
}

/*
 * Main Loop Hook (CWndMan::s_Update). This will be called on each frame (around 67 times a second).
 * The one that calls timeGetTime, scroll to top: 
 * FF 15 ?? ?? ?? ?? 2B 05 ?? ?? ?? ?? 3D ?? ?? ?? ?? 0F
 * FF 15 ? ? ? ? 2B ? ? ? ? ? ? ? ? ? ? 0F 86 ? ? ? ? ? 8B
 */
typedef void(__cdecl *pfnCWndMan__s_Update)();
static pfnCWndMan__s_Update CWndMan__s_Update = addrT(pfnCWndMan__s_Update, 01640100); /* v161.1 */

static void kami_onupdate(); /* kami stuff */
static void mousefly_onupdate(); /* mouse fly stuff */
static void slowtubi_onupdate(); /* slow tubi stuff */
extern int mousefly_enabled;

static void __cdecl CWndMan__s_Update_hook() {
	int i;
	HWND maplewnd;

	CWndMan__s_Update();
	cq_process_all();
	for (i = 0; i < onupdate_count; i++) {
		onupdate[i]();
	}
	kami_onupdate();
	mousefly_onupdate();
	slowtubi_onupdate();
	maple_processmacros();

	/* hotkeys (I could do this by hooking OnKey but I'm too lazy) */
	maplewnd = maple_window();
	if (maplewnd && GetActiveWindow() == maplewnd) {
		if (hacks_ismouseflyenabled() && GetAsyncKeyState(VK_F10) & 1) {
#if _DEBUG
			puts("F10 hotkey pressed, toggling mousefly");
#endif
			mousefly_enabled ^= 1;
		}

		if (GetAsyncKeyState(VK_F11) & 1) {
			hacks_onkamihotkey();
		}

		if (GetAsyncKeyState(VK_F12) & 1) {
			hacks_onkamiloothotkey();
		}
	}
}

int maple_hookupdate(int enabled) {
	return memory_detour(enabled, (void **)&CWndMan__s_Update, CWndMan__s_Update_hook);
}

#if 0
/*
 * Black Cipher bypass (nop 3 readfile calls, found by Taku)
 * addy 1: FF ? 0F 81 ? ? ? ? 60 E9 ? ? ? ? AF ? ? ? ? ? F4 6B
 * addy 2: FF ? E9 ? ? ? ? 86 ? EB ? 11
 * addy 3: FF ? ? ? ? 60 61 ? ? 0F ? ? ? ? ? ? ? 0F 89
 */
static const uint32_t blackcipher1 = addrT(uint32_t, 29F53F);
static const uint32_t blackcipher2 = addrT(uint32_t, 297022);
static const uint32_t blackcipher3 = addrT(uint32_t, 2AB79B);

/* bypasses black cipher if it isn't already bypassed. returns 1 on success. */
int maple_blackcipher_trybypass() {
	uint8_t *ngclient;
	uint32_t shit;
	ngclient = (uint8_t *)GetModuleHandleA("NGClient.aes");
	if (!ngclient) { 
		win32_show_error("GetModuleHandleA failed for NGClient.aes");
	}

	memory_protect(ngclient + blackcipher1, 2, PAGE_EXECUTE_READWRITE, &shit);
	memory_protect(ngclient + blackcipher2, 2, PAGE_EXECUTE_READWRITE, &shit);
	memory_protect(ngclient + blackcipher3, 2, PAGE_EXECUTE_READWRITE, &shit);
	if (*(ngclient + blackcipher1) != 0xFF &&
		*(ngclient + blackcipher2) != 0xFF &&
		*(ngclient + blackcipher3) != 0xFF) {
#if _DEBUG
		puts("Black Cipher is already bypassed");
#endif
		return 0;
	}

#if _DEBUG
	puts("Bypassing Black Cipher...");
#endif
	memory_write2(ngclient + blackcipher1, 0x9090);
	memory_write2(ngclient + blackcipher2, 0x9090);
	memory_write2(ngclient + blackcipher3, 0x9090);
#if _DEBUG
	puts("Done!");
#endif

	return 1;
}
#endif

/*
 * Block Crash Report 
 * (old v159.2) 1st addy (scroll to the beginning of the func): 68 ? ? ? ? ? 8B ? ? ? ? ? ? ? ? ? ? 8D ? ? ? ? ? ? 68
 * 1st addy (scroll to the beginning of the func): 68 ? ? 00 00 E8 ? ? ? ? 83 ? ? 6A ? 8D
 * (old v159.2) 2nd addy (scroll to the beginning of the func): 68 ? ? ? ? ? 8B ? ? ? ? 8B ? ? ? ? ? ? 8D ? ? ? ? ? ? 68
 * 2nd addy (scroll to the beginning of the func): removed?
 */
static void * const crashreport1 = addr(0166366C); /* v161.1 test this pls */
/*static void * const crashreport2 = addr(015F1EE0);*/

/* prevents maple from creating crash reports */
void maple_blockcrashreports() {
#if _DEBUG
	puts("Blocking crash reports...");
#endif
	memory_write2(crashreport1, 0x90C3);
	/*memory_write2(crashreport2, 0x90C3);*/
#if _DEBUG
	puts("Done!");
#endif
}

/*
 * Tubi (CWvsContext::CanSendExclRequest)
 * 8B ? ? ? ? 8B ? ? 8D ? ? ? 00 00 E8 ? ? ? ? E8 ? ? ? ? ? 8D
 */
static memory_patch tubi = { addr(0167D580), "B8 00 00 00 00 C2 04 00" }; /* v161.1 */
static generic_hack htubi[] = {
	{ HACK_MEMPATCH, &tubi }
};
hack maple_tubi = { 1, htubi };

static int slowtubi_enabled = 0;
static void slowtubi_toggle(void *param) {
	slowtubi_enabled = (int)param;
}

/* 
 * __int16 __thiscall TSecType<unsigned_long>::SetData(void *this, int a2) 
 * call at 8B ? ? ? ? 8B ? ? 8D ? ? ? 00 00 E8 ? ? ? ? E8 ? ? ? ? ? 8D
 */
typedef __int16(__fastcall *pfnTSecType_unsigned_long___SetData)(void *pthis, void *edx, uint32_t value);
static const pfnTSecType_unsigned_long___SetData TSecType_unsigned_long___SetData = addrT(pfnTSecType_unsigned_long___SetData, 004B4160); /* v161.1 */
static int lastloot = 0;
static void slowtubi_onupdate() {
	CWvsContext *srv;

	if (!slowtubi_enabled) {
		return;
	}

	srv = CWvsContext__GetInstance();
	if (!srv) {
#if _DEBUG
		puts("slowtubi: null serverbase");
#endif
		return;
	}

	if (get_update_time() - lastloot > 30) {

		/*
#if _DEBUG
		puts("zeroing tubi");
#endif
		*/
		TSecType_unsigned_long___SetData(&srv->encrypted_tubi, NULL, 0);
		lastloot = get_update_time();
	}
}

int maple_slowtubi(int enabled) {
	cq_callback cb = { slowtubi_toggle };
	return cq_call(cb, (void *)enabled, cq_no_callback);
}

/*
 * NoKB (third cmp in CVecCtrl::SetImpactNext)
 * 01 00 00 00 D8 D1 DF E0 F6 C4 05 7A 28 DC 91
 * 01 00 00 00 D8 D1 DF E0 F6 C4 ?? 7A ?? DC 91 ?? ?? ?? ?? DF E0 F6 C4 ?? 7A ?? D9 C0
 * 01 00 00 00 ? ? ? ? F6 ? ? 7A ? DC ? ? ? ? ? ? ? F6 ? ? 7A
 */
static memory_patch nokb = { addr(00D4E99A), "00" }; /* v161.1 */
static generic_hack hnokb[] = {
	{ HACK_MEMPATCH, &nokb }
};
hack maple_nokb = { 1, hnokb };

/*
 * Full Godmode (makes CUserLocal::SetDamaged return immediately)
 * 6A ? 68 ? ? ? ? 64 ? ? ? ? ? ? 81 ? ? ? ? ? ? ? ? ? ? ? ? ? ? 33 ? ? 8D ? ? ? ? ? ? 64 ? ? ? ? ? 8B ? C7 ? ? ? ? ? ? ? E8 ? ? ? ? 8B
 */
#define timedgodmode_base 015182F0 /* v161.1 */
static memory_patch fullgodmode = { addr(timedgodmode_base), "C2 2C 00" };
static generic_hack hfullgodmode[] = {
	{ HACK_MEMPATCH, &fullgodmode }
};
hack maple_fullgodmode = { 1, hfullgodmode };

/*
 * Timed Godmode
 * 6A ? 68 ? ? ? ? 64 ? ? ? ? ? ? 81 ? ? ? ? ? ? ? ? ? ? ? ? ? ? 33 ? ? 8D ? ? ? ? ? ? 64 ? ? ? ? ? 8B ? C7 ? ? ? ? ? ? ? E8 ? ? ? ? 8B
 * get_update_time: call at E8 ? ? ? ? ? 8D ? ? ? E8 ? ? ? ? 8B ? ? ? ? 8D ? ? ? E8 ? ? ? ? ? 8D
 */
extern void timedgodmode_hook();
static hook timedgodmode = { addr(timedgodmode_base), timedgodmode_hook, 2 };
static const uint32_t timedgodmode_ret = (uint32_t)addr(timedgodmode_base + 7);
static const pfnget_update_time get_update_time = addrT(pfnget_update_time, 01595D90); /* v161.1 */
static generic_hack htimedgodmode[] = {
	{ HACK_HOOK, &timedgodmode }
};
hack maple_timedgodmode = { 1, htimedgodmode };

static int timedgodmode_nexthit = 0;
void __declspec(naked) timedgodmode_hook() {
	__asm {
		call get_update_time
		cmp eax, [timedgodmode_nexthit]
		jbe timedgodmode_exit

		call get_update_time
		add eax, 0xE290
		mov [timedgodmode_nexthit], eax
		jmp timedgodmode_exit2

timedgodmode_exit:
		ret 0x002C

timedgodmode_exit2:
		/* original code */
		/* v161.1 */
		push 0xFF
		push 0x0198157B
		jmp timedgodmode_ret
	}
}

/* 
 * Full Mob Disarm (some conditional jump in CUserRemote::OnHit(?)
 * 75 ?? 8B CE E8 ?? ?? ?? ?? 8B CE E8 ?? ?? ?? ?? 8B CE E8 ?? ?? ?? ?? 8B CE E8 ?? ?? ?? ?? 8B
 * 75 ?? 8B CE E8 ?? ?? ?? ?? 8B CE E8 ?? ?? ?? ?? 8B CE E8 ?? ?? ?? ?? 8B CE E8 ?? ?? ?? ?? 8B 96 ?? ?? ?? ?? 8D 86 ?? ?? ?? ?? 52
 * 75 ? 8B ? E8 ? ? ? ? 8B ? E8 ? ? ? ? 8B ? E8 ? ? ? ? 8B ? E8 ? ? ? ? 8B ? ? ? ? ? 8D
 *
 * jump addy:
 * 8B 86 ?? ?? ?? ?? 85 C0 0F 84 ?? ?? ?? ?? 2B 45 ?? 0F 89 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8D ?? ?? ?? ?? ?? ??
 * 8B ? ? ? ? ? 85 ? 0F 84 ? ? ? ? 2B ? ? 0F 89 ? ? ? ? C7
 */
static hook fullmobdisarm = { addr(00AF7AE3), addr(00AF7F40), 4 }; /* v161.1 */
static generic_hack hfullmobdisarm[] = {
	{ HACK_HOOK, &fullmobdisarm }
};
hack maple_fullmobdisarm = { 1, hfullmobdisarm };

/*
 * Lemmings (two conditional jumps in CVecCtrlMob::CollisionDetectWalk)
 * 74 ?? D9 EE DC 5C 24 ?? DF E0 F6 C4 ?? 0F 85 ?? ?? ?? ?? DC 96 ?? ?? ?? ?? DF E0 F6 C4 ?? 0F 85 ?? ?? ?? ?? D9 EE DC 5C 24 ?? DF E0 F6 C4 ?? 7A ?? 8B CE
 * 74 ?? D9 EE DC 5C 24 ?? DF E0 F6 C4 ?? 0F 85 ?? ?? ?? ?? DC 96 ?? ?? ?? ?? DF E0 F6 C4 ??
 * 74 ? ? ? DC ? ? ? ? ? F6 ? ? 0F 85 ? ? ? ? DC ? ? ? ? ? ? ? F6
 */
#define lemmings 01615A3A  /* v161.1 */
static memory_patch lemmings1 = { addr(lemmings),		"90 90" };
static memory_patch lemmings2 = { addr(lemmings + 0xD),	"90 E9" };
static generic_hack hlemmings[] = {
	{ HACK_MEMPATCH, &lemmings1 },
	{ HACK_MEMPATCH, &lemmings2 }
};
hack maple_lemmings = { 2, hlemmings };

/*
 * Logo Skip (conditional jump after test in CLogo::UpdateLog)
 * 74 19 2B F8 81 FF DC 05 00 00 0F 86 ?? ?? 00 00 5F 88 5E ?? C6 46 ?? 00 5E 5B C3
 * 74 ?? 2B F8 81 FF ?? ?? ?? ?? 0F 86 ?? ?? ?? ?? 5F
 * 74 ? 2B ? 81 ? ? ? ? ? 0F 86 ? ? ? ? ? 88 ? ? C6
 */
static memory_patch skiplogo = { addr(00A39829), "75" }; /* v161.1 */
static generic_hack hskiplogo[] = {
	{ HACK_MEMPATCH, &skiplogo }
};
hack maple_skiplogo = { 1, hskiplogo };

/*
 * Block HWID (makes CLogin::GetLocalMacAddressWithHDDSerialNo and CSystemInfo::Init return immediately)
 *
 * CLogin::GetLocalMacAddressWithHDDSerialNo
 * 6A FF 68 ?? ?? ?? 01 64 A1 00 00 00 00 50 B8 B0 2E 00 00 E8 ?? ?? ?? 00 A1 ?? ?? ?? 01 33 C4 89 84 24 AC 2E 00 00 53 55 56 57 A1 ?? ?? ?? 01 33 C4 50 8D 84 24 C4 2E 00
 * 6A ?? 68 ?? ?? ?? ?? 64 A1 ?? ?? ?? ?? 50 B8 ?? ?? ?? ?? E8 ?? ?? ?? ??
 * 6A ? 68 ? ? ? ? 64 ? ? ? ? ? ? ? ? ? ? ? E8 ? ? ? ? ? ? ? ? ? ? ? 89 ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? 33 ? ? 8D ? ? ? ? ? ? 64 ? ? ? ? ? 8B ? ? ? ? ? ? 89 ? ? ? ? ? C6
 *
 * CSystemInfo::Init
 * 81 EC ?? ?? ?? ?? 33 C0 53 89 44 24 ?? 56
 * 81 EC ? ? ? ? 33 ? ? 89 ? ? ? ? 89
 */
static memory_patch hwid1 = { addr(009DA710), "C2 04 00 90 90 90 90" };	/* v161.1 */
static memory_patch hwid2 = { addr(01749CB0), "C3 90 90 90 90 90" };		/* v161.1 */
static generic_hack hhwid[] = {
	{ HACK_MEMPATCH, &hwid1 }, 
	{ HACK_MEMPATCH, &hwid2 },
};
hack maple_blockhwid = { 2, hhwid };

/*
 * Randomize HWID
 */
extern void macformat_hook();
extern void macformat2_hook();
static const uint8_t * __fastcall  CSystemInfo__GetMachineId_hook(void *pthis, void *edx);

#ifdef HWIDWINAPI
static const char *rndcharset = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

typedef DWORD (WINAPI *pfnGetAdaptersInfo)(
	_Out_    PIP_ADAPTER_INFO pinfo,
	_Inout_  PULONG plen
);
static pfnGetAdaptersInfo pGetAdaptersInfo = NULL;
static DWORD WINAPI GetAdaptersInfo_hook(
	_Out_    PIP_ADAPTER_INFO pinfo,
	_Inout_  PULONG plen
);

typedef BOOL (WINAPI *pfnGetVolumeInformationA)(
	_In_opt_   LPCSTR rootpath,
	_Out_opt_  LPSTR volumename,
	_In_       DWORD volumename_len,
	_Out_opt_  LPDWORD pvolumeserialno,
	_Out_opt_  LPDWORD pmaxcomponentlength,
	_Out_opt_  LPDWORD pfsflags,
	_Out_opt_  LPSTR fsname,
	_In_       DWORD fsname_len
);
static pfnGetVolumeInformationA pGetVolumeInformationA = NULL;
static BOOL WINAPI GetVolumeInformationA_hook(
	_In_opt_   LPCSTR rootpath,
	_Out_opt_  LPSTR volumename,
	_In_       DWORD volumename_len,
	_Out_opt_  LPDWORD pvolumeserialno,
	_Out_opt_  LPDWORD pmaxcomponentlength,
	_Out_opt_  LPDWORD pfsflags,
	_Out_opt_  LPSTR fsname,
	_In_       DWORD fsname_len
);
#endif

/*
 * CLogin::GetLocalMacAddressWithHDDSerialNo (scroll down to where it pushes the string format specifier and hook the 10th push above)
 * 6A ? 68 ? ? ? ? 64 ? ? ? ? ? ? ? ? ? ? ? E8 ? ? ? ? ? ? ? ? ? ? ? 89 ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? 33 ? ? 8D ? ? ? ? ? ? 64 ? ? ? ? ? 8B ? ? ? ? ? ? 89 ? ? ? ? ? C6
 * or just scan for the hook point directly: ? C1 ? ? 0F B6 ? ? 0F B6 ? ? ? ? ? ? C1 ? ? 0F
 *
 * const unsigned char * __thiscall CSystemInfo::GetMachineId(void)
 * 8D 41 14 C3
 *
 * CLogin::GetLocalMacAddress (scroll down to where it pushes the string format specifier and hook the 6th push above)
 * 6A ? 68 ? ? ? ? 64 ? ? ? ? ? ? 81 ? ? ? ? ? ? ? ? ? ? 33 ? 89 ? ? ? ? ? ? ? ? ? ? ? ? ? ? 33 ? ? 8D ? ? ? ? ? ? 64 ? ? ? ? ? 8B ? ? ? ? ? ? 33 ? 33
 * hook point: ? 0F B6 ? ? ? ? ? ? ? 0F B6 ? ? ? ? ? ? ? 0F B6 ? ? ? ? ? ? ? ? ? 8D ? ? ? 68 ? ? ? ? ? E8 ? ? ? ? 83
 *
 * Should also hook GetVolumeInformationA and GetAdaptersInfo just to be sure.
 */
static hook rhwid1 =       { addr(009DA90C), macformat_hook, 2 };							/* v161.1 */
static uint32_t rhwid1_ret = addrT(uint32_t, 009DA95A);										/* v161.1 lea above the string format specifier push */
static hook rhwid2 =       { addr(01749F80), (void *)CSystemInfo__GetMachineId_hook, 0 };	/* v161.1 */
static hook rhwid3 =	   { addr(009DA658), macformat2_hook, 4 };							/* v161.1 */
static uint32_t rhwid3_ret = addrT(uint32_t, 009DA676);										/* v161.1 lea above the string format specifier push */
#ifdef HWIDWINAPI
static winapidetour rhwid5 = { "Kernel32.dll", "GetVolumeInformationA", (void **)&pGetVolumeInformationA, (void *)GetVolumeInformationA_hook };
static winapidetour rhwid6 = { "Iphlpapi.dll", "GetAdaptersInfo",		(void **)&pGetAdaptersInfo,		  (void *)GetAdaptersInfo_hook };
#endif
static generic_hack hrhwid[] = {
	{ HACK_MEMPATCH, &hwid2 }, /* block CSystemInfo::Init from "Block HWID" */
	{ HACK_HOOK, &rhwid1 },
	{ HACK_HOOK, &rhwid2 },
	{ HACK_HOOK, &rhwid3 },
#ifdef HWIDWINAPI
	{ HACK_WINAPIDETOUR, &rhwid5 },
	{ HACK_WINAPIDETOUR, &rhwid6 },
#endif
};
#ifdef HWIDWINAPI
hack maple_randomizehwid = { 6, hrhwid };
#else
hack maple_randomizehwid = { 4, hrhwid };
#endif

/* I'm not sure about the size of GetMachineId so I'm gonna leave some room */
static uint8_t random_hwid[512] = { 0 };
static int hwid_initialized = 0;
void hwid_tryinit() {
	int i = 0;
	if (hwid_initialized) {
		return;
	}

	for (i = 0; i < 512; i++) {
		random_hwid[i] = (uint8_t)(rand() % 0x100);
	}

#if _DEBUG
	puts("Random HWID initialized.");

	printf(
		"CLogin::GetLocalMacAddressWithHDDSerialNo "
		"will use this random MAC and HDD serial: "
		"%02X%02X%02X%02X%02X%02X_%02X%02X%02X%02X\n",
		random_hwid[9],
		random_hwid[8],
		random_hwid[7],
		random_hwid[6],
		random_hwid[5],
		random_hwid[4],
		random_hwid[3],
		random_hwid[2],
		random_hwid[1],
		random_hwid[0]
	);
	printf(
		"CSystemInfo::GetMachineId (first 16 bytes after cookie in login packet) will return "
		"%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\n",
		random_hwid[10],
		random_hwid[11],
		random_hwid[12],
		random_hwid[13],
		random_hwid[14],
		random_hwid[15],
		random_hwid[16],
		random_hwid[17],
		random_hwid[18],
		random_hwid[19], 
		random_hwid[20],
		random_hwid[21],
		random_hwid[22],
		random_hwid[23],
		random_hwid[24],
		random_hwid[25]
	);
#endif

	hwid_initialized = 1;
}

static const uint8_t * __fastcall  CSystemInfo__GetMachineId_hook(void *pthis, void *edx) {
	if (!hwid_initialized) {
#if _DEBUG
		puts("Warning! Your forgot to call hwid_tryinit!");
#endif
	}
	return (const uint8_t *)&random_hwid[10];
}

static void __declspec(naked) macformat_hook() {
	__asm {
		movzx ecx, random_hwid[0]
		push ecx
		movzx ecx, random_hwid[1]
		push ecx
		movzx ecx, random_hwid[2]
		push ecx
		movzx ecx, random_hwid[3]
		push ecx
		movzx ecx, random_hwid[4]
		push ecx
		movzx ecx, random_hwid[5]
		push ecx
		movzx ecx, random_hwid[6]
		push ecx
		movzx ecx, random_hwid[7]
		push ecx
		movzx ecx, random_hwid[8]
		push ecx
		movzx ecx, random_hwid[9]
		push ecx
		jmp rhwid1_ret
	}
}

static void __declspec(naked) macformat2_hook() {
	__asm {
		movzx ecx, random_hwid[4]
		push ecx
		movzx ecx, random_hwid[5]
		push ecx
		movzx ecx, random_hwid[6]
		push ecx
		movzx ecx, random_hwid[7]
		push ecx
		movzx ecx, random_hwid[8]
		push ecx
		movzx ecx, random_hwid[9]
		push ecx
		jmp rhwid3_ret
	}
}

#ifdef HWIDWINAPI
static DWORD WINAPI GetAdaptersInfo_hook(
	_Out_    PIP_ADAPTER_INFO pinfo,
	_Inout_  PULONG plen
) {
	DWORD res;
	PIP_ADAPTER_INFO ai = NULL;
	int ri = 26;
	UINT i;

#if _DEBUG
	printf("GetAdaptersInfo called with *plen=%lu from %.08X\n", *plen, _ReturnAddress());
#endif
	res = pGetAdaptersInfo(pinfo, plen);

	if (res != ERROR_SUCCESS || !*plen) {
		return res;
	}

	ai = pinfo;
	do {
#if _DEBUG
		printf("\n%s:\n", ai->AdapterName);
#endif
		/*if (*plen == 10368) {*/
			string_rand(ai->AdapterName, rndcharset, MAX_ADAPTER_NAME_LENGTH / 8);
			string_rand(ai->Description, rndcharset, MAX_ADAPTER_DESCRIPTION_LENGTH / 8);
		/*}*/
		for (i = 0; i < ai->AddressLength; i++) {
			ai->Address[i] = random_hwid[ri];
			ri++;
			if (ri > 511) {
				ri = 26;
			}
		}
		/*if (*plen == 10368) {*/
			ai->Index = rand();
#if _DEBUG
			printf("Randomized AdapterName: %s\n", ai->AdapterName);
			printf("Randomized Description: %s\n", ai->Description);
#endif
		/*}*/
#if _DEBUG
		printf("Randomized MAC: ");
		for (i = 0; i < ai->AddressLength; i++) {
			printf("%02X ", ai->Address[i]);
		}
		printf("\n");
#endif

		ai = ai->Next;
	} while (ai);

	return res;
}

static BOOL WINAPI GetVolumeInformationA_hook(
	_In_opt_   LPCSTR rootpath,
	_Out_opt_  LPSTR volumename,
	_In_       DWORD volumename_len,
	_Out_opt_  LPDWORD pvolumeserialno,
	_Out_opt_  LPDWORD pmaxcomponentlength,
	_Out_opt_  LPDWORD pfsflags,
	_Out_opt_  LPSTR fsname,
	_In_       DWORD fsname_len
) {
	BOOL res;

#if _DEBUG
	printf("GetVolumeInformationA called for rootpath %s from %.08X\n", rootpath, _ReturnAddress());
#endif
	res = pGetVolumeInformationA(rootpath, volumename, volumename_len, pvolumeserialno, 
		pmaxcomponentlength, pfsflags, fsname, fsname_len);

	if (volumename) {
#if _DEBUG
		printf("real name: %s\n", volumename);
#endif
		string_rand(volumename, rndcharset, volumename_len / 4);
#if _DEBUG
		printf("spoofed name to: %s\n", volumename);
#endif
	}

	if (pvolumeserialno) {
#if _DEBUG
		printf("real sn: %.08X\n", *pvolumeserialno);
#endif
		*pvolumeserialno = rand();
#if _DEBUG
		printf("spoofed sn to: %.08X\n", *pvolumeserialno);
#endif
	}

	return res;
}
#endif

/* 
 * CPU Hack
 *
 * 4 calls to CWvsPhysicalSpace2D::Load, CMapLoadable::RestoreTile, 
 * CMapLoadable::RestoreBack and CMapLoadable::RestoreWeather 
 * in CMapLoadable::LoadMap
 * E8 ?? ?? ?? ?? 8B CB E8 ?? ?? ?? ?? 8B CB E8 ?? ?? ?? ?? 8B CB E8 ?? ?? ?? ?? 8B CB E8 ?? ?? ?? ?? 57
 * E8 ? ? ? ? 8B ? E8 ? ? ? ? 8B ? E8 ? ? ? ? 8B ? E8 ? ? ? ? 8B ? E8 ? ? ? ? 8B ? E8 ? ? ? ? ? 8B ? E8
 *
 * no mob reaction, no damage, no hitmarks
 * 6A ?? 68 ?? ?? ?? ?? 64 A1 ?? ?? ?? ?? 50 83 EC ?? 53 55 56 57 A1 ?? ?? ?? ?? 33 C4 50 8D 44 24 ?? 64 A3 ?? ?? ?? ?? 8B F1 89 74 24 ?? A1 ?? ?? ?? ?? 33 FF
 * 6A ? 68 ? ? ? ? 64 ? ? ? ? ? ? 83 ? ? ? ? ? ? A1 ? ? ? ? 33 ? ? 8D ? ? ? 64 ? ? ? ? ? 8B ? 89 ? ? ? A1 ? ? ? ? 33 ? 89
 *
 * no skill animation
 * 0F 85 ?? ?? ?? ?? 8B 7D ?? 3B FB 0F 84 ?? ?? ?? ?? E8 ?? ?? ?? ?? 85 C0
 * 0F 85 ? ? ? ? 8B ? ? 3B ? 0F 84 ? ? ? ? E8 ? ? ? ? 85
 */
#define maple_cpuhackbase 00A6AF30														/* v161.1 */
static memory_patch cpu1 = { addr(maple_cpuhackbase),		"90 90 90 90 90" };
static memory_patch cpu2 = { addr(maple_cpuhackbase + 7),	"90 90 90 90 90" };
static memory_patch cpu3 = { addr(maple_cpuhackbase + 14),	"90 90 90 90 90" };
static memory_patch cpu4 = { addr(maple_cpuhackbase + 21),	"90 90 90 90 90" };
static memory_patch cpu5 = { addr(00ADD0F0),				"C2 58 00 90 90 90 90" };	/* thanks Nickerian, v161.1 */
static memory_patch cpu6 = { addr(0145BA50),				"90 E9" };					/* thanks Nickerian, v161.1 */
static generic_hack hcpu[] = {
	{ HACK_MEMPATCH, &cpu1 },
	{ HACK_MEMPATCH, &cpu2 },
	{ HACK_MEMPATCH, &cpu3 },
	{ HACK_MEMPATCH, &cpu4 },
	{ HACK_MEMPATCH, &cpu5 },
	{ HACK_MEMPATCH, &cpu6 },
};
hack maple_cpuhack = { 6, hcpu };

/*
 * Magic godmode (makes CMob::ProcessAttack return immediately)
 * 55 8D ?? ?? 8C 83 ?? ?? 6A ?? 68 ?? ?? ?? ?? 64 ?? ?? ?? ?? ?? 50 83 ?? ?? A1 ?? ?? ?? ?? 33 ?? 89 ?? ?? 53 56 57 50 8D ?? ?? 64 ?? ?? ?? ?? ?? 8B ?? 8B ?? ?? ?? ?? ?? 8B ?? ?? ?? ?? ?? 51
 * 55 8D 6C 24 ?? 83 EC ?? 6A ?? 68 ?? ?? ?? ?? 64 A1 ?? ?? ?? ?? 50 83 EC ?? A1 ?? ?? ?? ?? 33 C5 89 45 ?? 53 56 57 50 8D 45 ?? 64 A3 ?? ?? ?? ?? 8B F9 8B 87 ?? ?? ?? ?? 8B 88 ?? ?? ?? ?? 51 05 ?? ?? ?? ?? 50
 * ? 8D ? ? ? 83 ? ? 6A ? 68 ? ? ? ? 64 ? ? ? ? ? ? 83 ? ? ? ? ? ? ? 33 ? 89 ? ? ? ? ? ? 8D ? ? 64 ? ? ? ? ? 8B ? 8B ? ? ? ? ? 8B ? ? ? ? ? ? ? ? ? ? ? ? E8
 */
static memory_patch magicgodmode = { addr(00A9C9B0), "C2 04 00 90 90" }; /* v161.1 */
static generic_hack hmagicgodmode[] = {
	{ HACK_MEMPATCH, &magicgodmode },
};
hack maple_magicgodmode = { 1, hmagicgodmode };

/*
 * Instant Drop (conditional jump in calculate_parbolic_motion_duration)
 * 0D ?? ?? ?? ?? 83 C4 ?? E9 ?? ?? ?? ?? DD 05 ?? ?? ?? ?? DC C9
 * 0D ? ? ? ? 83 ? ? E9 ? ? ? ? DD ? ? ? ? ? ? ? ? ? DD
 */
static memory_patch instadrop = { addr(006F1B35), "25" }; /* v161.1 */
static generic_hack hinstadrop[] = {
	{ HACK_MEMPATCH, &instadrop },
};
hack maple_instadrop = { 1, hinstadrop };

/*
 * Jump Down Anywhere
 * 
 * 1st addy
 * 74 ?? 3B 6C 24 ?? 75 ?? 8B 44 24 ?? 8B 54 24 ?? 6A ?? 03 F0
 * 74 ? 3B ? ? ? 75 ? 8B ? ? ? 8B ? ? ? 6A ? ? ? ? 8D ? ? ? ? 8B
 *
 * 2nd addy (some cond jump in CUserLocal::FallDown)
 * 7D ?? 8B 16 8B 52 ?? 8D 44 24 ?? 50
 * 7D ? 8B ? 8B ? ? 8D ? ? ? ? 8B ? FF ? 8B ? ? 83 ? ? 39 ? ? ? 7C ? 85 ? 74 ? 8B ? C7 ? ? ? ? ? ? ? ? ? E8 ? ? ? ? 89
 *
 * 3rd addy 
 * last: 74 ?? 8B ?? C7 ?? ?? ?? ?? ?? ?? ?? ?? ?? E8
 * or:   74 ?? 8B CB C7 83 ?? ?? ?? ?? ?? ?? ?? ?? E8 ?? ?? ?? ?? 6A ?? 6A ?? 8B CB 89 83 ?? ?? ?? ?? E8 ?? ?? ?? ?? 6A ?? 6A ?? 6A ?? 6A ?? 6A ?? 6A ?? 8B CB E8 ?? ?? ?? ?? 5D
 *       74 ? 8B ? C7 ? ? ? ? ? ? ? ? ? E8 ? ? ? ? 6A ? 6A ? 8B ? 89 ? ? ? ? ? E8 ? ? ? ? 6A ? 6A ? 6A ? 6A ? 6A ? 6A ? 8B ? E8
 */
static memory_patch jda1 = { addr(014B9552), "EB" };		/* v161.1 */
static memory_patch jda2 = { addr(00AC01AF), "EB" };		/* v161.1 */
static memory_patch jda3 = { addr(014B95AF), "90 90" };	/* v161.1 */
static generic_hack hjda[] = {
	{ HACK_MEMPATCH, &jda1 },
	{ HACK_MEMPATCH, &jda2 },
	{ HACK_MEMPATCH, &jda3 },
};
hack maple_jda = { 3, hjda };

/*
 * View Swears (cond jump in CCurseProcess::SearchSubstring)
 * 74 ?? 80 3E ?? 75 ?? 0F B6 13 52
 * 74 ? 80 ? ? 75 ? ? ? ? ? FF 15 ? ? ? ? F7
 */
static memory_patch viewswears = { addr(00D48A8B), "90 90" }; /* v161.1 */
static generic_hack hviewswears[] = {
	{ HACK_MEMPATCH, &viewswears },
};
hack maple_viewswears = { 1, hviewswears };

/*
 * Reactor DEM (cond jump in CUserLocal::TryDoingNormalAttack)
 * 74 ?? 83 7D ?? ?? 74 ?? 8B 8D ?? ?? ?? ?? 81 C1 ?? ?? ?? ?? E8 ?? ?? ?? ?? 8B F0
 * 74 ? 83 ? ? ? 74 ? 8B ? ? ? ? ? 81 ? ? ? ? ? E8 ? ? ? ? 8B
 */
static memory_patch reactordem = { addr(014D528B), "90 90" }; /* v161.1 */
static generic_hack hreactordem[] = {
	{ HACK_MEMPATCH, &reactordem },
};
hack maple_reactordem = { 1, hreactordem };

/*
 * No FA (call in CUserLocal::TryDoingMeleeAttack)
 * E8 ?? ?? ?? ?? 85 C0 74 ?? C7 85 ?? ?? ?? ?? ?? ?? ?? ?? EB ?? C7 85 ?? ?? ?? ?? ?? ?? ?? ?? 8A 8D ?? ?? ?? ?? 88 8D ?? ?? ?? ?? 8B 8D ?? ?? ?? ?? E8 ?? ?? ?? ?? 85 C0 0F 95 C2
 * E8 ? ? ? ? 85 ? 74 ? C7 ? ? ? ? ? ? ? ? ? EB ? C7 ? ? ? ? ? ? ? ? ? 8A ? ? ? ? ? 88 ? ? ? ? ? 8B ? ? ? ? ? E8
 */
static memory_patch nofa = { addr(014ECC61), "90 90 90 90 90 90 90" }; /* v161.1 */
static generic_hack hnofa[] = {
	{ HACK_MEMPATCH, &nofa },
};
hack maple_nofa = { 1, hnofa };

/*
 * Unlimited Attack (some cond jump in CAntiRepeat::TryRepeat)
 * 7E ?? 83 F8 ?? 7D ?? 8B 41 ?? 2B C2
 * 7E ? 83 ? ? 7D ? 8B ? ? 2B ? ? ? ? ? ? 7E
 */
static memory_patch ua = { addr(01493790), "EB" }; /* v161.1 */
static generic_hack hua[] = {
	{ HACK_MEMPATCH, &ua },
};
hack maple_ua = { 1, hua };

/*
 * Item Filter (most likely somewhere in CField::OnFieldEffect where it decodes itemid)
 * 8B ? 89 ? ? E8 ? ? ? ? 8B ? 89 ? ? E8 ? ? ? ? 0F ? ? 89 ? ? 8B ? E8 ? ? ? ? 0F
 */
extern void itemfilter_hook();
#define itemfilter_base 006FC30D /* v161.1 */
static hook itemfilter = { addr(itemfilter_base), itemfilter_hook, 0 };
static uint32_t itemfilter_ret = (uint32_t)addr(itemfilter_base + 5);
static generic_hack hitemfilter[] = {
	{ HACK_HOOK, &itemfilter },
};
hack maple_itemfilter = { 1, hitemfilter };

/* item filter globals */
#define MAX_ITEMS 512
static uint32_t itemfilter_meso = 1;
static int itemfilter_mode = 0;
static uint32_t itemfilter_list[MAX_ITEMS] = { 0 };

/* sets the minimum meso amount for item filter */
void maple_itemfilter_setmeso(uint32_t meso) {
#if _DEBUG
	printf("setting itemfilter meso to %lu\n", meso);
#endif
	itemfilter_meso = meso;
}

/* sets the itemfilter mode (0 = accept, 1 = reject) */
void maple_itemfilter_setmode(int mode) {
#if _DEBUG
	printf("setting itemfilter mode to %d\n", mode);
#endif
	itemfilter_mode = mode;
}

/* (called from maple's main thread) adds an item to the item filter list */
static void itemfilter_add(void *param) {
	uint32_t *it;
	uint32_t itemid = (uint32_t)param;

	if (!itemid) {
		assert(0);
		return;
	}

	for (it = (uint32_t *)itemfilter_list; *it; it++) {
		if (*it == itemid) {
			return;
		}
	}

	if (it >= itemfilter_list + MAX_ITEMS) {
#if _DEBUG
		puts("warning: item filter list full");
		return;
#endif
	}

	assert(*it == 0);
	*it = itemid;
#if _DEBUG
	printf("adding %lu to the item filter list\n", itemid);
#endif
}

/* adds an item to the item filter list */
void maple_itemfilter_add(uint32_t itemid) {
	cq_callback cb = { itemfilter_add };
	cq_call(cb, (void *)itemid, cq_no_callback);
}

/* (called from maple's main thread) removes an item from the item filter list */
static void itemfilter_remove(void *param) {
	uint32_t *it;
	size_t size;
	uint32_t itemid = (uint32_t)param;

#if _DEBUG
	printf("removing %lu from the item filter list\n", itemid);

	puts("before:");
	for (it = (uint32_t *)itemfilter_list; *it; it++) {
		printf("%lu, ", *it);
	}
	printf("\n");
#endif

	for (it = (uint32_t *)itemfilter_list; *it; it++) {
		if (*it == itemid) {
			size = itemfilter_list + MAX_ITEMS - it;
			memmove_s(it, size, it + 1, size - 1);
			break;
		}
	}

#if _DEBUG
	puts("after:");
	for (it = (uint32_t *)itemfilter_list; *it; it++) {
		printf("%lu, ", *it);
	}
	printf("\n");
#endif
}

/* removes an item from the item filter list */
void maple_itemfilter_remove(uint32_t itemid) {
	cq_callback cb = { itemfilter_remove };
	cq_call(cb, (void *)itemid, cq_no_callback);
}

static void itemfilter_clear(void *param) {
	(void)param;
	uint32_t *it;

#if _DEBUG
	puts("clearing item filter list");

	puts("before:");
	for (it = (uint32_t *)itemfilter_list; *it; it++) {
		printf("%lu, ", *it);
	}
	printf("\n");
#endif
	for (it = (uint32_t *)itemfilter_list; *it; it++) {
		*it = 0;
	}
#if _DEBUG
	puts("after:");
	for (it = (uint32_t *)itemfilter_list; *it; it++) {
		printf("%lu, ", *it);
	}
	printf("\n");
#endif
}

/* clears the item filter list */
void maple_itemfilter_clear() {
	cq_callback cb = { itemfilter_clear };
	cq_call(cb, NULL, cq_no_callback);
}

static void __declspec(naked) itemfilter_hook() {
	__asm {
		push edx
		mov edx, itemfilter_meso
		cmp eax, edx
		jle FilterMesos
		lea edx, itemfilter_list
		jmp RejectOrAccept

FilterMesos:
		mov[esi+0x40], 0
		jmp End


RejectOrAccept:
		cmp byte ptr[itemfilter_mode], 0
		je AcceptFilter
		cmp byte ptr[itemfilter_mode], 1
		je RejectFilter


AcceptFilter:
		cmp eax, [edx]
		je End
		cmp dword ptr[edx], 0
		je Ignore
		add edx, 4
		jmp AcceptFilter


RejectFilter:
		cmp eax, [edx]
		je Ignore
		cmp dword ptr[edx], 0
		je End
		add edx, 4
		jmp RejectFilter


Ignore:
		cmp eax, 60000 /* added this code otherwise mesos is dropped but not shown in accept mode */
		jle End
		mov eax, 0

End:
		pop edx

		/* original code, v161.1 */
		mov ecx, ebx
		mov[esi+0x44], eax
		jmp itemfilter_ret
	}
}

/*
 * Suck Left
 * 75 ? DC ? DF ? F6 ? ? 75
 * 75 ? DC ? ? ? F6 ? ? 75
 */
static memory_patch suckleft = { addr(015FEA60), "DD D0" }; /* v161.1 */
static generic_hack hsuckleft[] = {
	{ HACK_MEMPATCH, &suckleft },
};
hack maple_suckleft = { 1, hsuckleft };

/*
 * Swim Hack
 * 74 ? 8B BE ? ? 00 00 85 FF 74 ? DD 46 ? E8
 * 74 ? 8B ? ? ? 00 00 ? ? 74 ? DD
 */
static memory_patch swimhack = { addr(01604C19), "EB" }; /* v161.1 */
static generic_hack hswimhack[] = {
	{ HACK_MEMPATCH, &swimhack },
};
hack maple_swimhack = { 1, hswimhack };

/*
 * Air checks (made by Taku/Peru)
 * warrior: 74 ? 8B ? ? ? ? ? 8B ? 8B ? ? ? ? ? 8B ? ? ? ? ? FF ? 85
 * archer: 75 ? 8B ? ? ? ? ? 8B ? 8B ? ? ? ? ? 8B ? ? FF ? 85 ? 75 ? 8B ? ? ? ? ? 8B
 * magician: 0F 85 ? ? ? ? 8B ? ? ? ? ? E8 ? ? ? ? 85 ? 74 ? 8B ? ? ? ? ?  ? E8 ? ? ? ? 83
 */
static memory_patch airwarrior = { addr(015090EF), "EB" };		/* v161.1 */
static memory_patch airarcher = { addr(014FFCD2), "EB" };		/* v161.1 */
static memory_patch airmagician = { addr(01508F21), "90 E9" };	/* v161.1 */
static generic_hack hairchecks[] = {
	{ HACK_MEMPATCH, &airwarrior }, 
	{ HACK_MEMPATCH, &airarcher },
	{ HACK_MEMPATCH, &airmagician },
};
hack maple_airchecks = { 3, hairchecks };

/*
 * No Codex Box (by kevintjuh93)
 * first addy 0F 85 ? ? ? ? 83 ? ? 0F 84 ? ? ? ? 85 ? 0F 85
 * second addy 0F 85 ? ? ? ? 8B ? ? ? 8B ? ? 8B ? C1
 */
static memory_patch nocodex1 = { addr(0171D2EB), "90 E9" };
static memory_patch nocodex2 = { addr(016C2127), "90 E9" };
static generic_hack hnocodex[] = {
	{ HACK_MEMPATCH, &nocodex1 },
	{ HACK_MEMPATCH, &nocodex2 },
};
hack maple_nocodex = { 2, hnocodex };

/* 
 * - [Kami stuff] --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 */

typedef BOOL (WINAPI *pfnPtInRect)(
	_In_  const RECT *lprc,
	_In_  POINT pt
);
static pfnPtInRect pPtInRect = NULL;

static BOOL WINAPI PartyInRectum(
	_In_  const RECT *lprc,
	_In_  POINT pt
);

/* 
 * item hook ret addy cmp: 
 * 85 C0 75 ? 8D ? 24 ? C7 ? 24 ? ? ? ? ? E8 ? ? ? ? 8B 
 */
static void *itemhookret = addr(006F477E); /* v161.1 */
static winapi_detour itemhook = { "User32.dll", "PtInRect", (void **)&pPtInRect, (void *)PartyInRectum };
static generic_hack hitemhook[] = {
	{ HACK_WINAPI_DETOUR, &itemhook }
};
hack maple_itemhook = { 1, hitemhook };

static POINT itempos = { 0 };
static BOOL WINAPI PartyInRectum(
	_In_  const RECT *lprc,
	_In_  POINT pt
) {
	if (_ReturnAddress() == itemhookret) {
		itempos = pt;
	}

	return pPtInRect(lprc, pt);
}

#define KAMI_CLOSEST

/* getpos shit disabled because I cbf to figure out how to detect dead mobs for now */
#ifdef KAMI_GETPOS
/*
	GetPos's prototype is:
	virtual struct tagPOINT const __thiscall CMob::GetPos(void)const

	but it compiles to:

	int __thiscall CMob__GetPos(void *this, int a2) {
		int result; // eax@1
		int v3; // esi@1
		int v4; // edi@1
		int v5; // ecx@1

		v3 = (int)((char *)this + 1332);
		v4 = TSecType_long___GetData((char *)this + 1344);
		v5 = TSecType_long___GetData(v3);
		result = a2;
		*(_DWORD *)a2 = v4;
		*(_DWORD *)(a2 + 4) = v5;
		return result;
	}

	(where a2 and the return value are pointers to the tagPOINT struct)
	and since this isn't C++ we have to call it the way the compiler internally does
*/

/*
 * CMob::Init = 00AEBB20 v161.1
 * 6A ?? 68 ?? ?? ?? ?? 64 A1 ?? ?? ?? ?? 50 81 EC ?? ?? ?? ?? 53 55 56 57 A1 ?? ?? ?? ?? 33 C4 50 8D 84 24 ?? ?? ?? ?? 64 A3 ?? ?? ?? ?? 8B F1 E8 ?? ?? ?? ?? 8B 8C 24 ?? ?? ?? ?? 8D 96 ?? ?? ?? ?? 89 44 24 ?? C7 86 ?? ?? ?? ?? ?? ?? ?? ?? E8 ?? ?? ?? ?? 8B CE
 *
 * CMob::CMob = 00AE3EC0 v161.1
 * 6A ?? 68 ?? ?? ?? ?? 64 A1 ?? ?? ?? ?? 50 83 EC ?? 53 55 56 57 A1 ?? ?? ?? ?? 33 C4 50 8D 44 24 ?? 64 A3 ?? ?? ?? ?? 8B F1 89 74 24 ?? E8 ?? ?? ?? ?? 33 DB 8D 8E ?? ?? ?? ?? C7 06 ?? ?? ?? ??
 *
 * 5th entry in 2nd vtable is GetPos
 * CMob::GetPos = 00A9C6A0 v161.1
 */

/* 
	find all GetPos funcs: 
	56 8D B1 ? ? 00 00 57 8D 4E 0C E8 ? ? ? ? 8B CE 8B F8 E8 ? ? ? ? 8B C8 8B 44 24 0C 89 38 5F

	004CD730: ? v161.1
	0060BF60: ? v161.1
	00654A00: x always 0 and y seems to be some kind of counter v161.1
	006F0220: never called v161.1
	0073E4C0: never called v161.1
	008BA4B0: never called v161.1
	00A9C6A0: kinda works but occasionally gets stuck on weird positions. most likely CMob::GetPos. v161.1
	00BCB450: never called v161.1
	00D572C0: never called v161.1
*/
typedef int(__fastcall *pfnCMob__GetPos)(void *pthis, void *edx, int ppoint);
static pfnCMob__GetPos pCMob__GetPos = addrT(pfnCMob__GetPos, 00A9C6A0); /* 161.1 */
typedef int(__stdcall *pfninvoke_CMob__GetPos)(void *pthis, int ppoint);
void __declspec(naked) invoke_CMob__GetPos() {
	__asm {
		push ebp
			mov ebp, esp

			mov ecx, [ebp + 0x08] /* pthis */
			push[ebp + 0x0C] /* ppoint */
			call pCMob__GetPos

			pop ebp
			ret 0x0008
	}
}
#endif

#define UNINITIALIZED_DISTANCE (float)1000000000.0

/* calculates the distance between the given point (in world coords) and the character */
static float maple_distance_from_character(long x, long y) {
	CUserLocal *u;
	long vx, vy; /* distance vector */

	u = CUserLocal__GetInstance();
	if (!u) {
		return UNINITIALIZED_DISTANCE + 1;
	}
	if (!u->m_pvc) {
		return UNINITIALIZED_DISTANCE + 1;
	}
	vx = x - (long)u->m_pvc->x;
	vy = y - (long)u->m_pvc->y;
	return sqrtf((float)(vx * vx + vy * vy)); /* distance = magnitude / length of the vec */
}

#ifdef KAMI_GETPOS
static long kami_closestmobx = 0, kami_closestmoby = 0;
static float kami_closestdistance = UNINITIALIZED_DISTANCE;
#endif

static int kami_enabled = 0;
static long kami_rangex = 100, kami_rangey = 0;
static long kami_lastx = 0, kami_lasty = 0;
static keypresstype kami_attacktype = KEYPRESS_FASTHOLD;
static int kami_looting = 0;
static int kami_lootstarttime = 0;
static int kami_lootstartautoatt = 0;
static long kami_lootstartx = 0, kami_lootstarty = 0;
static int32_t kami_lootitems = 20;

static void kami_teleport(long x, long y);

static void stoploot(void *param) {
	(void)param;

	if (kami_looting) {
		kami_teleport(kami_lootstartx, kami_lootstarty);
	}
	kami_looting = 0;
}

/* forces kami loot to stop */
void maple_kami_stoploot() {
	cq_callback cb = { stoploot };
	cq_call(cb, NULL, cq_no_callback);
}

/* sets the number of items after which Kami Loot will loot */
void maple_itemhook_setitems(int32_t count) {
	assert(count >= 0);
	kami_lootitems = count;
}

static int kami_nearestmobpos(__out PPOINT ppos) {
	CMobPool *pool;
	MobNode *mn;
	MobData1 *m1;
	MobData2 *m2;
	MobData3 *m3 = NULL;
#ifdef KAMI_CLOSEST
	float closest = UNINITIALIZED_DISTANCE;
	float dist;
#endif

	pool = CMobPool__GetInstance();
	if (!pool) {
#if _DEBUG
		puts("null mob pool");
#endif
		return 0;
	}

	mn = CMobPool_first(pool);
	if (!mn) {
#if _DEBUG
		puts("null mob node");
#endif
		return 0;
	}

	while (mn) {
		do {
			m1 = mn->pdata1;
			if (!m1) {
#if _DEBUG
				puts("null mobdata1");
#endif
				break;
			}

			m2 = m1->pdata2;
			if (!m2) {
#if _DEBUG
				puts("null mobdata2");
#endif
				break;
			}

			m3 = m2->pdata3;
			if (!m3) {
#if _DEBUG
				puts("null mobdata3");
#endif
				break;
			}

			/* mob is visible so use it */
			if (m3->ipos.x || m3->ipos.y) {
#ifdef KAMI_CLOSEST
				dist = maple_distance_from_character(m3->pos.x, m3->pos.y);
				if (dist < closest) {
					*ppos = m3->pos;
					closest = dist;
				}
#else
#if _DEBUG
				printf("%d, %d | %d, %d\n",
					m3->pos.x, m3->pos.y, m3->ipos.x, m3->ipos.y);
#endif
				*ppos = m3->pos;
				return 1;
#endif
			}
		} while (0);

		mn = mn->next;
	}

#ifdef KAMI_CLOSEST
	if (closest < UNINITIALIZED_DISTANCE - 1) {
#if _DEBUG
		if (m3) {
			printf("%d, %d | %d, %d\n",
				m3->pos.x, m3->pos.y, m3->ipos.x, m3->ipos.y);
		}
		else {
			printf("%d, %d\n", ppos->x, ppos->y);
		}
#endif
		return 1;
	}
#endif

#if _DEBUG
	puts("no valid mobs found");
#endif
	return 0; /* no mobs / no visible mobs */
}

/* this is called each frame and handles kami logic. */
static void kami_onupdate() {
#ifndef KAMI_GETPOS
	POINT pos = { 0 };
	CWvsPhysicalSpace2D *map;
	CDropPool *drops;
	RECT bds = { -1000000, -1000000, 1000000, 1000000 };
	CUserLocal *u;
#endif

	if (!kami_enabled && !itemhook.enabled) {
		return;
	}

#ifdef KAMI_GETPOS
#if _DEBUG
	printf("%d, %d (%f)\n", kami_closestmobx, kami_closestmoby, kami_closestdistance);
#endif
	kami_teleport(kami_closestmobx, kami_closestmoby);
	kami_closestdistance = UNINITIALIZED_DISTANCE;
	kami_closestmobx = 0;
	kami_closestmoby = 0;
#else
	map = CWvsPhysicalSpace2D__GetInstance();
	if (!map) {
#if _DEBUG
		puts("null map");
#endif
	}
	else {
		bds = map->bounds;
	}

	if (map && maple_isautoccing(0)) {
		kami_teleport(bds.left, bds.top);
		return;
	}

	drops = CDropPool__GetInstance();
	if (!drops) {
#if _DEBUG
		puts("null droppool");
#endif
	}

	if (drops && itemhook.enabled) {
		/* loot is ticked, so we need to check for loot conditions */
		if (drops->size > kami_lootitems && !kami_looting) { /* TODO: make this more customizable */
			kami_looting = 1;
			kami_lootstarttime = get_update_time();
			kami_lootstartautoatt = maple_macroenabled(MAPLEKEY_CTRL);

			/* time to loot! save current coords so we can teleport back once we're done */
			u = CUserLocal__GetInstance();
			if (!u) {
#if _DEBUG
				puts("null userlocal");
#endif
				kami_lootstartx = 0;
				kami_lootstarty = 0;
			}
			else if (u->m_pvc) {
					kami_lootstartx = (long)u->m_pvc->x;
					kami_lootstarty = (long)u->m_pvc->y;
			}
			else {
#if _DEBUG
				puts("null m_pvc");
#endif
				kami_lootstartx = 0;
				kami_lootstarty = 0;
			}
		}
		else if (kami_looting && (get_update_time() - kami_lootstarttime > 5000 || !drops->size)) {
			/* kami is done looting or it's been stuck for more than 5 secs looting */
			kami_looting = 0;

			/* teleport back to initial position */
			kami_teleport(kami_lootstartx, kami_lootstarty);
			maple_setmacro(0, MAPLEKEY_Z, 0, 0, KEYPRESS_PRESSED);
			maple_setmacro(VK_CONTROL, MAPLEKEY_CTRL, kami_lootstartautoatt, 0, kami_attacktype);
		}
	}

	if (kami_enabled && !kami_looting && kami_nearestmobpos(&pos)) {
		kami_teleport(
			min(bds.right,	max(bds.left, pos.x - kami_rangex)), 
			min(bds.bottom, max(bds.top,  pos.y - kami_rangey))
		);
		maple_setmacro(0, MAPLEKEY_Z, 0, 0, KEYPRESS_PRESSED);
		maple_setmacro(VK_CONTROL, MAPLEKEY_CTRL, 1, 0, kami_attacktype);
		return;
	}
	
	drops = CDropPool__GetInstance();
	if (!drops) {
#if _DEBUG
		puts("null droppool");
#endif
	}
	else if (itemhook.enabled && kami_looting) { 
		kami_teleport(itempos.x, itempos.y);
		maple_setmacro(0, MAPLEKEY_Z, 1, 0, KEYPRESS_PRESSED);
		maple_setmacro(VK_CONTROL, MAPLEKEY_CTRL, 0, 0, kami_attacktype);
		return;
	}

	if (map && kami_enabled) {
		kami_teleport(bds.left, bds.top);
		return;
	}

	if (kami_enabled) {
		kami_teleport(kami_lastx, kami_lasty);
	}
#endif
}

#ifdef KAMI_GETPOS
/* CMob::GetPos hook */
int __fastcall CMob__GetPos_hook(void *pthis, void *edx, int ppoint) {
	/* gets called each frame for each mob (67 times / second per mob) */
	struct tagPOINT *pos = (struct tagPOINT *)ppoint;
	float dist = maple_distance_from_character(pos->x, pos->y);
	if (kami_closestdistance > dist) {
		kami_closestdistance = dist;
		kami_closestmobx = pos->x;
		kami_closestmoby = pos->y;
	}
	return ((pfninvoke_CMob__GetPos)invoke_CMob__GetPos)(pthis, ppoint);
}
#endif

static void kami_toggle(void *param) {
	kami_enabled = (int)param;
#ifdef KAMI_GETPOS
	memory_detour(kami_enabled, (void **)&pCMob__GetPos, CMob__GetPos_hook);
#endif
}

/* toggles kami on and off */
int maple_kami(int enabled, long rangex, long rangey, keypresstype kptype) {
	cq_callback cb = { kami_toggle };

	if (enabled) {
		kami_rangex = rangex;
		kami_rangey = rangey;
		kami_attacktype = kptype;
	}
	else {
		kami_lootstartautoatt = 0; /* fixes auto attack not getting disabled if unticked while looting */
		maple_setmacro(0, MAPLEKEY_Z, 0, 0, KEYPRESS_PRESSED);
		maple_setmacro(VK_CONTROL, MAPLEKEY_CTRL, 0, 0, kami_attacktype);
	}

	return cq_call(cb, (void *)enabled, cq_no_callback);
}

static int mousefly_enabled = 0;

static void mousefly_onupdate() {
	CInputSystem *in;
	MouseLocation *mpos;

	if (!mousefly_enabled) {
		return;
	}

	in = CInputSystem__GetInstance();
	if (!in) {
		puts("null inputsystem");
		return;
	}

	mpos = in->loc;
	if (!mpos) {
		puts("null mpos");
		return;
	}

	kami_teleport(mpos->x, mpos->y);
} 

void mousefly_toggle(void *param) {
	mousefly_enabled = (int)param;
}

/* toggles mouse fly on and off */
int maple_mousefly(int enabled) {
	cq_callback cb = { mousefly_toggle };
	return cq_call(cb, (void *)enabled, cq_no_callback);
}

/* void CVecCtrlUser::OnTeleport(long, long). Not sure if it's the real name of the function, it's most likely just a XY setter */
typedef void(__fastcall *pfnCVecCtrlUser__OnTeleport)(void *pthis, void *edx, void *, long x, long y);

/* 
 * I dunno the real name of this func, but it's most likely just a XY setter.
 * 1st result: 8B ? 24 ? 8B ? ? 8B ? ? ? 8D ? ? 8B ? ? ? ? ? ? FF ? 85 C0 ? ? ? ? ? ? ? ? ? E8 
 */
static pfnCVecCtrlUser__OnTeleport CVecCtrlUser__OnTeleport = addrT(pfnCVecCtrlUser__OnTeleport, 01604660); /* v161.1 */

/* teleports to the given position */
static void kami_teleport(long x, long y) {
	CUserLocal *puser = NULL;

	puser = CUserLocal__GetInstance();
	if (!puser) {
#if _DEBUG
		puts("maple_teleport: invalid charbase");
#endif
		return;
	}

	if (!puser->m_pvc) {
#if _DEBUG
		puts("maple_teleport: invalid m_pvc");
#endif
		return;
	}

	kami_lastx = x;
	kami_lasty = y;
	CVecCtrlUser__OnTeleport((uint8_t *)puser->m_pvc + 4, NULL, NULL, x, y);
}

static void scheduled_teleport(__in void *param) {
	POINT *p = (POINT *)param;
	kami_teleport(p->x, p->y);
}

static void scheduled_teleport_cleanup(__inout void *param) {
	POINT *p = (POINT *)param;
	free(p);
}

/* enqueues a teleport to the given position */
int maple_teleport(long x, long y) {
	cq_callback cb, cleanup;

	POINT *p = (POINT *)malloc(sizeof(POINT));
	p->x = x;
	p->y = y;

	cb.func_ptr = scheduled_teleport;
	cleanup.func_ptr = scheduled_teleport_cleanup;
	return cq_call(cb, p, cleanup);
}

/*
 * - [Login stuff] -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 */
#define MAX_EMAIL 254
static char autologin_username[MAX_EMAIL] = { 0 };
static char autologin_password[MAX_EMAIL] = { 0 };
int32_t autologin_channel = 19;
int32_t autologin_world = 0;
static char autologin_pic[16] = { 0 };
int32_t autologin_character = 0;

void maple_autologinsettings(const char *user, const char *pass, int32_t ch, int32_t w, const char *pic, int32_t chara) {
	strcpy_s(autologin_username, MAX_EMAIL, user);
	strcpy_s(autologin_password, MAX_EMAIL, pass);
	autologin_channel = ch;
	autologin_world = w;
	strcpy_s(autologin_pic, 16, pic);
	autologin_character = chara;
}

/*
	TSecType<long>::SetData (login hook)
	2nd result ? 8B ? 8B 0D ? ? ? ? ? ? ? ? ? ? F7 ? ? ? C1 ? ? ? ? C1 ? ? ? ? 6B
*/
typedef void(__fastcall *pfnTSecType_long___SetData)(void *pthis, __reserved void *edx, long data);
pfnTSecType_long___SetData pTSecType_long___SetData = addrT(pfnTSecType_long___SetData, 004B4160); /* v161.1 */
void __fastcall TSecType_long___SetData_hook(void *pthis, void *edx, long data);

#if 0
/*
	CLogin::OnRecommendWorldMessage (world select hook)
	scroll to the beginning 83 BD ? ? ? ? 01 0F 85 ? ? ? ? 8B
*/
typedef void(__fastcall *pfnCLogin__OnRecommendWorldMessage)(void *pthis, __reserved void *edx, void *ppacket);
pfnCLogin__OnRecommendWorldMessage pCLogin__OnRecommendWorldMessage = addrT(pfnCLogin__OnRecommendWorldMessage, 009DBE10); /* v161.1 */
void __fastcall CLogin__OnRecommendWorldMessage_hook(void *pthis, void *edx, void *ppacket);
#endif

/*
	world select hook
	? ? ? E8 ? ? ? ? F6 ? ? ? 01 74 ? ? B9 ? ? ? ? E8 ? ? ? ? ? ? ? C2 04 00 6A ? 68 ? ? ? ? 64 ? 00 00 00 00 ? 83 EC 28 ? ? ? ? ? ? ? ? ? ? 8D ? ? ? 64 
*/
typedef void(__fastcall *pfnWorldSelectFunc)(void *pthis, __reserved void *edx, void *unk);
pfnWorldSelectFunc pWorldSelectFunc = addrT(pfnWorldSelectFunc, 00A29C10); /* v161.1 */
void __fastcall WorldSelectFunc_hook(void *pthis, void *edx, void *unk);

/*
	CUIAvatar::SelectCharacter (char select / pic hook)
	call below 8B 87 A0 00 00 00 8B 80 ? ? ? ? 50 8B ? E8
*/
typedef void(__fastcall *pfnCUIAvatar__SelectCharacter)(void *pthis, __reserved void *edx, int32_t character);
pfnCUIAvatar__SelectCharacter pCUIAvatar__SelectCharacter = addrT(pfnCUIAvatar__SelectCharacter, 009F1660); /* v161.1 */
void __fastcall CUIAvatar__SelectCharacter_hook(void *pthis, void *edx, int32_t character);

static detour loginhook =		{ (void **)&pTSecType_long___SetData,			(void *)TSecType_long___SetData_hook };
static detour worldhook =		{ (void **)&pWorldSelectFunc,					(void *)WorldSelectFunc_hook };
static detour charselecthook =	{ (void **)&pCUIAvatar__SelectCharacter,		(void *)CUIAvatar__SelectCharacter_hook };
static generic_hack hautologin[] = {
	{ HACK_DETOUR, &loginhook }, 
	{ HACK_DETOUR, &worldhook }, 
	{ HACK_DETOUR, &charselecthook }, 
};
hack maple_autologin = { 3, hautologin };

/*
	TSecType<long>::GetData
	3rd result 83 EC 08 ? ? ? ? ? ? 08 ? ? ? ? ? 08 ? ? 04 ? ? ? 75
*/
typedef long (__fastcall *pfnTSecType_long___GetData)(void *pthis, __reserved void *edx);
pfnTSecType_long___GetData TSecType_long___GetData = addrT(pfnTSecType_long___GetData, 004B42C0); /* v161.1 */

/*
	ZXString<char>::ZXString<char>(char) costructor
	8B 44 24 08 56 8B F1 8B 4C 24 08 50 51 8B CE C7 (3rd result)
*/
typedef ZXString_char_ *(__fastcall *pfnZXString_char___ZXString_char__char_)(ZXString_char_ *pthis, __reserved void *edx, char *str, int len);
pfnZXString_char___ZXString_char__char_ ZXString_char___ZXString_char__char_ = addrT(pfnZXString_char___ZXString_char__char_, 004A2820); /* v161.1 */

/*
	ZXString<char>::~ZXString<char>() destructor
	56 8B F1 8B 06 85 C0 74 (4th result)
*/
typedef void(__fastcall *pfnZXString_char____ZXString_char_)(ZXString_char_ *pthis, __reserved void *edx);
pfnZXString_char____ZXString_char_ ZXString_char____ZXString_char_ = addrT(pfnZXString_char____ZXString_char_, 00481DB0); /* v161.1 */

/*
	ZXString<char>::operator=(ZXString<char> *other)
	8B 44 24 04 56 8B F1 3B F0 74 (2nd result)
*/
typedef ZXString_char_ *(__fastcall *pfnZXString_char___operator_)(ZXString_char_ *pthis, __reserved void *edx, ZXString_char_ *pother);
pfnZXString_char___operator_ ZXString_char___operator_ = addrT(pfnZXString_char___operator_, 00483FF0); /* v161.1 */

/*
	CUITitle::SetRet
	6A ? 68 ? ? ? ? ? ? 00 00 00 00 ? 83 EC 10 ? ? ? ? ? ? ? ? ? ? ? ? 8D ? ? ? ? ? 00 00 00 00 ? ? E8 ? ? ? ? 85 C0 0F 84 ? ? ? ? 8B
*/
typedef void(__fastcall *pfnCUITitle__SetRet)(CUITitle *pthis, __reserved void *edx, uint32_t dw);
pfnCUITitle__SetRet CUITitle__SetRet = addrT(pfnCUITitle__SetRet, 00A2BA40);

/*
	CLogin::SendLoginPacket
	scroll to the beginning 8B F1 89 74 24 ? E9
*/
typedef void(__fastcall *pfnCLogin__SendLoginPacket)(CLogin *pthis, __reserved void *edx, int world, int channel);
pfnCLogin__SendLoginPacket CLogin__SendLoginPacket = addrT(pfnCLogin__SendLoginPacket, 009E4500);

/* login hook */
void __fastcall TSecType_long___SetData_hook(void *pthis, void *edx, long data) {
	int islogin = 0;
	ZXString_char_ user = { 0 };
	ZXString_char_ pass = { 0 };
	CSecurityClient *pclient = CSecurityClient__GetInstance();
	CUITitle *ptitle = NULL;

	do {
		if (!pclient) {
#if _DEBUG
			puts("TSecType<long>::SetData: null CSecurityClient");
#endif
			break;
		}

		if (&pclient->t != pthis) {
			break;
		}

#if _DEBUG
		puts("TSecType<long>::SetData was called for CSecurityClient");
#endif

		if (data != 1) {
			break;
		}

		if (TSecType_long___GetData(&pclient->t, 0) == 1) {
			break;
		}

#if _DEBUG
		puts("Logging in...");
#endif
		islogin = 1;
	} while (0);

	pTSecType_long___SetData(pthis, NULL, data);

	if (islogin) {
		ZXString_char___ZXString_char__char_(&user, NULL, autologin_username, -1);
		ZXString_char___ZXString_char__char_(&pass, NULL, autologin_password, -1);
		
		ptitle = CUITitle__GetInstance();
		if (ptitle) {
			ZXString_char___operator_(&ptitle->edit_user->text, 0, &user);
			ZXString_char___operator_(&ptitle->edit_password->text, 0, &pass);
			CUITitle__SetRet(ptitle, 0, 1);
		}
#if _DEBUG
		else {
			puts("TSecType<long>::SetData: null CUITitle");
		}
#endif

		ZXString_char____ZXString_char_(&user, NULL);
		ZXString_char____ZXString_char_(&pass, NULL);
	}
}

#if 0
/* world select hook */
void __fastcall CLogin__OnRecommendWorldMessage_hook(void *pthis, void *edx, void *ppacket) {
	CLogin *plogin = CLogin__GetInstance();
	pCLogin__OnRecommendWorldMessage(pthis, NULL, ppacket);

	if (!plogin) {
#if _DEBUG
		puts("CLogin::OnRecommendWorldMessage: null CLogin");
#endif
		return;
	}

	CLogin__SendLoginPacket(plogin, NULL, autologin_world, autologin_channel);
}
#endif

/* world select hook */
void __fastcall WorldSelectFunc_hook(void *pthis, void *edx, void *unk) {
	CLogin *plogin = CLogin__GetInstance();
	pWorldSelectFunc(pthis, NULL, unk);

	if (!plogin) {
#if _DEBUG
		puts("CLogin::OnRecommendWorldMessage: null CLogin");
#endif
		return;
	}

	CLogin__SendLoginPacket(plogin, NULL, autologin_world, autologin_channel);
}

/* char select hook */
void __fastcall CUIAvatar__SelectCharacter_hook(void *pthis, void *edx, int32_t character) {
	CLogin *plogin = CLogin__GetInstance();
	pCUIAvatar__SelectCharacter(pthis, NULL, character);

	if (!plogin) {
#if _DEBUG
		puts("CUIAvatar::SelectCharacter: null CLogin");
#endif
		return;
	}

	plogin->index = autologin_character;
	ZXString_char___ZXString_char__char_(&plogin->pic, NULL, autologin_pic, -1);
}