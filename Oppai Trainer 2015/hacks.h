/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of Oppai Trainer 2015.
	Oppai Trainer 2015 is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	Oppai Trainer 2015 is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with Oppai Trainer 2015. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __HACKS_H__
#define __HACKS_H__

#include <windows.h>
void hacks_savesettings();
void hacks_loadsettings();
void hacks_show();
void hacks_create(HWND parent);
void hacks_destroy();
int hacks_ismouseflyenabled();

void hacks_onkamihotkey();
void hacks_onkamiloothotkey();

#endif
