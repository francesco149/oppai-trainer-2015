/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of Oppai Trainer 2015.
	Oppai Trainer 2015 is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	Oppai Trainer 2015 is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with Oppai Trainer 2015. If not, see <http://www.gnu.org/licenses/>.
*/

#include "hacks.h"

#include <stdio.h>
#include <hackdesu/ini.h>
#include "globals.h"
#include "resource.h"
#include "win32_gui.h"
#include "maple_hacks.h"
#include "maple_macros.h"
#include "botting.h"

#define HACKS_CLASS "OppaiTrainer2015Hacks"
#define HACKS_NAME "Hacks"

#define IDC_GROUP_HACKS 101
#define IDC_CHECK_NOKB 102
#define IDC_CHECK_FULLGODMODE 103
#define IDC_CHECK_FULLMOBDISARM 104
#define IDC_CHECK_LEMMINGS 105
#define IDC_CHECK_SKIPLOGO 106
#define IDC_CHECK_BLOCKHWID 107
#define IDC_CHECK_CPUHACK 108
#define IDC_CHECK_MAGICGODMODE 109
#define IDC_CHECK_INSTADROP 110
#define IDC_CHECK_JDA 111
#define IDC_CHECK_VIEWSWEARS 112
#define IDC_CHECK_REACTORDEM 113
#define IDC_CHECK_NOFA 114
#define IDC_CHECK_UA 115
#define IDC_CHECK_TUBI 116
#define IDC_CHECK_TIMEDGODMODE 117
#define IDC_CHECK_STABLETUBI 118
#define IDC_CHECK_SUCKLEFT 119

#define IDC_GROUP_KAMI 201
#define IDC_CHECK_KAMI 202
#define IDC_CHECK_KAMILOOT 203
#define IDC_LABEL_KAMIRANGEX 204
#define IDC_LABEL_KAMIRANGEY 205
#define IDC_EDIT_KAMIRANGEX 206
#define IDC_EDIT_KAMIRANGEY 207
#define IDC_LABEL_KAMIITEMS 208
#define IDC_EDIT_KAMIITEMS 209
#define IDC_CHECK_AIRCHECKS 210
#define IDC_CHECK_MOUSE 211
#define IDC_EDIT_TELEX 212
#define IDC_EDIT_TELEY 213
#define IDC_BUTTON_TELEPORT 214

#define GROUP_HACKS_MARGINW 10
#define GROUP_HACKS_MARGINH 10
#define GROUP_HACKS_Y GROUP_HACKS_MARGINH
#define GROUP_HACKS_H 210
#define GROUP_HACKS_W 215
#define CHECK_NOKB_X (GROUP_HACKS_MARGINW + 14)
#define CHECK_NOKB_Y (GROUP_HACKS_Y + 14 + 4)

#define GROUP_KAMI_W 154
#define GROUP_KAMI_H GROUP_HACKS_H
#define GROUP_KAMI_X (GROUP_HACKS_W + GROUP_HACKS_MARGINH * 2)
#define GROUP_KAMI_Y GROUP_HACKS_Y
#define CHECK_KAMI_X (GROUP_KAMI_X + 14)
#define CHECK_KAMI_Y CHECK_NOKB_Y

#define HACKS_W (GROUP_HACKS_W + GROUP_KAMI_W + GROUP_HACKS_MARGINW * 3 + 6)
#define HACKS_H (GROUP_HACKS_H + GROUP_HACKS_MARGINH * 2 + 28)

extern HINSTANCE oppai_instance;
static HWND hacks_wnd = NULL;

static void hacks_handlecheckbox(HWND wnd, int cb);

/* <settings> */
static const char *sect = "hacks";

static const ini_control checkboxes[] = {
	{ "nokb", IDC_CHECK_NOKB },
	{ "fullgodmode", IDC_CHECK_FULLGODMODE },
	{ "fullmobdisarm", IDC_CHECK_FULLMOBDISARM },
	{ "lemmings", IDC_CHECK_LEMMINGS },
	{ "skiplogo", IDC_CHECK_SKIPLOGO },
	{ "blockhwid", IDC_CHECK_BLOCKHWID },
	{ "cpuhack", IDC_CHECK_CPUHACK },
	{ "magicgodmode", IDC_CHECK_MAGICGODMODE },
	{ "instadrop", IDC_CHECK_INSTADROP },
	{ "jda", IDC_CHECK_JDA },
	{ "viewswears", IDC_CHECK_VIEWSWEARS },
	{ "reactordem", IDC_CHECK_REACTORDEM },
	{ "nofa", IDC_CHECK_NOFA },
	{ "ua", IDC_CHECK_UA },
	{ "tubi", IDC_CHECK_TUBI },
	{ "timedgodmode", IDC_CHECK_TIMEDGODMODE },
	{ "stabletubi", IDC_CHECK_STABLETUBI },
	{ "suckleft", IDC_CHECK_SUCKLEFT },
	{ "kami", IDC_CHECK_KAMI },
	{ "kamiloot", IDC_CHECK_KAMILOOT },
	{ "airchecks", IDC_CHECK_AIRCHECKS },
	{ "mouse", IDC_CHECK_MOUSE },
	{ NULL, 0 },
};

static const ini_control edits[] = {
	{ "kamirangex", IDC_EDIT_KAMIRANGEX },
	{ "kamirangey", IDC_EDIT_KAMIRANGEY },
	{ "kamiitems", IDC_EDIT_KAMIITEMS },
	{ "telex", IDC_EDIT_TELEX },
	{ "teley", IDC_EDIT_TELEY },
	{ NULL, 0 },
};

void hacks_savesettings() {
	RECT r;

#if _DEBUG
	puts("Saving hacks settings...");
#endif

	if (!GetWindowRect(hacks_wnd, &r)) {
		win32_show_error("GetWindowRect failed for hacks_wnd");
	}
	else if (r.left < 32767 && r.left > -32000 && r.top < 32767 && r.top > -32000) { /* should prevent invalid positions from saving */
		ini_write_int(sect, "x", r.left, OPPAI_SETTINGS);
		ini_write_int(sect, "y", r.top, OPPAI_SETTINGS);
	}
	ini_write_checks(sect, hacks_wnd, checkboxes, OPPAI_SETTINGS);
	ini_write_edits(sect, hacks_wnd, edits, OPPAI_SETTINGS);
#if _DEBUG
	puts("Hacks saved!");
#endif
}

void hacks_loadsettings() {
	POINT pos;
	const ini_control *it;

#if _DEBUG
	puts("Loading hacks settings...");
#endif
	pos.x = ini_get_int(sect, "x", CW_USEDEFAULT, OPPAI_SETTINGS);
	pos.y = ini_get_int(sect, "y", CW_USEDEFAULT, OPPAI_SETTINGS);

	if (pos.x != CW_USEDEFAULT && pos.y != CW_USEDEFAULT) {
		SetWindowPos(hacks_wnd, NULL, pos.x, pos.y, 0, 0,
			SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOZORDER);
	}

	ini_get_checks(sect, hacks_wnd, checkboxes, OPPAI_SETTINGS);
	ini_get_edits(sect, hacks_wnd, edits, OPPAI_SETTINGS);

	/* force all checkboxes to be processed so that enabled hacks in the settings will be enabled */
	for (it = checkboxes; it->key; it++) {
		hacks_handlecheckbox(hacks_wnd, it->ctlid);
	}
#if _DEBUG
	puts("Loaded hacks!");
#endif
}
/* </settings> */

static LRESULT hacks_initwnd(HWND wnd) {
	HWND group, check, edit, button, label;
	int w;
	int d;
	char buftitle[128] = { 0 };

	sprintf_s(buftitle, 128, "Hacks | PID: %d", GetCurrentProcessId());
	SetWindowTextA(wnd, buftitle);

	/* hacks groupbox */
	group = win32_make_groupbox(
		wnd, IDC_GROUP_HACKS, "group_hacks", "Hacks",
		GROUP_HACKS_MARGINW, GROUP_HACKS_Y,
		GROUP_HACKS_W, GROUP_HACKS_H);
	if (!group) {
		return -1;
	}

	w = GROUP_HACKS_W / 2 - CHECK_NOKB_X;
	d = 0;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_NOKB, "check_nokb", "NoKB",
		CHECK_NOKB_X, CHECK_NOKB_Y, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_FULLGODMODE, "check_fullgodmode", "Full Godmode",
		CHECK_NOKB_X, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_FULLMOBDISARM, "check_fullmobdisarm", "Mob Disarm",
		CHECK_NOKB_X, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_LEMMINGS, "check_lemmings", "Lemmings",
		CHECK_NOKB_X, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_SKIPLOGO, "check_skiplogo", "Skip Logo",
		CHECK_NOKB_X, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_BLOCKHWID, "check_blockhwid", "Rand HWID",
		CHECK_NOKB_X, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_CPUHACK, "check_cpuhack", "CPU Hack",
		CHECK_NOKB_X, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_TUBI, "check_tubi", "Tubi",
		CHECK_NOKB_X, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_STABLETUBI, "check_stabletubi", "Stable Tubi",
		CHECK_NOKB_X, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d = 0;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_MAGICGODMODE, "check_magicgodmode", "Magic Godm.",
		CHECK_NOKB_X * 2 + w, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_INSTADROP, "check_instadrop", "Insta Drop",
		CHECK_NOKB_X * 2 + w, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_JDA, "check_jda", "JDA",
		CHECK_NOKB_X * 2 + w, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_VIEWSWEARS, "check_viewswears", "View Swears",
		CHECK_NOKB_X * 2 + w, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_REACTORDEM, "check_reactordem", "Reactor DEM",
		CHECK_NOKB_X * 2 + w, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_NOFA, "check_nofa", "No FA",
		CHECK_NOKB_X * 2 + w, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_UA, "check_ua", "UA",
		CHECK_NOKB_X * 2 + w, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_TIMEDGODMODE, "check_timedgodmode", "58s Godmode",
		CHECK_NOKB_X * 2 + w, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	d += 20;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_SUCKLEFT, "check_suckleft", "Suck Left",
		CHECK_NOKB_X * 2 + w, CHECK_NOKB_Y + d, w, 20);
	if (!check) {
		return -1;
	}

	/* kami groupbox */
	group = win32_make_groupbox(
		wnd, IDC_GROUP_KAMI, "group_kami", "Kami",
		GROUP_KAMI_X, GROUP_KAMI_Y,
		GROUP_KAMI_W, GROUP_KAMI_H);
	if (!group) {
		return -1;
	}

	w = GROUP_KAMI_W / 2 - CHECK_NOKB_X;
	check = win32_make_checkbox(
		wnd, IDC_CHECK_KAMI, "check_kami", "Enable",
		CHECK_KAMI_X, CHECK_KAMI_Y, w, 20);
	if (!check) {
		return -1;
	}

	check = win32_make_checkbox(
		wnd, IDC_CHECK_KAMILOOT, "check_kamiloot", "Loot",
		CHECK_KAMI_X + w + CHECK_NOKB_X, CHECK_KAMI_Y, w, 20);
	if (!check) {
		return -1;
	}

	label = win32_make_static(
		wnd, IDC_LABEL_KAMIRANGEX, "static_kamirx", "RX: ",
		CHECK_KAMI_X, CHECK_KAMI_Y + 33,
		25, 20);
	if (!label) {
		return -1;
	}

	label = win32_make_static(
		wnd, IDC_LABEL_KAMIRANGEY, "static_kamiry", "RY: ",
		CHECK_KAMI_X, CHECK_KAMI_Y + 58,
		25, 20);
	if (!label) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_KAMIRANGEX, "edit_kamirx", "100",
		CHECK_KAMI_X + 25, CHECK_KAMI_Y + 30,
		GROUP_KAMI_W - 25 - 28, 20, WS_EDIT_DEFAULT);
	if (!edit) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_KAMIRANGEY, "edit_kamiry", "0",
		CHECK_KAMI_X + 25, CHECK_KAMI_Y + 55,
		GROUP_KAMI_W - 25 - 28, 20, WS_EDIT_DEFAULT);
	if (!edit) {
		return -1;
	}

	label = win32_make_static(
		wnd, IDC_LABEL_KAMIITEMS, "static_kamiitems", "Loot if items > ",
		CHECK_KAMI_X, CHECK_KAMI_Y + 88,
		100, 20);
	if (!label) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_KAMIITEMS, "edit_kamiitems", "20",
		CHECK_KAMI_X + 100, CHECK_KAMI_Y + 85,
		GROUP_KAMI_W - 100 - 28, 20, WS_EDIT_DEFAULT | ES_NUMBER);
	if (!edit) {
		return -1;
	}

	check = win32_make_checkbox(
		wnd, IDC_CHECK_AIRCHECKS, "check_airchecks", "AirCheck",
		CHECK_KAMI_X, CHECK_KAMI_Y + 112, 
		w + 10, 20);
	if (!check) {
		return -1;
	}

	check = win32_make_checkbox(
		wnd, IDC_CHECK_MOUSE, "check_mouse", "Mouse",
		CHECK_KAMI_X + w + CHECK_NOKB_X, CHECK_KAMI_Y + 112,
		w, 20);
	if (!check) {
		return -1;
	}

	w = GROUP_KAMI_W / 2 - 14;
	edit = win32_make_edit_s(
		wnd, IDC_EDIT_TELEX, "edit_telex", "10",
		CHECK_KAMI_X, CHECK_KAMI_Y + 132,
		w - 2, 20, WS_EDIT_DEFAULT);
	if (!edit) {
		return -1;
	}

	edit = win32_make_edit_s(
		wnd, IDC_EDIT_TELEY, "edit_teley", "10",
		CHECK_KAMI_X + w + 3, CHECK_KAMI_Y + 132,
		w - 3, 20, WS_EDIT_DEFAULT);
	if (!edit) {
		return -1;
	}

	button = win32_make_button(
		wnd, IDC_BUTTON_TELEPORT, "button_teleport", "Teleport to coords",
		CHECK_KAMI_X - 1, CHECK_KAMI_Y + 156 - 1,
		GROUP_KAMI_W - 28 + 2, 22);
	if (!button) {
		return -1;
	}

	return 0;
}

/* triggered by the kami checkbox */
static void hacks_togglekami(HWND wnd, int enabled) {
	char buf[16] = { 0 };
	long rangex = 0, rangey = 0;
	keypresstype kptype = KEYPRESS_PRESSED;
	if (enabled) {
		if (!botting_getautoattacktype(&kptype)) {
			return;
		}

		if (!GetDlgItemTextA(wnd, IDC_EDIT_KAMIRANGEX, buf, 16)) {
			win32_show_error("GetDlgItemTextA failed for rangex");
			return;
		}
		if (!strlen(buf)) {
			MessageBoxA(wnd, "Empty X range", HACKS_NAME, MB_OK | MB_ICONERROR);
		}
		if (sscanf_s(buf, "%d", &rangex) != 1) {
			MessageBoxA(wnd, "Invalid X range", HACKS_NAME, MB_OK | MB_ICONERROR);
		}

		if (!GetDlgItemTextA(wnd, IDC_EDIT_KAMIRANGEY, buf, 16)) {
			win32_show_error("GetDlgItemTextA failed for rangey");
			return;
		}
		if (!strlen(buf)) {
			MessageBoxA(wnd, "Empty Y range", HACKS_NAME, MB_OK | MB_ICONERROR);
		}
		if (sscanf_s(buf, "%d", &rangey) != 1) {
			MessageBoxA(wnd, "Invalid Y range", HACKS_NAME, MB_OK | MB_ICONERROR);
		}
	}

	maple_kami(enabled, rangex, rangey, kptype);
}

/* triggered by the loot checkbox */
static void hacks_toggleloot(HWND wnd, int enabled) {
	char buf[16] = { 0 };
	int32_t items = 0;

	if (enabled) {
		if (!GetDlgItemTextA(wnd, IDC_EDIT_KAMIITEMS, buf, 16)) {
			win32_show_error("GetDlgItemTextA failed for kamiitems");
			return;
		}

		if (!strlen(buf)) {
			MessageBoxA(wnd, "Empty item count.", HACKS_NAME, MB_OK | MB_ICONERROR);
			return;
		}

		if (sscanf_s(buf, "%d", &items) != 1) {
			MessageBoxA(wnd, "Invalid item count.", HACKS_NAME, MB_OK | MB_ICONERROR);
			return;
		}

		maple_itemhook_setitems(items);
	}
	else {
		maple_setmacro(0, MAPLEKEY_Z, 0, 0, KEYPRESS_PRESSED);
	}

	maple_kami_stoploot();
	hack_toggle(&maple_itemhook, enabled);
}

/* trigged by the "Teleport to coords" button */
static void hacks_teleportclicked(HWND wnd) {
	char buf[512] = { 0 };
	int n;
	long x, y;

	/* x */
	n = GetDlgItemTextA(wnd, IDC_EDIT_TELEX, buf, 512);
	if (!n) {
		win32_show_error("GetDlgItemTextA failed");
		return;
	}
	if (!strlen(buf)) {
		MessageBoxA(wnd, "Empty X coord.", HACKS_NAME, MB_OK | MB_ICONERROR);
		return;
	}
	if (sscanf_s(buf, "%d", &x) != 1) {
		MessageBoxA(wnd, "Invalid X coord.", HACKS_NAME, MB_OK | MB_ICONERROR);
		return;
	}

	/* y */
	n = GetDlgItemTextA(wnd, IDC_EDIT_TELEY, buf, 512);
	if (!n) {
		win32_show_error("GetDlgItemTextA failed");
		return;
	}
	if (!strlen(buf)) {
		MessageBoxA(wnd, "Empty Y coord.", HACKS_NAME, MB_OK | MB_ICONERROR);
		return;
	}
	if (sscanf_s(buf, "%d", &y) != 1) {
		MessageBoxA(wnd, "Invalid Y coord.", HACKS_NAME, MB_OK | MB_ICONERROR);
		return;
	}

	if (!maple_teleport(x, y)) {
		MessageBoxA(wnd, "Something went wrong with the teleport function. "
			"Please report this bug.", HACKS_NAME, MB_OK | MB_ICONERROR);
	}
}

/* triggered when any checkbox is clicked */
static void hacks_handlecheckbox(HWND wnd, int cb) {
	HWND wndcheck;
	int checked;

	wndcheck = GetDlgItem(wnd, cb);
	if (!wndcheck) {
		win32_show_error("GetDlgItem failed for wndcheck");
		return;
	}
	checked = Button_GetCheck(wndcheck) == BST_CHECKED;

	switch (cb) {
	case IDC_CHECK_NOKB:
		hack_toggle(&maple_nokb, checked);
		break;

	case IDC_CHECK_FULLGODMODE:
		hack_toggle(&maple_fullgodmode, checked);
		break;

	case IDC_CHECK_FULLMOBDISARM:
		hack_toggle(&maple_fullmobdisarm, checked);
		break;

	case IDC_CHECK_LEMMINGS:
		hack_toggle(&maple_lemmings, checked);
		break;

	case IDC_CHECK_SKIPLOGO:
		hack_toggle(&maple_skiplogo, checked);
		break;

	case IDC_CHECK_BLOCKHWID:
		hack_toggle(&maple_randomizehwid, checked);
		break;

	case IDC_CHECK_CPUHACK:
		hack_toggle(&maple_cpuhack, checked);
		break;

	case IDC_CHECK_MAGICGODMODE:
		hack_toggle(&maple_magicgodmode, checked);
		break;

	case IDC_CHECK_INSTADROP:
		hack_toggle(&maple_instadrop, checked);
		break;

	case IDC_CHECK_JDA:
		hack_toggle(&maple_jda, checked);
		break;

	case IDC_CHECK_VIEWSWEARS:
		hack_toggle(&maple_viewswears, checked);
		break;

	case IDC_CHECK_REACTORDEM:
		hack_toggle(&maple_reactordem, checked);
		break;

	case IDC_CHECK_NOFA:
		hack_toggle(&maple_nofa, checked);
		break;

	case IDC_CHECK_UA:
		hack_toggle(&maple_ua, checked);
		break;

	case IDC_CHECK_TUBI:
		hack_toggle(&maple_tubi, checked);
		break;

	case IDC_CHECK_TIMEDGODMODE:
		hack_toggle(&maple_timedgodmode, checked);
		break;

	case IDC_CHECK_STABLETUBI:
		maple_slowtubi(checked);
		break;

	case IDC_CHECK_SUCKLEFT:
		hack_toggle(&maple_suckleft, checked);
		break;

	case IDC_CHECK_KAMI:
		hacks_togglekami(wnd, checked);
		break;

	case IDC_CHECK_KAMILOOT:
		hacks_toggleloot(wnd, checked);
		break;

	case IDC_CHECK_AIRCHECKS:
		hack_toggle(&maple_airchecks, checked);
		break;

	/*
	case IDC_CHECK_MOUSE:
		maple_mousefly(checked);
		break;
	*/

#if _DEBUG
	default:
		printf("Unhandled checkbox message for control id %d\n", cb);
#endif
	}
}

static LRESULT CALLBACK hacks_wndproc(HWND wnd, UINT msgid, WPARAM wparam, LPARAM lparam) {
	switch (msgid) {
	case WM_CREATE:
		return hacks_initwnd(wnd);

	case WM_CLOSE:
		ShowWindow(wnd, SW_HIDE);
		break;

	case WM_COMMAND:
		switch (LOWORD(wparam)) {
		case IDC_BUTTON_TELEPORT:
			hacks_teleportclicked(wnd);
			break;

		case IDC_CHECK_NOKB:
		case IDC_CHECK_FULLGODMODE:
		case IDC_CHECK_FULLMOBDISARM:
		case IDC_CHECK_LEMMINGS:
		case IDC_CHECK_SKIPLOGO:
		case IDC_CHECK_BLOCKHWID:
		case IDC_CHECK_CPUHACK:
		case IDC_CHECK_MAGICGODMODE:
		case IDC_CHECK_INSTADROP:
		case IDC_CHECK_JDA:
		case IDC_CHECK_VIEWSWEARS:
		case IDC_CHECK_REACTORDEM:
		case IDC_CHECK_NOFA:
		case IDC_CHECK_UA:
		case IDC_CHECK_TUBI:
		case IDC_CHECK_TIMEDGODMODE:
		case IDC_CHECK_STABLETUBI:
		case IDC_CHECK_SUCKLEFT:
		case IDC_CHECK_KAMI:
		case IDC_CHECK_KAMILOOT:
		case IDC_CHECK_AIRCHECKS:
		/*case IDC_CHECK_MOUSE:*/
			hacks_handlecheckbox(wnd, LOWORD(wparam));
			break;
		}
		break;

	default:
		return DefWindowProcA(wnd, msgid, wparam, lparam);
	}

	return 0;
}

void hacks_show() {
	if (hacks_wnd) {
		ShowWindow(hacks_wnd, SW_SHOW);
	}
}

void hacks_create(HWND parent) {
	hacks_wnd = win32_make_child_window_ex(
		parent, HACKS_CLASS, HACKS_NAME,
		oppai_instance, hacks_wndproc,
		LoadIconA(oppai_instance, MAKEINTRESOURCEA(IDI_ICON1)),
		HACKS_W, HACKS_H, SW_HIDE, 0,
		WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME ^ WS_MAXIMIZEBOX
	);
}

void hacks_destroy() {
	if (hacks_wnd) {
		DestroyWindow(hacks_wnd);
	}
	UnregisterClassA(HACKS_CLASS, oppai_instance);
}

static int hacks_ischecked(int cb) {
	HWND wndcheck;

	if (!hacks_wnd) {
		return 0;
	}

	wndcheck = GetDlgItem(hacks_wnd, cb);
	if (!wndcheck) {
		win32_show_error("GetDlgItem failed for wndcheck");
		return 0;
	}

	return Button_GetCheck(wndcheck) == BST_CHECKED;
}

int hacks_ismouseflyenabled() {
	return hacks_ischecked(IDC_CHECK_MOUSE);
}

static void hacks_check(int checked, int cb) {
	HWND wndcheck;

	if (!hacks_wnd) {
		return;
	}

	wndcheck = GetDlgItem(hacks_wnd, cb);
	if (!wndcheck) {
		win32_show_error("GetDlgItem failed for check_kami");
		return;
	}

	Button_SetCheck(wndcheck, checked ? BST_CHECKED : BST_UNCHECKED);
}

void hacks_checkkami(int checked) {
	hacks_check(checked, IDC_CHECK_KAMI);
}

void hacks_checkkamiloot(int checked) {
	hacks_check(checked, IDC_CHECK_KAMILOOT);
}

void hacks_onkamihotkey() {
	hacks_check(!hacks_ischecked(IDC_CHECK_KAMI), IDC_CHECK_KAMI);
	hacks_handlecheckbox(hacks_wnd, IDC_CHECK_KAMI);
}

void hacks_onkamiloothotkey() {
	hacks_check(!hacks_ischecked(IDC_CHECK_KAMILOOT), IDC_CHECK_KAMILOOT);
	hacks_handlecheckbox(hacks_wnd, IDC_CHECK_KAMILOOT);
}
