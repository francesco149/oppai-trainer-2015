/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of Oppai Trainer 2015.
	Oppai Trainer 2015 is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	Oppai Trainer 2015 is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with Oppai Trainer 2015. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAPLE_STRUCT_H
#define MAPLE_STRUCT_H

#include <stdint.h>
#include <Windows.h>
#include <hackdesu/padding_macros.h>
#include <hackdesu/hack.h>

/*
 * TSingleton<CUserLocal>::ms_pInstance (char base).
 * ptr mov before last call in CAvatarMegaphone::OnMouseButton
 * 83 EC ?? 81 7C 24 ?? ?? ?? ?? ?? 75 ?? 56 8B 35 ?? ?? ?? ?? 85 F6
 * 83 EC ? 81 ? ? ? ? ? 00 00 75 ? ? 8B ? ? ? ? ? 85
 * or
 * A1 ? ? ? ? 85 C0 75 ? 5F C3 8D
 */
#define CUserLocal__GetInstance() *addrT(CUserLocal **, 01E2CBF4) /* v161.1 */

#pragma pack(push, 1) /* ensures that the compiler won't mess with the byte alignment */
/*
 * the offsets should be constant so I'm not gonna use the offset macros here
 * credits to Shadow for this
 */
typedef struct tagIWzVector2D {
	uint8_t padding1[0xAC];
	double x;						/* 0xAC */
	double y;						/* 0xB4 */
	uint8_t padding2[0xC0];			/* 0xBC */
	int animation;					/* 0x17C */
	uint8_t padding3[0x6C];			/* 0x180 */
	int knockback_toggle;			/* 0x1EC */
	uint8_t padding4[0x04];			/* 0x1F0 */
	long double knockback_vx;		/* 0x1F4    | Default knockback-values are (-)270 in decimal */
	long double knockback_vy;		/* 0x1FC */
} IWzVector2D;

typedef struct tagCUserLocal {
	init_offsets(0, 0, 6)
	uint32_t vtable;

	/* mob movement offset 83 ? ? ? ? ? ? 0F 85 ? ? ? ? 8B ? ? 8B ? ? ? ? 00 8D ? ? 8B */
	define_offset(400) /* v161.1 */
	struct tagmobinfo {
		uint32_t movement;
		uint32_t unk;
		uint32_t aggro;
	} mobinfo;

	/* breath offset 83 B8 ? ? ? ? 00 7E ? 6A 00 6A [Any Result] */
	define_offset(8B0) /* v161.1 */
	uint32_t breath;
	struct taganimation {
		uint32_t id;
		int32_t unk1; /* seems to be always 0xFFFFFFFF (-1), could be either a bitmask or a signed int */
		uint32_t frame;
		uint32_t delay;
		uint32_t totalduration;
		uint32_t unk2; /* probabilly breath duration (2000ms) */
	} animation;

	/* 
	 * vector controller offset 
	 * 8B 86 ? ? 00 00 6A D8 
	 * 8B ? ? ? 00 00 6A ? ? ? 8B ? 89 ? ? ? 89 ? 3B ? 74
	 */
	define_offset(517C) /* v161.1 */
	IWzVector2D *m_pvc; /* _com_ptr_t<_com_IIID<IWzVector2D,&_GUID_f28bd1ed_3deb_4f92_9eec_10ef5a1c3fb4> > m_pvc; */

	/* 
	 * teleport X offset 
	 * 8D 8E ? ? ? ? C7 44 24 14 0A 00 00 00 E8 ? ? ? ? 68 
	 * 1st: 8D ? ? ? ? 00 C7 ? ? ? ? ? 00 00 E8 ? ? ? ? 68 ? ? ? ? 6A ? 6A ? 8D ? ? ? ? ? ? C6 ? ? ? ? E8 ? ? ? ? 8B
	 */
	define_offset(BCA4 - 36) /* v161.1 */
	uint32_t teleport_toggle;

	/* attack count offset 89 ? ? ? ? 00 C7 ? ? ? ? 00 ? ? ? 00 8D ? ? ? ? 00 C6 */
	define_offset(BF84 - 8) /* v161.1 */
	struct tagattack {
		int32_t lastx, lasty;
		uint32_t count;
	} attack;

	/* 
	 * char XY offset 
	 * 89 8E ? ? ? ? 8B 50 ? 8B 06 89 96 ? ? ? ? 8B 50 
	 * 89 ? ? ? ? ? 8B ? ? 8B ? 89 ? ? ? ? ? 8B ? ? 8B ? FF
	 */
	define_offset(D0F0) /* v161.1 */
	int32_t x, y;
} CUserLocal;

void CUserLocal_str(__in CUserLocal *p, __out char *buf, size_t bufsize);

/* thanks Shadow */
typedef struct tagMobData3 {
	uint8_t padding[0x58]; /* xy offset */
	POINT pos;
	POINT ipos; /* == 0 if the mob is invisible */
} MobData3;

/* thanks Shadow */
typedef struct tagMobData2 {
	uint8_t padding[0x24]; /* 4th offset */
	MobData3 *pdata3;
} MobData2;

/* thanks Shadow */
typedef struct tagMobData1 {
	init_offsets(0, 0, 1)
	uint32_t placeholder;

	/* 3rd offset 83 ? ? ? ? ? ? 0F 84 ? ? ? ? 83 ? ? 39 ? ? ? ? ? 0F 8E ? ? ? ? 68 ? ? ? ? 8D ? ? ? E8 ? ? ? ? 68 ? ? ? ? 8D ? ? ? C7 ? ? ? ? ? ? ? E8 ? ? ? ? 8B ? ? ? ? ? C6 ? ? ? ? 85 */
	define_offset(1CC) /* v161.1 */
	MobData2 *pdata2;
} MobData1;

/* thanks Shadow */
typedef struct tagMobNode {
	uint8_t padding1[4]; /* these offsets are most likely constant */
	struct tagMobNode *next;
	struct tagMobNode *prev;
	uint8_t padding2[8];
	MobData1 *pdata1; 
	/* 2nd offset is usually 4 because pfirst_plus_0x10 points at padding2 + 4, 4 bytes before data */
} MobNode;

/*
 * TSingleton<CMobPool>::ms_pInstance (Mob Base):
 * 8B 0D ? ? ? ? ? E8 ? ? ? ? 8B ? 85 ? 74 ? 8B ? ? 8B
 */
#define CMobPool__GetInstance() *addrT(CMobPool **, 01E312C4) /* v161.1 */

typedef struct tagCMobPool {
	init_offsets(0, 0, 2)
	uint32_t placeholder;
	
	/* 
	 * mob count offset 
	 * 8B 52 ? 83 C7 ? 8D 44 24 ? 50 8B CF FF D2 8B 00 89 
	 * 8B ? ? 83 ? ? 8D ? ? ? ? 8B ? FF ? ? ? 89
	 */
	define_offset(10)
	int32_t size;

	/* 
	 * 1st offset 
	 * 8B 49 ? 33 ED 89 4C 24 ? 3B CD 0F 84 ? ? ? ? EB ? 8D 9B ? ? ? ? 8D 44 24 ? 50 
	 * 8B ? ? 33 ? 89 ? ? ? 3B ? 0F 84 ? ? ? ? EB ? 8D ? ? ? ? ? 8D ? ? ? ? E8 ? ? ? ? 8B ? ? 8B
	 */
	define_offset(28)
	uint8_t *pfirst_plus_0x10; /* (subtract 0x10 from this) pointer to the first mob in the linked list */
} CMobPool;

MobNode *CMobPool_first(__in CMobPool *p);
void CMobPool_str(__in CMobPool *p, __out char *buf, size_t bufsize);

/*
 * TSingleton<CDropPool>::ms_pInstance (Item Base):
 * 89 3D ? ? ? ? 8D 4E ? C7 06
 * 89 ? ? ? ? ? 8D ? ? C7 ? ? ? ? ? 89 ? BB
 */
#define CDropPool__GetInstance() *addrT(CDropPool **, 01E31970) /* v161.1 */

typedef struct tagCDropPool {
	/* 
	 * item count offset (prolly constant) 
	 * 8B 4C 24 ? 83 C1 F8 83 F9 50 77 ? 0F ? ? ? ? ? ? FF 
	 * 8B ? ? ? 83 ? ? 83 ? ? 77 ? 0F ? ? ? ? ? ? FF ? ? ? ? ? ? 85 ? 74 ? EB
	 */
	uint8_t padding[0x14];

	int32_t size;
} CDropPool;

void CDropPool_str(__in CDropPool *p, __out char *buf, size_t bufsize);

/*
 * TSingleton<CWndMan>::ms_pInstance:
 * 8B 15 ? ? ? ? 85 D2 74 23
 * 8B 15 ? ? ? ? 85 ? 74 ? E8 ? ? ? ? 85
 */
#define CWndMan__GetInstance() *addrT(CWndMan **, 01E313BC) /* v161.1 */

typedef struct tagCWndMan {
	int32_t placeholder;
} CWndMan;

/*
 * TSingleton<CWvsPhysicalSpace2D>::ms_pInstance:
 * A1 ? ? ? ? 8B 50 ? 83 C0
 * A1 ? ? ? ? 8B ? ? 83 ? ? 83 ? ? 89
 */
#define CWvsPhysicalSpace2D__GetInstance() *addrT(CWvsPhysicalSpace2D **, 01E312CC) /* v161.1 */

typedef struct tagCWvsPhysicalSpace2D {
	uint8_t padding1[0x1C]; /* constant bounds offset */
	RECT bounds;
} CWvsPhysicalSpace2D;

void CWvsPhysicalSpace2D_str(__in CWvsPhysicalSpace2D *p, __out char *buf, size_t bufsize);

/*
 * TSingleton<CUIStatusBar>::ms_pInstance:
 * 8B 0D ? ? ? ? 89 ? 24 ? ? ? 6A
 * 8B 0D ? ? ? ? 89 ? ? ? ? ? 6A ? ? E8 ? ? ? ? 8B
 */
#define CUIStatusBar__GetInstance() *addrT(CUIStatusBar **, 01E2CBF8) /* v161.1 */

typedef struct tagCUIStatusBar {
	/* 
	 * hp offset 
	 * 89 8E ? ? 00 00 80 BE ? ? 00 00 00 75 
	 * 89 ? ? ? 00 00 80 ? ? ? 00 00 00 75
	 */
	uint8_t padding[0x2734]; /* v161.1 */
	int32_t hp;
	int32_t mp;
} CUIStatusBar;

void CUIStatusBar_str(__in CUIStatusBar *p, __out char *buf, size_t bufsize);

/*
 * TSingleton<CWvsContext>::ms_pInstance (server base):
 * 8B 2D ? ? ? ? A1 ? ? ? ? 8D ? 24 ? ? 8B
 * 8B ? ? ? ? ? A1 ? ? ? ? 8D ? ? ? ? 8B ? 89 ? ? ? E8
 */
#define CWvsContext__GetInstance() *addrT(CWvsContext **, 01E2CBF0) /* v161.1 */

typedef struct tagTSecType_unsigned_long_ {
	uint32_t placeholder;
} TSecType_unsigned_long_;

typedef struct tagCWvsContext {
	init_offsets(0, 0, 2)
	uint32_t placeholder;

	/* 
	 * world offset 
	 * 8B 8F ? ? 00 00 8B ? 51 50 6A ? 8B ? C7 ? 24 ? 01 
	 * 8B 8F ? ? 00 00 8B ? ? ? 6A ? 8B ? C7 ? ? ? 01 
	 */
	define_offset(2194) /* v161.1 */
	uint32_t worldid;
	uint32_t unk;
	uint32_t channel;

	/* credits to DarkBoy */
	/* tubi offset - lea at 8B ? ? ? ? 8B ? ? 8D ? ? ? 00 00 E8 ? ? ? ? E8 ? ? ? ? ? 8D */
	define_offset(2218) /* v161.1 */
	TSecType_unsigned_long_ encrypted_tubi;
} CWvsContext;

void CWvsContext_str(__in CWvsContext *p, __out char *buf, size_t bufsize);

/* 
 * CInputystem::ms_pInstance (mouse base) 
 * 8B 0D ? ? ? ? 74 ? 83 ? ? ? ? ? 00 74
 * 8B ? ? ? ? ? 74 ? 83 ? ? ? ? ? 00 74 ? 6A ? 6A ? E8 ? ? ? ? C2 04 00
 */
#define CInputSystem__GetInstance() *addrT(CInputSystem **, 01E2CC00) /* v161.1 */

typedef struct tagMouseLocation {
	/* GR2D_DX9.DLL+175A6 - 8B 8D 90000000        - mov ecx,[ebp+00000090] */
	uint8_t padding[0x90]; 
	long x;
	long y;
} MouseLocation;

typedef struct tagCInputSystem {
	init_offsets(0, 0, 2)
	uint32_t placeholder;
	
	/* 
	 * location offset 
	 * 8B ? ? ? ? ? 85 ? 75 ? 68 ? ? ? ? E8 ? ? ? 00 8B ? 24 ? 8B ? 8B ? ? ? ? ? F7 
	 * 8B ? ? ? ? ? 85 ? 75 ? 68 ? ? ? ? E8 ? ? ? ? 8B ? ? ? 8B ? 8B ? ? ? ? ? F7
	 */
	define_offset(978) /* v161.1 */
	MouseLocation *loc;

	/* animation offset (below base ptr) */
	define_offset(A48) /* v161.1 */
	uint32_t animation;
} CInputSystem;

/*
 * CUserPool::ms_pInstance (people base)
 * 8B 0D ? ? ? ? 74 ? 83 ? ? ? ? ? 00 74
 * 8B ? ? ? ? ? 74 ? 83 ? ? ? ? ? 00 74 ? 6A ? 6A ? E8 ? ? ? ? C2 04 00
 */
#define CUserPool__GetInstance() *addrT(CUserPool **, 01E2CC00) /* v161.1 */

typedef struct tagCUserPool {
	/* second mov below base */
	uint8_t padding1[0x18];
	int32_t count;
} CUserPool;

/*
 * Field pointer when in-game and in an actual map (doesn't work for cash shop and other special stuff)
 * TODO: call / clone maple's get_field
 * above sendtransferchannel request call
 */
#define get_field() *addrT(CField **, 01E3AC7C) /* v161.1 */

typedef struct tagCField {
	/* ? ? ? ? 00 00 88 ? ? ? 8B ? ? ? ? 8D ? ? ? E8 ? ? ? ? ? 83 ? ? 8B ? 89 */
	uint8_t padding1[0x348];
	uint8_t portalcount;
} CField;

/* by Benny / Shadow */
typedef struct tagTSecData_long_ {
	long data;
	uint8_t key;
	uint8_t fake1;
	uint8_t fake2;
	uint16_t checksum;
} TSecData_long_;

/* by Benny / Shadow */
typedef struct tagTSecType_long_ {
	uint32_t fakeptr1;
	uint32_t fakeptr2;
	TSecData_long_ *psecdata;
} TSecType_long_;

typedef struct tagZXString_char_ {
	char *data;
} ZXString_char_;

/* by Mahorori */
typedef struct tagCCtrlEdit {
	uint8_t padding[0x34];
	ZXString_char_ text;
} CCtrlEdit;

/* by Mahorori */
/* 8B 0D ? ? ? ? B8 01 00 00 00 85 C9 74 ? ? ? ? ? 00 00 E8 */
/* or find out what calls 85 C0 0F 84 ? ? 00 00 8B 4E ? 8B C1 */
#define CSecurityClient__GetInstance() *addrT(CSecurityClient **, 01E31990) /* v161.1 */
typedef struct tagCSecurityClient {
	uint8_t padding[0x140];
	TSecType_long_ t;
} CSecurityClient;

/* by Mahorori */
/* 8B 0D ? ? ? ? 83 C4 08 C7 ? ? ? 01 00 00 00 E9 */
#define CUITitle__GetInstance() *addrT(CUITitle **, 01E3A53C) /* v161.1 */
typedef struct tagCUITitle {
	uint8_t padding1[0x180];
	CCtrlEdit *edit_user;
	uint8_t padding2[0x04];
	CCtrlEdit *edit_password;
} CUITitle;

/* by Mahorori */
/* 8B 0D ? ? ? ? 85 C9 74 ? 8B ? ? 8B ? ? ? 00 00 83 C1 04 68 ? ? ? ? FF ? 85 C0 74 ? */
#define CLogin__GetInstance() *addrT(CLogin **, 01E3AC7C) /* v161.1 */
typedef struct tagCLogin {
	uint8_t padding1[0x3C8];
	int index;
	uint8_t padding2[0x16C];
	ZXString_char_ pic;
} CLogin;
#pragma pack(pop)

#endif
