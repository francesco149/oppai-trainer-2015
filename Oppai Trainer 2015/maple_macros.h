#ifndef MAPLE_MACROS_H
#define MAPLE_MACROS_H

#include <stdint.h>

#define AUTOCC_DISABLED 0
#define AUTOCC_PEOPLE	(1 << 0)
#define AUTOCC_MOBCOUNT (1 << 1)
#define AUTOCC_TIMED	(1 << 2)

/* 
	sets the autocc mode, which is a bitfield that uses the following flags:

	AUTOCC_DISABLED: default value for disabled autocc
	AUTOCC_PEOPLE: cc when people > something
	AUTOCC_MOBCOUNT: cc when mobs < something
	AUTOCC_TIMED: cc every x milliseconds
*/
void maple_setautoccflags(int32_t flags);

void maple_setautoccpeople(int v);
void maple_setautoccmobs(int v);
void maple_setautocctime(int v);
int maple_isautoccing(int enqueue);

#define MAPLEKEY_DOWN(x)	(x | 0x00000030)
#define MAPLEKEY_UP(x)		(x | 0x80000000)
#define MAPLEKEY_CTRL		0x001D0000
#define MAPLEKEY_SHIFT		0x002A0000
#define MAPLEKEY_INSERT		0x01520000
#define MAPLEKEY_DEL		0x01530000
#define MAPLEKEY_HOME		0x01470000
#define MAPLEKEY_END		0x014F0000
#define MAPLEKEY_PAGEUP		0x01490000
#define MAPLEKEY_PAGEDOWN	0x01510000
#define MAPLEKEY_1			0x00020000
#define MAPLEKEY_2			0x00030000
#define MAPLEKEY_3			0x00040000
#define MAPLEKEY_4			0x00050000
#define MAPLEKEY_5			0x00060000
#define MAPLEKEY_6			0x00070000
#define MAPLEKEY_7			0x00080000
#define MAPLEKEY_8			0x00090000
#define MAPLEKEY_9			0x000A0000
#define MAPLEKEY_0			0x000B0000
#define MAPLEKEY_ALT		0x00380000
#define MAPLEKEY_Z			0x002C0000

/* calls CWndMan::OnKey with the given key. see MAPLEKEY_* codes. */
int maple_presskey(unsigned int key);

/* keypress type for maplestory macros */
typedef enum tagkeypresstype {
	KEYPRESS_PRESSED, /* normal keypress */
	KEYPRESS_HOLD, /* hold and release (for combos) */
	KEYPRESS_FASTHOLD, /* fast hold and release (cancels combo animation on some classes) */
	KEYPRESS_KEEPDOWN, /* hold down all the time (for specific classes) */
} keypresstype;

/* enables or disable the given key's macro (type and delay are ignored when enabled is 0) */
int maple_setmacro(unsigned int virtual_key, unsigned int key, int enabled, uint32_t delay, keypresstype type);

/* returns 1 if the macro for the given key is enabled */
int maple_macroenabled(unsigned int key);

/* enables or disables autohp (value is ignored when enabled is 0) */
int maple_setautohp(unsigned int key, int enabled, uint32_t value);

/* enables or disables automp (value is ignored when enabled is 0) */
int maple_setautomp(unsigned int key, int enabled, uint32_t value);

/* executes one frame for all of the macros */
void maple_processmacros();

#if _DEBUG
typedef struct taghack hack;
extern hack maple_debugkeyhook;
#endif

#endif
