/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of Oppai Trainer 2015.
	Oppai Trainer 2015 is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	Oppai Trainer 2015 is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with Oppai Trainer 2015. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __BOTTING_H__
#define __BOTTING_H__

typedef enum tagkeypresstype keypresstype;

#include <windows.h>
void botting_savesettings();
void botting_loadsettings();
void botting_show();
void botting_create(HWND parent);
void botting_destroy();
int botting_getautoattacktype(keypresstype *kptype);

#endif
