/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of hackdesu.
	hackdesu is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	hackdesu is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with hackdesu. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __HACKDESU_STRING_H__
#define __HACKDESU_STRING_H__

#include <stdint.h>

/* errors returned by string_utils functions */
typedef enum tagstrerr {
	STR_SUCCESS = 0,
	STR_INVALID,		/* invalid input */
	STR_BUFOVERRUN,		/* buffer too small */
	STR_LARGEINPUT,		/* input too large */
} strerr;

/* returns a message that describes the given error */
const char *strerr_msg(strerr err);

/* removes any spaces from str and copies the result to dst */
strerr string_stripspaces(__in const char *str, __out char *dst, int dstsize);

/* returns non-zero if the string only contains hex characters (0123456789ABCDEF). case-insensitive. */
uint8_t string_ishex(__in const char *str);

/*	
	converts a hex string to a byte array
	e.g: "AA BB 12 34" -> { 0xAA, 0xBB, 0x12, 0x34 }

	accepted string formats:
	-	spaces are stripped from the string, so both "AA BB" and "AABB" are valid inputs.
		NOTE: this also means that "a b c d" is valid and will translate to { 0xAB, 0xCD }.
	-	case-insensitive. "aaBB", "aabb", "AABB" are all valid inputs.
	-	there must be no truncated bytes in the string. "AA B CC" and similar inputs will be invalid.	
*/
strerr string_tobytearray(__in const char *str, __out uint8_t *dst, int dstsize, __out_opt int *pcbparsed);

/*
	fills dst with len random characters from charset.
	this assumes that charset is null-terminated and contains at least one character.
	this also assumes that rand()'s seed has already been initialized.
*/
strerr string_rand(__out char *dst, __in const char *charset, size_t len);

#endif
