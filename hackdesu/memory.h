/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of hackdesu.
	hackdesu is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	hackdesu is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with hackdesu. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __HACKDESU_MEMORY_H__
#define __HACKDESU_MEMORY_H__

#include <inttypes.h>
#include <windows.h>

/* retrieves module base and size of the given HMODULE and stores them in pbase and psize */
int module_size(__in HMODULE module, __out void **pbase, __out size_t *psize);

/* module_size alternative with integer parameters */
#define module_size_i(module, pbase, psize) (uint32_t)module_size(module, (void **)pbase, psize)

/* 
	creates a copy of the given memory region and returns a pointer to it.
	the memory dump can be later freed by simply calling free.
	if the machine is out of memory or any other error occurs, the function will return NULL. 
*/
void *memory_dump(__in void *begin, size_t size);

/* memory_dump alternative with integer parameters and return value */
#define memory_dump_i(begin, size) (uint32_t)memory_dump((void *)begin, size)

/* 
	retrieves module base and size of the given HMODULE, creates a dump of the module,
	stores module start and end in begin and end and returns a pointer to the memory dump.
	the memory dump can be later freed by simply calling free. 
*/
void *module_dump(__in HMODULE module, __out void **begin, __out void **end);

/* module_dump alternative with integer parameters and return value */
#define module_dump_i(module, begin, end) (uint32_t)module_dump(module, (void **)begin, (void **)end)

/* 
	sets memory permissions at pmemory for cb bytes
	and stores the previous permissions value in the given dword pointer
	returns zero if unsuccessful. 
*/
int memory_protect(__in void *pmemory, int cb, uint32_t flprotect, uint32_t *poldprotect);

/* memory_protect alternative with integer address */
#define memory_protect_i(pmemory, cb, flprotect, poldprotect) memory_protect((void *)pmemory, cb, flprotect, poldprotect)

/* copies bytes (byte array of length cb) to pmemory. returns zero if unsuccessful. */
int memory_write(__inout uint8_t *pmemory, __in const uint8_t *bytes, int cb);

/* writes the raw bytes of the value val of type type at pmemory. returns zero if unsuccessful. */
#define memory_write_type(type, pmemory, val) memory_write(pmemory, (uint8_t *)&val, sizeof(type))

/* writes val (uint8_t, 1 byte) at pmemory */
void memory_write1(__inout void *pmemory, uint8_t val);

/* writes val (int8_t, 1 byte) at pmemory */
#define memory_write1s(pmemory, val) memory_write1(pmemory, (uint8_t)val)

/* writes val (uint16_t, 2 bytes) at pmemory */
void memory_write2(__inout void *pmemory, uint16_t val);

/* writes val (int16_t, 2 bytes) at pmemory */
#define memory_write2s(pmemory, val) memory_write2(pmemory, (uint16_t)val)

/* writes val (uint32_t, 4 bytes) at pmemory */
void memory_write4(__inout void *pmemory, uint32_t val);

/* writes val (int32_t, 4 bytes) at pmemory */
#define memory_write4s(pmemory, val) memory_write4(pmemory, (uint32_t)val)

/* writes val (uint64_t, 8 bytes) at pmemory */
void memory_write8(__inout void *pmemory, uint64_t val);

/* writes val (int64_t, 8 bytes) at pmemory */
#define memory_write8s(pmemory, val) memory_write8(pmemory, (uint64_t)val)

/* detours a function. returns zero if unsuccessful. */
int memory_detour(int enabled, __inout void **pptarget, __in void *pdetour);

/* 
	writes the given 1-byte opcode at dst followed by the distance between pmemory and dst
	and the given number of nop bytes. 
*/
void memory_write_distance1(int8_t opcode, __inout int8_t *pmemory, __in int8_t *dst, int nops);

/* writes a 0xE9 5-byte jump at pmemory to dst followed by the given number of nop bytes */
#define memory_write_jumpE9(pmemory, dst, nops) memory_write_distance1(0xE9, pmemory, dst, nops)

#endif
