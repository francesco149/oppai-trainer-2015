/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of hackdesu.
	hackdesu is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	hackdesu is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with hackdesu. If not, see <http://www.gnu.org/licenses/>.
*/

#include "command_queue.h"

#include <errno.h>
#include "memory.h"
#include "io.h"

/* liblfds611 */
#include "liblfds611.h"
#if _DEBUG
#pragma comment(lib, "liblfds611_d.lib")
#else
#pragma comment(lib, "liblfds611.lib")
#endif

const cq_callback cq_no_callback = { 0 };

/* commands */
typedef enum tagcq_command_id {
	CQ_CALL, 
	CQ_WRITE, 
	CQ_WRITE1, 
	CQ_WRITE2, 
	CQ_WRITE4, 
	CQ_WRITE8, 
	CQ_WRITE_JUMPE9, 
	CQ_DETOUR, 
	CQ_READ, 
} cq_command_id;

/* consts */
#define CQ_CAPACITY 512

/* internal lock-free MPMC queue */
static struct lfds611_queue_state *qs = NULL;

/* this struct holds information about an enqueued command */
typedef struct _cq_command {
	cq_command_id id; /* command id */
	void **params; /* parameters array (all casted to generic void ptrs) */
	int nparams; /* length of the parameters array */
} cq_command;

/*
	initializes the command queue.
	must be called before doing anything with the command queue.
	returns zero on failure.
*/
int cq_init() {
	if (!lfds611_queue_new(&qs, CQ_CAPACITY) || !qs) {
		dbgputs("cq_init: lfds611_queue_new failed");
		return 0;
	}
	dbgputs("Command queue initialized.");
	return 1;
}

/* creates a cq_command struct with the given parameters and enqueues it */
static int cq_enqueue(cq_command_id id, __inout_opt void **params, int nparams) {
	cq_command *cmd = NULL;

	cmd = (cq_command *)malloc(sizeof(cq_command));
	if (!cmd) {
		dbgputs("cq_enqueue: malloc failed (out of memory?)");
		return 0;
	}

	memset(cmd, 0, sizeof(cq_command));
	cmd->id = id;
	cmd->params = params;
	cmd->nparams = nparams;

	if (!lfds611_queue_enqueue(qs, cmd)) {
		dbgputs("cq_enqueue: lfds611_queue_enqueue failed (queue full?)");
		return 0;
	}

	return 1;
}

/* allocates a parameter list for the cq_command struct. returns NULL on failure. */
static void **cq_make_params(int nparams) {
	void **params = NULL;
	params = (void **)malloc(sizeof(void *) * nparams);
	if (!params) {
		dbgputs("cq_makeparams: malloc failed (out of memory?)");
	}
	return params;
}

/*
	enqueues the execution of a callback. returns zero on failure.
	the cleanup callback will be called when param needs to be
	deallocated and should perform any cleanup required for param.
	cleanup can be null.
	if the function fails, cleanup will be automatically called.
*/
int cq_call(__in cq_callback c, __inout_opt void *param, __in_opt cq_callback cleanup) {
	void **params = cq_make_params(3);
	if (!params) {
		return 0;
	}
	params[0] = c.void_ptr;
	params[1] = param;
	params[2] = cleanup.void_ptr;

	if (!cq_enqueue(CQ_CALL, params, 3)) {
		cleanup.func_ptr(param);
		return 0;
	}

	return 1;
}

/* performs cleanup for a CQ_CALL command */
static void cq_delete_call(__in cq_command *cmd) {
	void **params;
	cq_callback cleanup;

	params = cmd->params;
	cleanup.void_ptr = params[2];
	if (cleanup.void_ptr) {
		cleanup.func_ptr(params[1]);
	}
}

/* executes a CQ_CALL command */
static void cq_perform_call(__in cq_command *cmd) {
	void **params;
	cq_callback c;
	void *param;
	
	params = cmd->params;
	c.void_ptr = params[0];
	param = params[1];

	c.func_ptr(param);
}

/* 
	enqueues a memory_write. returns zero on failure. 
	NOTE: a copy of bytes will be automatically allocated and freed by the command queue, 
		  so bytes only needs to be valid until cq_write returns.
*/
int cq_write(__inout uint8_t *pmemory, __in const uint8_t *bytes, int cb) {
	void **params;
	uint8_t *bytes_copy;
	errno_t err;
	char buferr[512] = { 0 };

	params = cq_make_params(3);
	if (!params) {
		return 0;
	}

	/* alloc a copy of the byte array */
	bytes_copy = malloc(sizeof(uint8_t) * cb);
	if (!bytes_copy) {
		dbgputs("cq_write: malloc failed (out of memory?).");
		return 0;
	}
	
	/* copy the contents */
	err = memcpy_s(bytes_copy, cb, bytes, cb);
	if (err) {
		strerror_s(buferr, 512, err);
		dbgprintf("memcpy_s failed: %s\n", buferr);
		return 0;
	}

	params[0] = pmemory;
	params[1] = bytes_copy;
	params[2] = (void *)cb;
	return cq_enqueue(CQ_WRITE, params, 3);
}

/* executes a CQ_WRITE command */
static void cq_perform_write(__in cq_command *cmd) {
	void **params;
	void *pmemory;
	uint8_t *bytes;
	int cb;

	params = cmd->params;
	pmemory = params[0];
	bytes = (uint8_t *)params[1];
	cb = (int)params[2];

	memory_write(pmemory, bytes, cb);
}

/* performs cleanup for CQ_WRITE commands */
static void cq_delete_write(__inout cq_command *cmd) {
	void **params;
	uint8_t *bytes;

	params = cmd->params;
	bytes = (uint8_t *)params[1];
	free(bytes);
	bytes = NULL;
	params[1] = NULL;
}

/* enqueues a memory_write1. returns zero on failure. */
int cq_write1(__inout void *pmemory, uint8_t val) {
	void **params = cq_make_params(2);
	if (!params) {
		return 0;
	}
	params[0] = pmemory;
	params[1] = (void *)(uint32_t)val;
	return cq_enqueue(CQ_WRITE1, params, 2);
}

/* executes a CQ_WRITE1 command */
static void cq_perform_write1(__in cq_command *cmd) {
	void **params;
	void *pmemory;
	uint8_t val;

	params = cmd->params;
	pmemory = params[0];
	val = (uint8_t)(uint32_t)params[1];

	memory_write1(pmemory, val);
}

/* enqueues a memory_write2. returns zero on failure. */
int cq_write2(__inout void *pmemory, uint16_t val) {
	void **params = cq_make_params(2);
	if (!params) {
		return 0;
	}
	params[0] = pmemory;
	params[1] = (void *)(uint32_t)val;
	return cq_enqueue(CQ_WRITE2, params, 2);
}

/* executes a CQ_WRITE2 command */
static void cq_perform_write2(__in cq_command *cmd) {
	void **params;
	void *pmemory;
	uint16_t val;

	params = cmd->params;
	pmemory = params[0];
	val = (uint16_t)(uint32_t)params[1];

	memory_write2(pmemory, val);
}

/* enqueues a memory_write4. returns zero on failure. */
int cq_write4(__inout void *pmemory, uint32_t val) {
	void **params = cq_make_params(2);
	if (!params) {
		return 0;
	}
	params[0] = pmemory;
	params[1] = (void *)val;
	return cq_enqueue(CQ_WRITE4, params, 2);
}

/* executes a CQ_WRITE4 command */
static void cq_perform_write4(__in cq_command *cmd) {
	void **params;
	void *pmemory;
	uint32_t val;

	params = cmd->params;
	pmemory = params[0];
	val = (uint32_t)params[1];

	memory_write4(pmemory, val);
}

/* enqueues a memory_write8. returns zero on failure. */
int cq_write8(__inout void *pmemory, uint64_t val) {
	void **params = cq_make_params(2);
	uint64_t *pval = (uint64_t *)malloc(sizeof(uint64_t));
	if (!params) {
		return 0;
	}
	params[0] = pmemory;
	params[1] = pval;
	*pval = val;
	return cq_enqueue(CQ_WRITE8, params, 2);
}

/* executes a CQ_WRITE8 command */
static void cq_perform_write8(__in cq_command *cmd) {
	void **params;
	void *pmemory;
	uint64_t *pval;

	params = cmd->params;
	pmemory = params[0];
	pval = (uint64_t *)params[1];

	memory_write8(pmemory, *pval);
}

/* performs cleanup for a CQ_WRITE8 command */
static void cq_delete_write8(__in cq_command *cmd) {
	void **params;
	uint64_t *pval;

	params = cmd->params;
	pval = (uint64_t *)params[1];
	free(pval);
	pval = NULL;
	params[1] = NULL;
}

/* enqueues a memory_writejumpE9. returns zero on failure. */
int cq_write_jumpE9(__inout void *pmemory, __in int8_t *dst, int nops) {
	void **params = cq_make_params(3);
	if (!params) {
		return 0;
	}
	params[0] = pmemory;
	params[1] = dst;
	params[2] = (void *)nops;
	return cq_enqueue(CQ_WRITE_JUMPE9, params, 3);
}

/* executes a CQ_WRITEJUMPE9 command */
static void cq_perform_write_jumpE9(__in cq_command *cmd) {
	void **params;
	void *pmemory;
	int8_t *dst;
	int nops;

	params = cmd->params;
	pmemory = params[0];
	dst = (int8_t *)params[1];
	nops = (int)params[2];

	memory_write_jumpE9(pmemory, dst, nops);
}

int cq_detour(int enabled, __inout void **pptarget, __in void *pdetour) {
	void **params = cq_make_params(3);
	if (!params) {
		return 0;
	}
	params[0] = (void *)enabled;
	params[1] = (void *)pptarget;
	params[2] = pdetour;
	return cq_enqueue(CQ_DETOUR, params, 3);
}

static void cq_perform_detour(__in cq_command *cmd) {
	void **params;
	int enabled;
	void **pptarget;
	void *pdetour;

	params = cmd->params;
	enabled = (int)params[0];
	pptarget = (void **)params[1];
	pdetour = params[2];

	memory_detour(enabled, pptarget, pdetour);
}

/* enqueues a memcpy and waits for the command to be processed. returns zero on failure. */
int cq_read(__inout uint8_t *pdst, __in uint8_t *pmemory, int cb) {
	void **params;
	struct lfds611_queue_state *readqs = NULL;
	void *res = NULL;

	if (!lfds611_queue_new(&readqs, 1) || !readqs) {
		dbgputs("lfds611_queue_new failed (out of memory?)");
		return 0;
	}

	params = cq_make_params(4);
	if (!params) {
		return 0;
	}

	params[0] = readqs;
	params[1] = pdst;
	params[2] = pmemory;
	params[3] = (void *)cb;

	if (!cq_enqueue(CQ_READ, params, 4)) {
		return 0;
	}

	while (!lfds611_queue_dequeue(readqs, &res)) {
		Sleep(1);
	}

	lfds611_queue_delete(readqs, NULL, NULL);
	readqs = NULL;
	params[0] = NULL;

	return (int)res;
}

static void cq_perform_read(__in cq_command *cmd) {
	void **params;
	struct lfds611_queue_state *readqs = NULL;
	uint8_t *pdst = NULL;
	uint8_t *pmemory = NULL;
	int cb = 0;
	errno_t err;
	int failed = 0;
	char buferr[512] = { 0 };

	params = cmd->params;
	readqs = (struct lfds611_queue_state *)params[0];
	pdst = (uint8_t *)params[1];
	pmemory = (uint8_t *)params[2];
	cb = (int)params[3];

	err = memcpy_s(pdst, cb, pmemory, cb);
	if (err) {
		strerror_s(buferr, 512, err);
		dbgprintf("memcpy_s failed: %s\n", buferr);
		if (!lfds611_queue_enqueue(readqs, (void *)0)) {
			failed = 1;
		}
	}
	else if (!lfds611_queue_enqueue(readqs, (void *)1)) {
		failed = 1;
	}

	if (failed) {
		dbgputs("lfds611_queue_enqueue failed, the cq_read call will freeze");
	}
}

/* handles each destroyed item on cq_destroy */
static void cq_handle_delete(void *user_data, void *user_state) {
	(void)user_state;
	cq_command *cmd = (cq_command *)user_data;

	/* call cleanup for the commands that need it */
	switch (cmd->id) { 
	case CQ_CALL:
		cq_delete_call(cmd);
		break;
	case CQ_WRITE:
		cq_delete_write(cmd);
		break;
	case CQ_WRITE8:
		cq_delete_write8(cmd);
		break;
	}

	/* free params array */
	free(cmd->params);
	cmd->params = NULL;

	/* free command struct */
	free(cmd);
}

/* destroys the command queue. must be called on cleanup. */
void cq_destroy() {
	dbgputs("Performing command queue cleanup...");
	lfds611_queue_delete(qs, cq_handle_delete, NULL);
	qs = NULL;
	dbgputs("Command queue deallocated.");
}

/* when called, all pending commands will be dequeued and executed. */
void cq_process_all() {
	cq_command *cmd = NULL;
	while (lfds611_queue_dequeue(qs, (void **)&cmd)) {
		switch (cmd->id) {
		case CQ_CALL:
			cq_perform_call(cmd);
			break;
		case CQ_WRITE:
			cq_perform_write(cmd);
			break;
		case CQ_WRITE1:
			cq_perform_write1(cmd);
			break;
		case CQ_WRITE2:
			cq_perform_write2(cmd);
			break;
		case CQ_WRITE4:
			cq_perform_write4(cmd);
			break;
		case CQ_WRITE8:
			cq_perform_write8(cmd);
			break;
		case CQ_WRITE_JUMPE9:
			cq_perform_write_jumpE9(cmd);
			break;
		case CQ_DETOUR:
			cq_perform_detour(cmd);
			break;
		case CQ_READ:
			cq_perform_read(cmd);
			break;
		}
		cq_handle_delete(cmd, NULL);
		cmd = NULL;
	}
}
