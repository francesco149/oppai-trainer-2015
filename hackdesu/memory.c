/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of hackdesu.
	hackdesu is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	hackdesu is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with hackdesu. If not, see <http://www.gnu.org/licenses/>.
*/

#include "memory.h"

#include <errno.h>
#include <stdio.h>

#include <dbghelp.h>
#include <psapi.h>
#pragma  comment(lib, "dbghelp")
#pragma  comment(lib, "psapi")

#include "io.h"

/* win32_gui */
#include "win32_gui.h"
#if _DEBUG
#pragma comment(lib, "win32_utils_d.lib")
#else
#pragma comment(lib, "win32_utils.lib")
#endif

/* detours */
#include "detours.h"
#pragma comment(lib, "detours.lib")

/* retrieves module base and size of the given HMODULE and stores them in pbase and psize */
int module_size(__in HMODULE module, __out void **pbase, __out size_t *psize) {
	if (module == GetModuleHandleA(NULL)) {
		PIMAGE_NT_HEADERS pImageNtHeaders = ImageNtHeader((PVOID)module);

		if (!pImageNtHeaders) {
#if _DEBUG
			win32_show_error(__FUNCDNAME__ ": ""ImageNtHeader returned NULL");
#endif
			return 0;
		}

		*pbase = (void *)module;
		*psize = pImageNtHeaders->OptionalHeader.SizeOfImage;
	}
	else {
		MODULEINFO mi;

		if (!GetModuleInformation(GetCurrentProcess(), module, &mi, sizeof(MODULEINFO))) {
#if _DEBUG
			win32_show_error(__FUNCDNAME__ ": ""GetModuleInformation failed");
#endif
			return 0;
		}

		*pbase = mi.lpBaseOfDll;
		*psize = mi.SizeOfImage;
	}

	return 1;
}

/*
	creates a copy of the given memory region and returns a pointer to it.
	the memory dump can be later freed by simply calling free.
	if the machine is out of memory or any other error occurs, the function will return NULL.
*/
void *memory_dump(__in void *begin, size_t size) {
	errno_t err;
	char buferr[512] = { 0 };
	void *dump = malloc(size);
	err = memcpy_s(dump, size, begin, size);
	if (err) {
		strerror_s(buferr, 512, err);
		dbgprintf("memcpy_s failed when dumping memory at %.08X (%lu bytes): %s.\n",
			begin, size, buferr);
		free(dump);
		return NULL;
	}
	return dump;
}

/*
	retrieves module base and size of the given HMODULE, creates a dump of the module,
	stores module start and end in begin and end and returns a pointer to the memory dump.
	the memory dump can be later freed by simply calling free.
*/
void *module_dump(__in HMODULE module, __out void **begin, __out void **end) {
	size_t size;
	if (!module_size(module, begin, &size)) {
		return NULL;
	}

	*end = (uint8_t *)(*begin) + size;
	return memory_dump(*begin, size);
}

/*
	sets memory permissions at pmemory for cb bytes
	and stores the previous permissions value in the given dword pointer
	returns zero if unsuccessful.
*/
int memory_protect(__in void *pmemory, int cb, uint32_t flprotect, uint32_t *poldprotect) {
#if _DEBUG
	char buf[512] = { 0 };
#endif
	if (!VirtualProtect(pmemory, cb, flprotect, (PDWORD)poldprotect)) {
#if _DEBUG
		sprintf_s(buf, 512, __FUNCDNAME__ ": ""VirtualProtect failed for %.08X (%lu bytes).", pmemory, cb);
		win32_show_error(buf);
#endif
		return 0;
	}
	return 1;
}

/* copies bytes (byte array of length cb) to pmemory. returns zero if unsuccessful. */
int memory_write(__inout uint8_t *pmemory, __in const uint8_t *bytes, int cb) {
	errno_t err = 0;
	uint32_t flprotect, trash;
	int i;
	char buferr[512] = { 0 };

	if (!memory_protect(pmemory, cb, PAGE_EXECUTE_READWRITE, &flprotect)) {
		return 0;
	}

	dbgprintf("writing %lu bytes at %.08X: ", cb, pmemory);
	for (i = 0; i < cb; i++) {
		dbgprintf("%.02X ", pmemory[i]);
	}
	dbgprintf("-> ");
	for (i = 0; i < cb; i++) {
		dbgprintf("%.02X ", bytes[i]);
	}
	dbgputchar('\n');

	err = memcpy_s(pmemory, cb, bytes, cb);
	if (err) {
		strerror_s(buferr, 512, err);
		dbgprintf("memcpy_s failed at %.08X (%lu bytes): %s.\n",
			pmemory, cb, buferr);
		return 0;
	}

	trash = flprotect;
	if (!memory_protect(pmemory, cb, flprotect, &trash)) {
		return 0;
	}

	return 1;
}

/* writes val (uint8_t, 1 byte) at pmemory */
void memory_write1(__inout void *pmemory, uint8_t val) {
	memory_write_type(uint8_t, pmemory, val);
}

/* writes val (uint16_t, 2 bytes) at pmemory */
void memory_write2(__inout void *pmemory, uint16_t val) {
	memory_write_type(uint16_t, pmemory, val);
}

/* writes val (uint32_t, 4 bytes) at pmemory */
void memory_write4(__inout void *pmemory, uint32_t val) {
	memory_write_type(uint32_t, pmemory, val);
}

/* writes val (uint64_t, 8 bytes) at pmemory */
void memory_write8(__inout void *pmemory, uint64_t val) {
	memory_write_type(uint64_t, pmemory, val);
}

/* detours a winapi. returns zero if unsuccessful. */
int memory_detour(int enabled, __inout void **pptarget, __in void *pdetour) {
	long err;

	if (enabled) {
		dbgprintf("Detouring %.08X -> %.08X\n", *pptarget, pdetour);
	}
	else {
		dbgprintf("Un-detouring %.08X\n", *pptarget);
	}

	err = DetourTransactionBegin();
	if (err != NO_ERROR) {
		dbgprintf("DetourTransactionBegin failed with error %d\n", err);
		return 0;
	}

	do { 
		err = DetourUpdateThread(GetCurrentThread());
		if (err != NO_ERROR) {
			dbgprintf("DetourUpdateThread failed with error %d\n", err);
			break;
		}

		err = (enabled ? DetourAttach : DetourDetach)(pptarget, pdetour);
		if (err != NO_ERROR) {
			dbgprintf("DetourAttach failed with error %d\n", err);
			break;
		}

		err = DetourTransactionCommit();
		if (err == NO_ERROR) {
			return 1;
		}
		dbgprintf("DetourTransactionCommit failed with error %d\n", err);
#pragma warning(disable: 127)
	} while (0);
#pragma warning(default: 127)
	/* cool trick to handle many errors with the same cleanup code */

	err = DetourTransactionAbort();
	if (err != NO_ERROR) {
		dbgprintf("DetourTransactionAbort failed with error %d\n", err);
	}
	return 0;
}

/*
	writes the given 1-byte opcode at dst followed by the distance between pmemory and dst
	and the given number of nop bytes.
*/
void memory_write_distance1(int8_t opcode, __inout int8_t *pmemory, __in int8_t *dst, int nops) {
	int i;
	int8_t *it = pmemory;

	memory_write1(it, opcode);
	it += sizeof(int8_t);

	memory_write4s(it, (int32_t)(dst - pmemory - 5));
	it += sizeof(int32_t);

	for (i = 0; i < nops; i++) {
		memory_write1(it, 0x90);
		it += sizeof(int8_t);
	}
}
