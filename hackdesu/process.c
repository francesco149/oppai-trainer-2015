/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of hackdesu.
	hackdesu is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	hackdesu is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with hackdesu. If not, see <http://www.gnu.org/licenses/>.
*/

#include "process.h"

#include <windows.h>
#include <tlhelp32.h>
#include <assert.h>

/*
	iterates the process list and finds the index-th value that matches the given key.
	returns the value of GetLastError().
	if the function fails, *pdw will be set to zero.

	key input:
	- if key is PROCESS_NAME, buf must contain the process name and bufsize must be zero.
	- for every other key value, pdw must point to a 32-bit integer that contains your input key value.

	value output:
	- if value is PROCESS_NAME, the result will be stored in buf. in this case,
	buf must point to an empty buffer and bufsize must be set to the maximum capacity of the buffer.
	- for any other type of value, the result will be stored in the 32-bit integer pointed by pdw and
	buf and bufsize will be ignored unless they are used for the key.

	possible values for key / value:
	PROCESS_ID: the process identifier
	PROCESS_THREADS: the number of execution threads started by the process
	PROCESS_PARENTID: the identifier of the process that created this process (its parent process)
	PROCESS_PRICLASSBASE: the base priority of any threads created by this process
	PROCESS_NAME: the name of the executable file for the process
*/
uint32_t process_get_info(__inout char *buf, size_t bufsize, __inout uint32_t *pdw, uint32_t key, uint32_t value, int index) {
	PROCESSENTRY32 pe;
	HANDLE snapshot;
	int found = 0;
	int matchindex = -1;
	BOOL res;

	snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (snapshot == INVALID_HANDLE_VALUE) {
		return GetLastError();
	}

	pe.dwSize = sizeof(PROCESSENTRY32);

	res = Process32First(snapshot, &pe);

	while (res) {
		switch (key) {
		case PROCESS_ID:
			found = pe.th32ProcessID == *pdw;
			break;

		case PROCESS_THREADS:
			found = pe.cntThreads == *pdw;
			break;

		case PROCESS_PARENTID:
			found = pe.th32ParentProcessID == *pdw;
			break;

		case PROCESS_PRICLASSBASE:
			found = pe.pcPriClassBase == (LONG)*pdw;
			break;

		case PROCESS_NAME:
			found = strstr(pe.szExeFile, buf) != NULL;
			break;
		default:
			assert(0);
		}

		if (found) {
			matchindex++;
			if (matchindex != index) {
				continue;
			}

			/* I could also return the entire PROCESSENTRY32 struct and let
			the caller get the info it wants, but I find it simpler this way,
			as the struct has many deprecated, unused members */
			switch (value) {
			case PROCESS_ID:
				*pdw = pe.th32ProcessID;
				return ERROR_SUCCESS;

			case PROCESS_THREADS:
				*pdw = pe.cntThreads;
				return ERROR_SUCCESS;

			case PROCESS_PARENTID:
				*pdw = pe.th32ParentProcessID;
				return ERROR_SUCCESS;

			case PROCESS_PRICLASSBASE:
				*pdw = pe.pcPriClassBase;
				return ERROR_SUCCESS;

			case PROCESS_NAME:
				strcpy_s(buf, bufsize, pe.szExeFile);
				return ERROR_SUCCESS;
			default:
				assert(0);
			}
		}

		res = Process32Next(snapshot, &pe);
	}

	*pdw = 0;
	memset(buf, 0, bufsize);
	return GetLastError();
}

/*
	stores the pid of the first process that matches the given name in *ppid.
	returns the value of GetLastError().
	if the function fails, *ppid will be set to zero.
*/
uint32_t process_pid_by_name(__in const char *processname, __out uint32_t *ppid) {
	return process_get_info((char *)processname, 0, ppid, PROCESS_NAME, PROCESS_ID, 0);
}

/*
	stores the pid of the first process that matches the given name in *ppid.
	returns the value of GetLastError().
	if the function fails, buf will contain an empty string.
*/
uint32_t process_name_by_pid(uint32_t pid, __out char *buf, size_t bufsize) {
	return process_get_info(buf, bufsize, &pid, PROCESS_ID, PROCESS_NAME, 0);
}

/* injects the given dll into pid. returns zero on failure (check GetLastError for details). */
int process_inject_dll(uint32_t pid, const char *fullpath) {
	HANDLE process;
	HMODULE lib;
	LPTHREAD_START_ROUTINE pfnLoadLibraryA;
	void *premotefullpath;
	size_t written;
	size_t cbwrite;
	int res;
	uint32_t gle;
	HANDLE remotethread;
	uint32_t waitevent;

	process = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	if (!process) {
		return 0;
	}

	/* using the local LoadLibraryA address might seem wrong, but in practice kernel32
	is mapped to the same address in all processes so it works */
	lib = GetModuleHandleA("kernel32.dll");
	if (!lib) {
		return 0;
	}
	pfnLoadLibraryA = (LPTHREAD_START_ROUTINE)GetProcAddress(lib, "LoadLibraryA");
	if (!pfnLoadLibraryA) {
		return 0;
	}

	cbwrite = strlen(fullpath) + 1;

	/* allocate a string that will hold the dll's full path in the target process*/
	premotefullpath = VirtualAllocEx(process, NULL, cbwrite, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	if (!premotefullpath) {
		return 0;
	}

	res = 1;
	written = 0;
	gle = ERROR_SUCCESS;

	do {
		/* write the data to the remotely allocated string */
		if (!WriteProcessMemory(process, premotefullpath, fullpath, cbwrite, (SIZE_T *)&written) || written != cbwrite) {
			gle = GetLastError();
			res = 0;
			break;
		}

		/* call LoadLibraryA with the remotely allocated string in the target process */
		remotethread = CreateRemoteThread(process, NULL, 0, pfnLoadLibraryA, premotefullpath, 0, NULL);
		if (!remotethread) {
			gle = GetLastError();
			res = 0;
			break;
		}

		/* wait for the remote thread to terminate */
		waitevent = WaitForSingleObject(remotethread, 30000);
		if (waitevent == WAIT_TIMEOUT) {
			gle = ERROR_TIMEOUT;
			res = 0;
			break;
		}
		else if (waitevent == WAIT_FAILED) {
			gle = GetLastError();
			res = 0;
			break;
		}
#pragma warning(disable: 127)
	} while (0);
#pragma warning(default: 127)

	/* free the remote string */
	VirtualFreeEx(process, premotefullpath, cbwrite, MEM_RELEASE | MEM_DECOMMIT);

	/* close handle to the target process */
	CloseHandle(process);

	/* this ensures that if an error occurs in WriteProcessMemory or CreateRemoteThread,
	the GetLastError value is not overwritten by the VirtualFreeEx and CloseHandle calls */
	SetLastError(gle);
	return res;
}
