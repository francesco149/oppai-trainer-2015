/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of hackdesu.
	hackdesu is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	hackdesu is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with hackdesu. If not, see <http://www.gnu.org/licenses/>.
*/

#include "string.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

/* returns a message that describes the given error */
const char *strerr_msg(strerr err) {
	switch (err) {
	case STR_SUCCESS:
		return "The operation completed successfully.";
	case STR_INVALID:
		return "Invalid input.";
	case STR_BUFOVERRUN:
		return "The destination buffer is too small.";
	case STR_LARGEINPUT:
		return "The input is too large.";
	}
	return "Unknown error.";
}

/* removes any spaces from str and copies the result to dst */
strerr string_stripspaces(__in const char *str, __out char *dst, int dstsize) {
	char *end;
	char c;

	end = dst + dstsize;
	for (; ; str++) {
		c = *str;
		if (!c) {
			break;
		}
		if (dst >= end) {
			return STR_BUFOVERRUN;
		}
		if (c != ' ') {
			*dst = c;
			dst++;
		}
	}

	return STR_SUCCESS;
}

/* returns non-zero if the string only contains hex characters (0123456789ABCDEF). case-insensitive. */
uint8_t string_ishex(__in const char *str) {
	char c;
	for (; ; str++) {
		c = (char)toupper(*str);
		if (!c) {
			break;
		}
		if (c >= '0' && c <= '9' || c >= 'A' && c <= 'F') {
			continue;
		}
		return 0;
	}

	return 1;
}

/*
	converts a hex string to a byte array
	e.g: "AA BB 12 34" -> { 0xAA, 0xBB, 0x12, 0x34 }
	
	accepted string formats:
	-	spaces are stripped from the string, so both "AA BB" and "AABB" are valid inputs.
		NOTE: this also means that "a b c d" is valid and will translate to { 0xAB, 0xCD }.
	-	case-insensitive. "aaBB", "aabb", "AABB" are all valid inputs.
	-	there must be no truncated bytes in the string. "AA B CC" and similar inputs will be invalid.
*/
strerr string_tobytearray(__in const char *str, __out uint8_t *dst, int dstsize, __out_opt int *pcbparsed) {
	const char *end;
	uint8_t *dstend;
	uint8_t *it;
	size_t slen;
	char tmp[512] = { 0 };

	slen = strlen(str);

	/* empty string */
	if (!slen) {
		return STR_INVALID;
	}

	/* remove spaces */
	if (string_stripspaces(str, tmp, 512)) {
		return STR_LARGEINPUT;
	}

	slen = strlen(tmp);

	/* truncated nibble */
	if (slen % 2) {
		return STR_INVALID;
	}

	/* not an hex string */
	if (!string_ishex(tmp)) {
		return STR_INVALID;
	}

	str = tmp;
	end = tmp + slen;

	it = dst;
	dstend = dst + dstsize - 1;

	for (; str < end; str += 2) {
		if (it > dstend) {
			return STR_BUFOVERRUN;
		}
		if (sscanf_s(str, "%02X", it) != 1) {
			return STR_INVALID;
		}
		it++;
	}

	if (pcbparsed) {
		*pcbparsed = it - dst;
	}

	return STR_SUCCESS;
}

/*
	fills dst with len random characters from charset.
	this assumes that charset is null-terminated and contains at least one character.
	this also assumes that rand()'s seed has already been initialized.
*/
strerr string_rand(__out char *dst, __in const char *charset, size_t len) {
	size_t i, cslen;

	cslen = strlen(charset);
	if (!cslen) {
		return STR_INVALID;
	}

	for (i = 0; i < len - 1; i++) {
		dst[i] = charset[rand() % cslen];
	}
	dst[i] = '\0';

	return STR_SUCCESS;
}
