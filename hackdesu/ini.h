/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of hackdesu.
	hackdesu is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	hackdesu is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with hackdesu. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __HACKDESU_INI_H__
#define __HACKDESU_INI_H__

#include <windows.h> 

/* helper struct that makes mass-saving control states easier */
typedef struct tagini_control {
	const char *key;
	int ctlid;
} ini_control;

/* calls WritePrivateProfileStringA with the given parameters and prints an error to stdout on failure */
void ini_write_string(const char *section, const char *key, const char *value, const char *file);

/* calls GetPrivateProfileStringA with the given parameters and prints an error to stdout on failure */
void ini_get_string(const char *section, const char *key, const char *defvalue, char *buf, size_t bufsize, const char *file);

/* 
	formats value as a string and calls WritePrivateProfileStringA with the given parameters 
	and prints an error to stdout on failure 
 */
void ini_write_int(const char *section, const char *key, long value, const char *file);

/* calls GetPrivateProfileIntA with the given parameters and prints an error to stdout and returns zero on failure */
int ini_get_int(const char *section, const char *key, int defvalue, const char *file);

/*
	gets the given checkbox control from the given parent window, 
	formats the checked state as a string and calls WritePrivateProfileStringA with the given parameters
	and prints an error to stdout on failure
*/
void ini_write_check(const char *section, const char *key, HWND parent, int controlid, const char *file);

/*
	gets the given checkbox control from the given parent window,
	gets the key value from the ini using ini_getint and sets the checkbox's checked state accordingly.
	prints an error to stdout on failure
*/
void ini_get_check(const char *section, const char *key, HWND parent, int controlid, const char *file);

/*
	iterates the given array of checkboxes stored as ini_control
	structs and writes their states to the ini using ini_writecheck.
	the array must be NULL-terminated (a NULL key string indicates the end of the array)
*/
void ini_write_checks(const char *section, HWND parent, const ini_control checkboxes[], const char *file);

/*
	iterates the given array of checkboxes stored as ini_control
	structs and reads and sets their states to the ini using ini_getcheck
*/
void ini_get_checks(const char *section, HWND parent, const ini_control checkboxes[], const char *file);

/*
	gets the given edit control from the given parent window,
	gets the text, calls WritePrivateProfileStringA with the given parameters
	and prints an error to stdout on failure
*/
void ini_write_edit(const char *section, const char *key, HWND parent, int controlid, const char *file);

/*
	gets the given edit control from the given parent window,
	gets the key value from the ini using ini_getint and sets the edit's text accordingly.
	prints an error to stdout on failure
*/
void ini_get_edit(const char *section, const char *key, HWND parent, int controlid, const char *file);

/*
	iterates the given array of edit controls stored as ini_control
	structs and writes their states to the ini using ini_writeedit.
	the array must be NULL-terminated (a NULL key string indicates the end of the array)
*/
void ini_write_edits(const char *section, HWND parent, const ini_control edits[], const char *file);

/*
	iterates the given array of edit controls stored as ini_control
	structs and restores their states from the ini using ini_getedit.
	the array must be NULL-terminated (a NULL key string indicates the end of the array)
*/
void ini_get_edits(const char *section, HWND parent, const ini_control edits[], const char *file);

/*
	gets the given combobox control from the given parent window,
	gets the selected index, calls WritePrivateProfileStringA with the given parameters
	and prints an error to stdout on failure
*/
void ini_write_combo(const char *section, const char *key, HWND parent, int controlid, const char *file);

/*
	gets the given combobox control from the given parent window,
	gets the selected index from the ini calling ini_getstring with the given parameters,
	sets the selected index on the control accordingly
	and prints an error to stdout on failure
*/
void ini_get_combo(const char *section, const char *key, HWND parent, int controlid, const char *file);

/*
	iterates the given array of combo controls stored as ini_control
	structs and writes their states to the ini using ini_writecombo.
	the array must be NULL-terminated (a NULL key string indicates the end of the array)
*/
void ini_write_combos(const char *section, HWND parent, const ini_control combos[], const char *file);

/*
	iterates the given array of combo controls stored as ini_control
	structs and restires their states from the ini using ini_getcombo.
	the array must be NULL-terminated (a NULL key string indicates the end of the array)
*/
void ini_get_combos(const char *section, HWND parent, const ini_control combos[], const char *file);

#endif
