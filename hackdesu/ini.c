/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of hackdesu.
	hackdesu is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	hackdesu is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with hackdesu. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ini.h"
#include <stdio.h>
#include "win32_gui.h" /* TODO: get rid of win32_gui lib and make a better one in hackdesu */
#include "io.h"

/* calls WritePrivateProfileStringA with the given parameters and prints an error to stdout on failure */
void ini_write_string(const char *section, const char *key, const char *value, const char *file) {
#if _DEBUG
	char buf[512] = { 0 };
#endif
	if (!WritePrivateProfileStringA(section, key, value, file)) {
#if _DEBUG
		sprintf_s(buf, 512, __FUNCDNAME__ ": " "WritePrivateProfileStringA failed for [%s] %s", section, key);
		win32_show_error(buf);
#endif
	}
}

/* calls GetPrivateProfileStringA with the given parameters and prints an error to stdout on failure */
void ini_get_string(const char *section, const char *key, const char *defval, char *buf, size_t bufsize, const char *file) {
#if _DEBUG
	char buf2[512] = { 0 };
#endif
	if (!GetPrivateProfileStringA(section, key, defval, buf, bufsize, file)) {
#if _DEBUG
		sprintf_s(buf2, 512, __FUNCDNAME__ ": " "GetPrivateProfileStringA failed for [%s] %s", section, key);
		win32_show_error(buf2);
#endif
		strcpy_s(buf, bufsize, defval);
	}
}

/*
	formats value as a string and calls WritePrivateProfileStringA with the given parameters
	and prints an error to stdout on failure
*/
void ini_write_int(const char *section, const char *key, long value, const char *file) {
	char buf[16] = { 0 };
	sprintf_s(buf, 16, "%d", value);
	ini_write_string(section, key, buf, file);
}

/* calls GetPrivateProfileIntA with the given parameters and prints an error to stdout and returns zero on failure */
int ini_get_int(const char *section, const char *key, int defval, const char *file) {
	return GetPrivateProfileIntA(section, key, defval, file);
}

/*
	gets the given checkbox control from the given parent window,
	formats the checked state as a string and calls WritePrivateProfileStringA with the given parameters
	and prints an error to stdout on failure
*/
void ini_write_check(const char *section, const char *key, HWND parent, int controlid, const char *file) {
	HWND ctl;
#if _DEBUG
	char buf[256] = { 0 };
#endif
	ctl = GetDlgItem(parent, controlid);
	if (!ctl) {
#if _DEBUG
		sprintf_s(buf, 256, __FUNCDNAME__ ": " "GetDlgItem failed in ini_writecheck for [%s] %s", section, key);
		win32_show_error(buf);
#endif
		return;
	}
	ini_write_int(section, key, Button_GetCheck(ctl) == BST_CHECKED, file);
}

/*
	gets the given checkbox control from the given parent window,
	gets the key value from the ini using ini_getint and sets the checkbox's checked state accordingly.
	prints an error to stdout on failure
*/
void ini_get_check(const char *section, const char *key, HWND parent, int controlid, const char *file) {
	HWND ctl;
#if _DEBUG
	char buf[256] = { 0 };
#endif
	int checked;
	int curcheck;
	ctl = GetDlgItem(parent, controlid);
	if (!ctl) {
#if _DEBUG
		sprintf_s(buf, 256, __FUNCDNAME__ ": " "GetDlgItem failed in ini_getcheck for [%s] %s", section, key);
		win32_show_error(buf);
#endif
		return;
	}
	curcheck = Button_GetCheck(ctl);
	checked = ini_get_int(section, key, curcheck, file);
	Button_SetCheck(ctl, checked ? BST_CHECKED : BST_UNCHECKED);
}

/* 
	iterates the given array of checkboxes stored as ini_control 
	structs and writes their states to the ini using ini_writecheck 
*/
void ini_write_checks(const char *section, HWND parent, const ini_control checkboxes[], const char *file) {
	for (; checkboxes->key; checkboxes++) {
		ini_write_check(section, checkboxes->key, parent, checkboxes->ctlid, file);
	}
}

/*
	iterates the given array of checkboxes stored as ini_control
	structs and reads and restores their states from the ini using ini_getcheck
*/
void ini_get_checks(const char *section, HWND parent, const ini_control checkboxes[], const char *file) {
	for (; checkboxes->key; checkboxes++) {
		ini_get_check(section, checkboxes->key, parent, checkboxes->ctlid, file);
	}
}

/*
	gets the given edit control from the given parent window,
	gets the text, calls WritePrivateProfileStringA with the given parameters
	and prints an error to stdout on failure
*/
void ini_write_edit(const char *section, const char *key, HWND parent, int controlid, const char *file) {
	char buf[256] = { 0 };
	if (!GetDlgItemTextA(parent, controlid, buf, 256)) {
#if _DEBUG
		sprintf_s(buf, 256, __FUNCDNAME__ ": " "GetDlgItemTextA failed in ini_writeedit for [%s] %s", section, key);
		win32_show_error(buf);
#endif
		return;
	}
	ini_write_string(section, key, buf, file);
}

/*
	gets the given edit control from the given parent window,
	gets the key value from the ini using ini_getint and sets the edit's text accordingly.
	prints an error to stdout on failure
 */
void ini_get_edit(const char *section, const char *key, HWND parent, int controlid, const char *file) {
	char buf[256] = { 0 };
	char buf2[256] = { 0 };

	if (!GetDlgItemTextA(parent, controlid, buf, 256)) {
#if _DEBUG
		sprintf_s(buf, 256, __FUNCDNAME__ ": " "GetDlgItemTextA failed in ini_getedit for [%s] %s", section, key);
		win32_show_error(buf);
#endif
		return;
	}

	ini_get_string(section, key, buf, buf2, 256, file);
	if (!SetDlgItemTextA(parent, controlid, buf2)) {
#if _DEBUG
		sprintf_s(buf, 256, __FUNCDNAME__ ": " "SetDlgItemTextA failed in ini_getedit for [%s] %s", section, key);
		win32_show_error(buf);
#endif
		return;
	}
}

/*
	iterates the given array of edit controls stored as ini_control
	structs and writes their states to the ini using ini_writeedit.
	the array must be NULL-terminated (a NULL key string indicates the end of the array)
*/
void ini_write_edits(const char *section, HWND parent, const ini_control edits[], const char *file) {
	for (; edits->key; edits++) {
		ini_write_edit(section, edits->key, parent, edits->ctlid, file);
	}
}

/*
	iterates the given array of edit controls stored as ini_control
	structs and restores their states from the ini using ini_getedit.
	the array must be NULL-terminated (a NULL key string indicates the end of the array)
*/
void ini_get_edits(const char *section, HWND parent, const ini_control edits[], const char *file) {
	for (; edits->key; edits++) {
		ini_get_edit(section, edits->key, parent, edits->ctlid, file);
	}
}

/*
	gets the given combobox control from the given parent window,
	gets the selected index, calls WritePrivateProfileStringA with the given parameters
	and prints an error to stdout on failure
*/
void ini_write_combo(const char *section, const char *key, HWND parent, int controlid, const char *file) {
	int selected_index;
	HWND wnd_combo;
#if _DEBUG
	char buf[256] = { 0 };
#endif

	wnd_combo = GetDlgItem(parent, controlid);
	if (!wnd_combo) {
#if _DEBUG
		sprintf_s(buf, 256, __FUNCDNAME__ ": " "GetDlgItem failed for [%s] %s", section, key);
		win32_show_error(buf);
#endif
		return;
	}
	selected_index = ComboBox_GetCurSel(wnd_combo);
	if (selected_index == CB_ERR) {
#if _DEBUG
		sprintf_s(buf, 256, __FUNCDNAME__ ": " "ComboBox_GetCurSel failed for [%s] %s", section, key);
		win32_show_error(buf);
#endif
		return;
	}

	ini_write_int(section, key, selected_index, file);
}

/*
	gets the given combobox control from the given parent window,
	gets the selected index from the ini calling ini_getstring with the given parameters, 
	sets the selected index on the control accordingly
	and prints an error to stdout on failure
*/
void ini_get_combo(const char *section, const char *key, HWND parent, int controlid, const char *file) {
	int selected_index;
	HWND wnd_combo;
#if _DEBUG
	char buf[256] = { 0 };
#endif

	wnd_combo = GetDlgItem(parent, controlid);
	if (!wnd_combo) {
#if _DEBUG
		sprintf_s(buf, 256, __FUNCDNAME__ ": " "GetDlgItem failed in ini_writecombo for [%s] %s", section, key);
		win32_show_error(buf);
#endif
		return;
	}
	selected_index = ComboBox_GetCurSel(wnd_combo);
	if (selected_index == CB_ERR) {
#if _DEBUG
		sprintf_s(buf, 256, __FUNCDNAME__ ": " "ComboBox_GetCurSel failed in ini_writecombo for [%s] %s", section, key);
		win32_show_error(buf);
#endif
		return;
	}

	ComboBox_SetCurSel(wnd_combo, ini_get_int(section, key, selected_index, file));
}

/*
	iterates the given array of combo controls stored as ini_control
	structs and writes their states to the ini using ini_writecombo.
	the array must be NULL-terminated (a NULL key string indicates the end of the array)
*/
void ini_write_combos(const char *section, HWND parent, const ini_control combos[], const char *file) {
	for (; combos->key; combos++) {
		ini_write_combo(section, combos->key, parent, combos->ctlid, file);
	}
}

/*
	iterates the given array of combo controls stored as ini_control
	structs and restires their states from the ini using ini_getcombo.
	the array must be NULL-terminated (a NULL key string indicates the end of the array)
*/
void ini_get_combos(const char *section, HWND parent, const ini_control combos[], const char *file) {
	for (; combos->key; combos++) {
		ini_get_combo(section, combos->key, parent, combos->ctlid, file);
	}
}
