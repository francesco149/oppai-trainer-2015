/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of hackdesu.
	hackdesu is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	hackdesu is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with hackdesu. If not, see <http://www.gnu.org/licenses/>.
*/

#include "hack.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <Windows.h>

#include "io.h"
#include "string.h"
#include "command_queue.h"

/* initializes clean_mem from the given address and mempatch size if it isn't already. returns zero on errors. */
static int basic_memory_patch_init(__inout basic_memory_patch *p, __in uint8_t *addr, uint32_t cb) {
	errno_t err;
	char buferr[512] = { 0 };

	if (!p->initialized) {
		if (!p->enabled) {
			dbgputs("memorypatch_toggle: invalid disable state when uninitialized.");
			return 0;
		}

		/* copy clean memory */
		err = memcpy_s(p->clean_mem, MAX_MEMORYPATCH, addr, cb);
		if (err) {
			strerror_s(buferr, 512, err);
			dbgprintf("memorypatch_toggle: memcpy_s failed: %s\n", buferr);
			return 0;
		}

		p->initialized = 1;
	}

	return 1;
}

/* sets the enable toggle. returns zero if already enabled or disabled. */
static int basic_memory_patch_toggle(__inout basic_memory_patch *p, int enabled) {
	if (p->enabled == enabled) {
		return 0;
	}

	p->enabled = enabled;
	return 1;
}

/*
	toggles a memory patch on and off.
	returns zero on failure (when the patch is already enabled/disabled)
*/
static int memory_patch_toggle(__inout memory_patch *p, int enabled) {
	strerr err;

	/* toggle logic */
	if (!basic_memory_patch_toggle(&p->common, enabled)) {
		return 0;
	}

	if (!p->common.initialized) {
		/* parse bytes */
		err = string_tobytearray(p->patch, p->edit_mem, MAX_MEMORYPATCH, (int *)&p->cb);
		if (err) {
			dbgprintf("memorypatch_toggle: string_tobytearray failed: %s\n", strerr_msg(err));
			return 0;
		}

		/* init clean memory backup */
		if (!basic_memory_patch_init(&p->common, p->addr, p->cb)) {
			return 0;
		}
	}

	return cq_write(p->addr, enabled ? p->edit_mem : p->common.clean_mem, p->cb);
}

/* toggles the hook on and off. returns zero on failure. */
static int hook_toggle(__inout hook *p, int enabled) {
	/* toggle logic */
	if (!basic_memory_patch_toggle(&p->common, enabled)) {
		return 0;
	}

	/* init clean memory backup */
	if (!basic_memory_patch_init(&p->common, p->addr, 5 + p->nops)) {
		return 0;
	}

	/* write the hook */
	if (enabled) {
		return cq_write_jumpE9(p->addr, p->hook, p->nops);
	}

	/* restore clean memory */
	return cq_write(p->addr, p->common.clean_mem, 5 + p->nops);
}

/* toggles the detour on and off. returns zero on failure. */
static int detour_toggle(__inout detour *p, int enabled) {
	if (p->enabled == enabled) {
		return 0;
	}
	if (!*p->pptarget) {
		return 0;
	}
	p->enabled = enabled;
	return cq_detour(enabled, p->pptarget, p->hook);
}

/* toggles the winapi detour on and off. returns zero on failure. */
static int winapi_detour_toggle(__inout winapi_detour *p, int enabled) {
	union tagfarproc_wrap { 
		void *void_ptr; 
		FARPROC func_ptr; 
	} proc_addr;

	HMODULE module;
	if (p->enabled == enabled) {
		return 0;
	}
	if (!*p->pptarget) {
		module = GetModuleHandleA(p->dll);
		if (!module) {
			dbgprintf("winapidetour_toggle: failed to get module handle for %s\n", p->dll);
			return 0;
		}
		proc_addr.func_ptr = GetProcAddress(module, p->func_name);
		*p->pptarget = proc_addr.void_ptr;
		if (!*p->pptarget) {
			dbgprintf("winapidetour_toggle: failed to get proc addr for %s\n", p->func_name);
			return 0;
		}
	}
	p->enabled = enabled;
	return cq_detour(enabled, p->pptarget, p->hook);
}

/* toggles the hack according to the specified type */
static int generic_hack_toggle(__inout generic_hack *p, int enabled) {
	switch (p->type) {
	case HACK_MEMPATCH:
		return memory_patch_toggle((memory_patch *)p->ptr, enabled);
	case HACK_HOOK:
		return hook_toggle((hook *)p->ptr, enabled);
	case HACK_DETOUR:
		return detour_toggle((detour *)p->ptr, enabled);
	case HACK_WINAPI_DETOUR:
		return winapi_detour_toggle((winapi_detour *)p->ptr, enabled);
	}

	dbgprintf("generichack_toggle: unknown hack type %d.\n", (int)p->type);
	return 0;
}

/* toggles the hack on and off. returns 0 on failure. */
int hack_toggle(__inout hack *p, int enabled) {
	int i;

	for (i = 0; i < p->count; i++) {
		if (!generic_hack_toggle(&p->hacks[i], enabled)) {
			return 0;
		}
	}

	return 1;
}
