/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of hackdesu.
	hackdesu is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	hackdesu is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with hackdesu. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __HACKDESU_PADDING_MACROS_H__
#define __HACKDESU_PADDING_MACROS_H__

#include "repeat_macros.h"

/*
	must be placed at the beginning of a pointer-reading struct
	h, t, o are hundreds, tens and ones of the number of offsets that the struct will have
	e.g 123 offsets = init_offsets(1, 2, 3)
	NOTE: each offset must be defined immediately below this macro in order of size using define_offset
	Example usage:
	struct MyStruct {
		init_offsets(0, 0, 2);
		uint32_t some_value; // +0x0000
		void *some_pointer; // +0x0004
	
		define_offset(AABB)
		double some_double; // +0xAABB
	 
		define_offset(BBCC)
		int32_t some_signed_int; // +0xBBCC
	};
*/
#define init_offsets(h, t, u) REP(h, t, u, union { struct {)

/*
	this macro must be used above the members that you want to be at the given offset
	off is a hex number without the 0x prefix.
	it will generate a padding byte array with an unique name to match the desired offset.
	see init_offsets for more info.
 */
#define define_offset(off) indirect_define_offset(off, __LINE__)

/* internal macro used by define_offset. don't touch. */
#define indirect_define_offset(off, unique) indirect_define_offset2(off, unique)

/* internal macro used by define_offset. don't touch. */
#define indirect_define_offset2(off, name) }; uint8_t padding##name[0x##off##]; };

#endif
